<?php

use Domain\Marketplace\Marketplace;

return [
    'marketplaces' => [
        Marketplace::AMAZON_MARKETPLACE => [

        ],
    ],
    'ccc'          => [
        // monthly-sales
        'ms' => [
            'ms-in' => [
                'value'  => 'monthly-sales',
                'factor' => 1,
                'order'  => 4,
            ],
            'ms-l1' => [
                'value'  => 'ms-in',
                'factor' => 0.8,
                'order'  => 3,
            ],
            'ms-l2' => [
                'value'  => 'ms-l1',
                'factor' => 0.8,
                'order'  => 2,
            ],
            'ms-l3' => [
                'value'  => 'ms-l2',
                'factor' => 0.8,
                'order'  => 1,
            ],
            'ms-u1' => [
                'value'  => 'ms-in',
                'factor' => 1.1,
                'order'  => 5,
            ],
            'ms-u2' => [
                'value'  => 'ms-u1',
                'factor' => 1.1,
                'order'  => 6,
            ],
            'ms-u3' => [
                'value'  => 'ms-u2',
                'factor' => 1.1,
                'order'  => 7,
            ],
        ],
        // sales price
        'sp' => [
            'sp-in' => [
                'value'  => 'sales-price',
                'factor' => 1,
                'order'  => 4,
            ],
            'sp-l1' => [
                'value'  => 'sp-in',
                'factor' => 0.8,
                'order'  => 5,
            ],
            'sp-l2' => [
                'value'  => 'sp-l1',
                'factor' => 0.8,
                'order'  => 6,
            ],
            'sp-l3' => [
                'value'  => 'break-event-sales-price',
                'factor' => 1,
                'order'  => 7,
            ],
            'sp-u1' => [
                'value'  => 'sp-in',
                'factor' => 1.1,
                'order'  => 3,
            ],
            'sp-u2' => [
                'value'  => 'sp-u1',
                'factor' => 1.1,
                'order'  => 2,
            ],
            'sp-u3' => [
                'value'  => 'sp-u2',
                'factor' => 1.1,
                'order'  => 1,
            ],
        ],
    ],
];
