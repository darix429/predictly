<?php

use Carbon\Carbon;
use Domain\Calculation\Calculator\Marketplace\AmazonCalculator;
use Domain\Core\CurrencyTransformer;
use Domain\Marketplace\Marketplace;

return [
    'marketplaces' => [
        Marketplace::AMAZON_MARKETPLACE => [
            'calculator'   => [
                'class' => AmazonCalculator::class,
            ],
            'price'        => [
                'factors' => [
                    '25' => [0, CurrencyTransformer::dollarsToCents(20)],
                    '15' => [CurrencyTransformer::dollarsToCents(20), CurrencyTransformer::dollarsToCents(50)],
                    '10' => [CurrencyTransformer::dollarsToCents(50)],
                ],
            ],
            'shifts'       => [-49, -36, -20, 0, 10, 21, 33],
            'monthsPeriod' => Carbon::MONTHS_PER_YEAR,
        ],
    ],
];
