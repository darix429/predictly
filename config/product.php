<?php

use Domain\Marketplace\Marketplace;
use Domain\Product\Parser\AmazonParser;

return [
    'parser' => [
        'marketplaces' => [
            Marketplace::AMAZON_MARKETPLACE => [
                'csv' => [
                    'class' => AmazonParser::class,
                ],
            ],
        ],
    ],
];
