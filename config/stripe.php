<?php
return [
    "key"=>env('MIX_STRIPE_KEY'),
    "secret"=>env('MIX_STRIPE_SECRET')
];