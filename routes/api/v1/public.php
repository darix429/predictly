<?php

/** @var \Illuminate\Routing\Router $router */

// support
$router->post('support/question/send', [
    'as'   => 'support.question.send',
    'uses' => 'Support\Controllers\SupportController@sendQuestion',
]);

$router->post('/auth/sign-in', [
    'as'   => 'auth.sign-in',
    'uses' => 'Auth\Controllers\AuthController@signIn',
]);

// auth
$router->post('/auth/sign-up', [
    'as'   => 'auth.sign-up',
    'uses' => 'Auth\Controllers\AuthController@signUp',
]);

$router->post('auth/refresh-token', [
    'as'   => 'auth.refresh-token',
    'uses' => 'Auth\Controllers\AuthController@refreshToken',
]);

// auth social

// $router->post('/auth/social/signin', [
//     // TODO: Deprecated Route - need to remove
//     'as'   => 'social.signin',
//     'uses' => 'Auth\Controllers\Social\AuthController@signIn',
// ]);

// $router->post('/auth/social/signup', [
//     // TODO: Deprecated Route - need to remove
//     'as'   => 'social.signup',
//     'uses' => 'Auth\Controllers\Social\AuthController@signUp',
// ]);

// $router->post('/auth/social/sign-in', [
//     'as'   => 'social.sign-in',
//     'uses' => 'Auth\Controllers\Social\AuthController@signIn',
// ]);

$router->post('/auth/social/sign-in', [
    'as'   => 'social.auth',
    'uses' => 'Auth\Controllers\Social\AuthController@socialSignIn',
]);