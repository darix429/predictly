<?php

/** @var \Illuminate\Routing\Router $router */

$router->get('support/feedback', [
    'as'   => 'support.feedback.list',
    'uses' => 'Support\Controllers\SupportController@feedbackList',
]);

$router->post('support/feedback/send', [
    'as'   => 'support.feedback.send',
    'uses' => 'Support\Controllers\SupportController@sendFeedback',
]);
