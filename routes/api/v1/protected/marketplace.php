<?php

/** @var  \Illuminate\Routing\Router $router */

$router->get('marketplaces', [
    'as' => 'marketplaces.index',
    'uses' => 'Marketplace\Controllers\MarketplaceController@index',
]);
