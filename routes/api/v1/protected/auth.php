<?php

/** @var \Illuminate\Routing\Router $router */

$router->post('auth/logout', [
    'as'   => 'auth.logout',
    'uses' => 'Auth\Controllers\AuthController@logout',
]);

$router->post('auth/social/link', [
    'as'   => 'auth.social.link',
    'uses' => 'Auth\Controllers\Social\AuthController@link',
]);
