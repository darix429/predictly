<?php

/** @var \Illuminate\Routing\Router $router */

$router->get('sessions/active', [
    'as'   => 'sessions.active',
    'uses' => 'Session\Controllers\SessionController@active',
]);

$router->patch('sessions/{sessionId}/marketplaces', [
    'as'    => 'sessions.marketplaces.update',
    'uses'  => 'Session\Controllers\SessionController@updateMarketplace',
    'where' => [
        'sessionId' => '[0-9]+',
    ],
]);

$router->patch('sessions/{sessionId}/predictions', [
    'as'    => 'sessions.predictions.update',
    'uses'  => 'Session\Controllers\SessionController@updatePrediction',
    'where' => [
        'sessionId' => '[0-9]+',
    ],
]);

$router->patch('sessions/{sessionId}/products', [
    'as'    => 'sessions.products.attach',
    'uses'  => 'Session\Controllers\SessionController@attachProduct',
    'where' => [
        'sessionId' => '[0-9]+',
    ],
]);

$router->delete('sessions/{sessionId}/products', [
    'as'    => 'sessions.products.detach',
    'uses'  => 'Session\Controllers\SessionController@detachProduct',
    'where' => [
        'sessionId' => '[0-9]+',
    ],
]);

$router->post('sessions/{sessionId}/calculate', [
    'as'   => 'sessions.calculate',
    'uses' => 'Calculation\Controllers\CalculationController@calculate',
    'where' => [
        'sessionId' => '[0-9]+',
    ],
]);

$router->post('sessions/{sessionId}/tune', [
    'as'   => 'sessions.calculate',
    'uses' => 'Calculation\Controllers\CalculationController@tune',
    'where' => [
        'sessionId' => '[0-9]+',
    ],
]);

$router->get('sessions/last', [
    'as'   => 'sessions.result',
    'uses' => 'Session\Controllers\SessionController@last',
]);
