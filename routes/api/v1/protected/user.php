<?php

/** @var \Illuminate\Routing\Router $router */

$router->get('me', [
    'as'   => 'user.me',
    'uses' => 'User\Controllers\UserController@me',
]);
