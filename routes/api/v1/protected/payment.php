<?php

/** @var  \Illuminate\Routing\Router $router */

$router->post('payment', [
    'as'   => 'payment.charge',
    'uses' => 'Payment\Controllers\PaymentController@payment',
]);

$router->post('cancelpayment', [
    'as'   => 'payment.cancel',
    'uses' => 'Payment\Controllers\PaymentController@processCancellation',
]);

$router->get('paymentoptions', [
    'as'   => 'paymentoptions.all',
    'uses' => 'Payment\Controllers\PaymentController@paymentOptions',
]);

$router->patch('paymentoption', [
    'as'   => 'paymentoption.update',
    'uses' => 'Payment\Controllers\PaymentController@updatePaymentOption',
]);