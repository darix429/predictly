<?php

/** @var  \Illuminate\Routing\Router $router */

$router->get('predictions', [
    'as'   => 'predictions.index',
    'uses' => 'Prediction\Controllers\PredictionController@index',
]);
