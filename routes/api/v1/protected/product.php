<?php

/** @var  \Illuminate\Routing\Router $router */

$router->get('products', [
    'as'   => 'products.index',
    'uses' => 'Product\Controllers\ProductController@index',
]);

$router->post('products', [
    'as'   => 'products.store',
    'uses' => 'Product\Controllers\ProductController@store',
]);

$router->post('products/upload', [
    'as'   => 'products.upload',
    'uses' => 'Product\Controllers\ProductController@upload',
]);

$router->put('products/{productId}', [
    'as'   => 'products.update',
    'uses' => 'Product\Controllers\ProductController@update',
    'where'      => [
        'productId' => '[0-9]+',
    ],
]);

$router->delete('products/{productId}', [
    'as'   => 'products.destroy',
    'uses' => 'Product\Controllers\ProductController@destroy',
    'where'      => [
        'productId' => '[0-9]+',
    ],
]);
