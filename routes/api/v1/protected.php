<?php

/** @var  \Illuminate\Routing\Router $router */

$router->group([], base_path('routes/api/v1/protected/auth.php'));
$router->group([], base_path('routes/api/v1/protected/marketplace.php'));
$router->group([], base_path('routes/api/v1/protected/product.php'));
$router->group([], base_path('routes/api/v1/protected/prediction.php'));
$router->group([], base_path('routes/api/v1/protected/session.php'));
$router->group([], base_path('routes/api/v1/protected/user.php'));
$router->group([], base_path('routes/api/v1/protected/support.php'));
$router->group([], base_path('routes/api/v1/protected/payment.php'));
