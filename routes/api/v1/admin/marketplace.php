<?php

/** @var \Illuminate\Routing\Router $router */

$router->get('marketplaces', [
    'as'         => 'marketplaces.list',
    'uses'       => 'Marketplace\Controllers\MarketplaceController@index',
    'middleware' => ['can:view marketplaces'],
]);

$router->get('marketplaces/{marketplaceId}', [
    'as'         => 'marketplaces.view',
    'uses'       => 'Marketplace\Controllers\MarketplaceController@show',
    'middleware' => ['can:show marketplace'],
    'where'      => [
        'marketplaceId' => '[0-9]+',
    ],
]);

$router->post('marketplaces', [
    'as'         => 'marketplaces.store',
    'uses'       => 'Marketplace\Controllers\MarketplaceController@store',
    'middleware' => ['can:create marketplace'],
]);

$router->put('marketplaces/{marketplaceId}', [
    'as'         => 'marketplaces.update',
    'uses'       => 'Marketplace\Controllers\MarketplaceController@update',
    'middleware' => ['can:update marketplace'],
    'where'      => [
        'marketplaceId' => '[0-9]+',
    ],
]);
