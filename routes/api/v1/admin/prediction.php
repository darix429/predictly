<?php

/** @var \Illuminate\Routing\Router $router */

$router->get('predictions', [
    'as'         => 'predictions.list',
    'uses'       => 'Prediction\Controllers\PredictionController@index',
    'middleware' => ['can:view predictions'],
]);

$router->get('predictions/{predictionsId}', [
    'as'         => 'predictions.view',
    'uses'       => 'Prediction\Controllers\PredictionController@show',
    'middleware' => ['can:show prediction'],
    'where'      => [
        'predictionsId' => '[0-9]+',
    ],
]);

$router->post('predictions', [
    'as'         => 'predictions.store',
    'uses'       => 'Prediction\Controllers\PredictionController@store',
    'middleware' => ['can:create prediction'],
]);

$router->put('predictions/{predictionsId}', [
    'as'         => 'predictions.update',
    'uses'       => 'Prediction\Controllers\PredictionController@update',
    'middleware' => ['can:update prediction'],
    'where'      => [
        'predictionsId' => '[0-9]+',
    ],
]);

$router->delete('predictions/{predictionsId}', [
    'as'         => 'predictions.destroy',
    'uses'       => 'Prediction\Controllers\PredictionController@destroy',
    'middleware' => ['can:delete prediction'],
    'where'      => [
        'predictionsId' => '[0-9]+',
    ],
]);
