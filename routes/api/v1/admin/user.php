<?php

/** @var \Illuminate\Routing\Router $router */

$router->get('users', [
    'as'         => 'users.list',
    'uses'       => 'User\Controllers\UserController@index',
    'middleware' => ['can:view users'],
]);

$router->get('users/{userId}', [
    'as'         => 'users.show',
    'uses'       => 'User\Controllers\UserController@show',
    'middleware' => ['can:show user'],
    'where'      => [
        'userId' => '[0-9]+',
    ],
]);

$router->patch('users/{userId}/verify', [
    'as'         => 'users.verify',
    'uses'       => 'User\Controllers\UserController@verify',
    'middleware' => ['can:verify user'],
    'where'      => [
        'userId' => '[0-9]+',
    ],
]);

$router->patch('users/{userId}/switchFree', [
    'as'         => 'users.switchfree',
    'uses'       => 'User\Controllers\UserController@switchfree',
    'where'      => [
        'userId' => '[0-9]+',
    ],
]);

$router->patch('users/{userId}/unverify', [
    'as'         => 'users.verify',
    'uses'       => 'User\Controllers\UserController@unverify',
    'middleware' => ['can:unverify user'],
    'where'      => [
        'userId' => '[0-9]+',
    ],
]);

$router->delete('users/{userId}', [
    'as'         => 'user.destroy',
    'uses'       => 'User\Controllers\UserController@destroy',
    'middleware' => ['can:delete user'],
    'where'      => [
        'userId' => '[0-9]+',
    ],
]);
