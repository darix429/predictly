<?php

/** @var \Illuminate\Routing\Router $router */

$router->get('/facebook/post-message', [
    'as'   => 'facebook.post-message',
    'uses' => 'Auth\Controllers\Facebook\PostMessageController@index',
]);

$router->get('/docs', [
    'as'   => 'docs.swagger.index',
    'uses' => 'Docs\Controllers\SwaggerController@index',
]);

$router->get('/docs/swagger/{fileName}', [
    'as'    => 'docs.swagger.file',
    'uses'  => 'Docs\Controllers\SwaggerController@file',
    'where' => [
        'fileName' => '(.*)',
    ],
]);
