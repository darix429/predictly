const mix = require('laravel-mix');
const path = require('path');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/dist/js')
    .sass('resources/sass/app.scss', 'public/dist/css')
    .version();

mix.copy('resources/images', 'public/dist/images')
    .copy('resources/fonts', 'public/dist/fonts')
    .webpackConfig({
        resolve: {
            alias: {
                '@': path.resolve('resources/sass')
            }
        }
    });

// copy swagger assets
mix.copy('node_modules/swagger-ui-dist/favicon-16x16.png', 'public/vendor/swagger-ui/favicon-16x16.png');
mix.copy('node_modules/swagger-ui-dist/favicon-32x32.png', 'public/vendor/swagger-ui/favicon-32x32.png');
mix.copy('node_modules/swagger-ui-dist/swagger-ui.css', 'public/vendor/swagger-ui/swagger-ui.css');
mix.copy('node_modules/swagger-ui-dist/swagger-ui.js', 'public/vendor/swagger-ui/swagger-ui.js');
mix.copy('node_modules/swagger-ui-dist/swagger-ui-bundle.js', 'public/vendor/swagger-ui/swagger-ui-bundle.js');
mix.copy('node_modules/swagger-ui-dist/swagger-ui-standalone-preset.js', 'public/vendor/swagger-ui/swagger-ui-standalone-preset.js');
