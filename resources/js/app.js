require('./bootstrap');
import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex';
import GAuth from 'vue-google-oauth2';
import Vuelidate from 'vuelidate';
import VueTypedJs from 'vue-typed-js';
import VueSwal from 'vue-swal'
import store from './store';
import router from './routing';
import directives from './helpers/directives';
import {i18n} from './translation/i18n.js';

const gauthOption = {
    clientId: process.env.MIX_GOOGLE_CLIENT_ID,
    clientSecret: process.env.MIX_GOOGLE_CLIENT_SECRET,
    redirectUrl: process.env.MIX_GOOGLE_REDIRECT_URL
};

Vue.use(GAuth, gauthOption);
Vue.use(Vuelidate);
Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(VueTypedJs);
Vue.use(VueSwal);
Vue.prototype.$http = axios;
Vue.prototype.$scrollToTop = () => window.scrollTo(0,0);

const dataFacebook = {
    id: process.env.MIX_FACEBOOK_APP_ID,
    url: process.env.MIX_FACEBOOK_REDIRECT_URL
};
Vue.prototype.$linkFacebook = `https://www.facebook.com/v6.0/dialog/oauth?client_id=${dataFacebook.id}&scope=email&redirect_uri=${dataFacebook.url}`;

import {isNumberCommaInsteadDot} from "./helpers/functions";

Vue.prototype.$isNumberCommaInsteadDot = isNumberCommaInsteadDot;


window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/App.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('app', require('./layouts/App.vue').default);
Vue.component('Icon', require('./components/Icon.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    router,
    store,
    i18n,
    el: '#app',
    created: function () {
        axios.interceptors.response.use(function (response) {
            return response;
        }, function (error) {
            if(403 === error.response.status) {
                router.push({name: 'forbidden', params : {message: error.response.data.message}});
            }
            if (401 === error.response.status) {
                let sendData = {
                    refreshToken: localStorage.getItem('refresh_token')
                };
                axios.post('/api/v1/auth/refresh-token', sendData)
                    .then(response => {
                        let access_token = {};
                        let refresh_token = {};

                        access_token = response.data.result.access_token;
                        refresh_token = response.data.result.refresh_token;
                        localStorage.setItem('refresh_token', refresh_token);
                        localStorage.setItem('access_token', access_token);
                        window.axios.defaults.headers.common['Authorization'] = 'Bearer ' + access_token;
                        location.reload();
                    })
                    .catch(error => {
                        localStorage.removeItem('refresh_token');
                        localStorage.removeItem('access_token');
                        localStorage.removeItem("user_name");
                        window.location.href = '/';

                    });

            } else {
                return Promise.reject(error);
            }
        });
    }
});
