export default (fileUrl, callback) => {
    const request = new XMLHttpRequest();
    request.open("GET", fileUrl, true);
    request.onload = function(e) {
        const div = document.createElement("div");
        div.innerHTML = request.responseText;
        document.body.insertBefore(div, document.body.childNodes[0]);

        callback && callback();
    };
    request.send();
};
