import VueRouter from 'vue-router';

let routes = [
    {
        path: "/",
        component: require('../modules/Home/Index').default,
        name: "home"
    },
    {
        path: "/faq",
        component: require('../modules/Support/pages/FAQ').default,
        name: "faq"
    },
    {
        path: "/about-us",
        component: require('../modules/Support/pages/AboutUs').default,
        name: "about-us"
    },
    {
        path: "/privacy-policy",
        component: require('../modules/Legal/pages/PrivacyPolicy').default,
        name: "privacy-policy"
    },
    {
        path: "/terms-of-service",
        component: require('../modules/Legal/pages/TermsOfService').default,
        name: "terms-of-service"
    },
    {
        path: "/403",
        component: require('../modules/Error/Forbidden').default,
        meta: true,
        name: "forbidden"
    },
    {
        path: "/initial-steps",
        component: require('../modules/Marketplace/InitialSteps').default,
        name: "initial-steps", 
        meta: {
            requiresAuth: true
        }
    },
    {
        path: "/payment",
        component: require('../modules/Payment/pages/Payment').default,
        name: "payment",
        meta: {
            requiresAuth: true
        }
    },
    {
        path: "/products",
        component: require('../modules/Product/pages/Products').default,
        name: "products",
        meta: {
            requiresAuth: true,
            requiresPayment: true
        }
    },
    {
        path: "/products/create",
        component: require('../modules/Product/pages/create/CreateProduct').default,
        name: "create-product",
        meta: {
            requiresAuth: true,
            requiresPayment: true
        }
    },
    {
        path: "/products/uploaded",
        component: require('../modules/Product/pages/uploaded/UploadedProducts').default,
        name: "uploaded-products",
        meta: {
            requiresAuth: true,
            requiresPayment: true
        }
    },
    {
        path: "/products/comparison",
        component: require('../modules/Product/pages/comparison/ProductComparison').default,
        name: "products-comparison",
        meta: {
            requiresAuth: true,
            requiresPayment: true
        }
    },
    {
        path: "/my-account",
        component: require('../modules/User/pages/Account').default,
        name: "user-account"
    },
    {
        path: "/support",
        component: require('../modules/Support/pages/Support').default,
        name: "support",
        meta: {
            requiresAuth: true
        }
    },
    {
        path: "/admin/users",
        component: require('../modules/Admin/pages/users/Users').default,
        name: "users",
        meta: {
            requiresAuth: true,
            requiresAdmin: true
        }
    },
    {
        path: "/admin/users/:id",
        component: require('../modules/Admin/pages/users/UserDetails').default,
        name: "user-details",
        meta: {
            requiresAuth: true,
            requiresAdmin: true
        }
    },
    {
        path: "/admin/calculation-models",
        component: require('../modules/Admin/pages/calculation-models/CalculationModels').default,
        name: "calculation-models",
        meta: {
            requiresAuth: true,
            requiresAdmin: true
        }
    },
    {
        path: "/admin/calculation-models/:type/:id",
        component: require('../modules/Admin/pages/calculation-models/CalculationModelDetails').default,
        name: "calculation-models-details",
        meta: {
            requiresAuth: true,
            requiresAdmin: true
        }
    },
    {
        path: "/admin/my-account",
        component: require('../modules/Admin/pages/Account').default,
        name: "admin-account",
        meta: {
            requiresAuth: true,
            requiresAdmin: true
        }
    },

];


const router = new VueRouter({
    mode: 'history',
    routes,
});


router.beforeEach((to, from, next) => {
    let user = JSON.parse(localStorage.getItem("user"));
    let token = localStorage.getItem("access_token");
    var redirect = undefined;

    if (to.meta.requiresAuth) {
        if (!user && !token) {
            redirect = "home";
        }
    }

    if (to.meta.requiresAdmin) {
        if (user !== null && user.role !== "administrator") {
            redirect = "create-product";
        }
    }
    
    if (to.meta.requiresPayment) {
        if (user !== null && user.role !== "administrator" && user.subscription_count == 0 && !user.free) {
            redirect = "payment";
        }
    }

    if (to.name === "home" && (user !== null)) {
        redirect = "create-product";
    }

    if (to.name === "payment" && (user !== null && user.free)) {
        redirect = "create-product";
    }

    if (redirect !== undefined) {
        next({name: redirect});
    } else {
        next();
    }
    
});


export default router;
