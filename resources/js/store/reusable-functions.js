export function errorExtractor(action) {
    const {data} = action.response;
    let errors = data.errors || [data.message];
    errors = Array.isArray(errors) ? errors : Object.values(errors).flat();
    return errors;
}
