export const GET_USERS = "GET_USERS";
export const VERIFY_USER = "VERIFY_USER";
export const UNVERIFY_USER = "UNVERIFY_USER";
export const GET_SINGLE_USER = "GET_SINGLE_USER";
export const DELETE_USER = "DELETE_USER";
export const SWITCH_FREE_USER = "SWITCH_FREE_USER";