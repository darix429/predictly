export const PREDICTIONS_GET = "PREDICTIONS_GET";
export const PREDICTIONS_GET_SUCCESS = "PREDICTIONS_GET_SUCCESS";
export const PREDICTIONS_GET_ERROR = "PREDICTIONS_GET_ERROR";
export const PREDICTION_ACTIVE = "PREDICTION_ACTIVE";
export const SESSION_ATTACH_PREDICTION = "SESSION_ATTACH_PREDICTION";
export const SESSION_PREDICTION_SUCCESS = "SESSION_PREDICTION_SUCCESS";
export const SESSION_PREDICTION_ERROR = "SESSION_PREDICTION_ERROR";
