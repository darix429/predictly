export const AUTH_REQUEST = "AUTH_REQUEST";
export const AUTH_REQUEST_EMAIL = "AUTH_REQUEST_EMAIL";
export const AUTH_LINK = "AUTH_LINK";
export const AUTH_SUCCESS = "AUTH_SUCCESS";
export const AUTH_ERROR = "AUTH_ERROR";
export const AUTH_LOGOUT = "AUTH_LOGOUT";
