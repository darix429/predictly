/* eslint-disable promise/param-names */
import {
    AUTH_REQUEST,
    AUTH_REQUEST_EMAIL,
    AUTH_ERROR,
    AUTH_SUCCESS,
    AUTH_LOGOUT,
    AUTH_LINK
} from "../actions/auth";
import {USER_REQUEST} from "../actions/user";
import {errorExtractor} from "../reusable-functions";

export default {
    state: {
        token: localStorage.getItem("access_token") || '',
        status: "",
        hasLoadedOnce: false
    },
    getters: {
        isAuthenticated: state => !!state.token,
        authStatus: state => state.status
    },
    mutations: {
        [AUTH_REQUEST]:
            state => {
                state.status = "loading";
            },
        [AUTH_SUCCESS]:
            (state, resp) => {
                state.status = "success";
                state.token = resp.result.access_token;
                state.hasLoadedOnce = true;
            },
        [AUTH_ERROR]:
            state => {
                state.status = "error";
                state.hasLoadedOnce = true;
            },
        [AUTH_LOGOUT]:
            state => {
                state.token = "";
            }
    },
    actions: {
        [AUTH_REQUEST]:
            ({commit, dispatch}, {...data}) => {
                return new Promise((resolve, reject) => {
                    commit(AUTH_REQUEST);
                    axios.post(`/api/v1/auth/social/${data.process}`, { code: data.code, provider: data.provider })
                        .then(({data}) => {
                                localStorage.setItem("access_token", data.result.access_token);
                                localStorage.setItem('refresh_token', data.result.refresh_token);

                                window.axios.defaults.headers.common['Authorization'] = 'Bearer ' + data.result.access_token;
                                
                                commit(AUTH_SUCCESS, data);
                                dispatch(USER_REQUEST);
                                resolve(data);

                        })
                        .catch(error => {
                            // console.log(error.response)
                            commit(AUTH_ERROR, error);
                            localStorage.removeItem("access_token");
                            localStorage.removeItem("refresh_token");
                            localStorage.removeItem("user_name");
                            localStorage.removeItem("user");
                            reject(error.response);
                        });

                });
            },
        [AUTH_LINK]:
            ({commit, dispatch}, {...data}) => {
                return new Promise((resolve, reject) => {
                    axios.post(`/api/v1/auth/social/${data.process}`, { data: data.data, provider: data.provider })
                        .then(({data}) => {
                            resolve(data);
                            dispatch(USER_REQUEST);
                        })
                        .catch(error => {
                            let errors = errorExtractor(error);
                            reject(errors);
                        });

                });
            },
        [AUTH_REQUEST_EMAIL]:
            ({commit, dispatch}, {...data}) => {
                return new Promise((resolve, reject) => {
                    commit(AUTH_REQUEST);
                    axios.post(`/api/v1/auth/${data.process}`, data.form)
                        .then(({data}) => {
                            localStorage.setItem("access_token", data.result.access_token);
                            localStorage.setItem('refresh_token', data.result.refresh_token);

                            window.axios.defaults.headers.common['Authorization'] = 'Bearer ' + data.result.access_token;

                            commit(AUTH_SUCCESS, data);
                            dispatch(USER_REQUEST);
                            resolve(data);

                        })
                        .catch(error => {
                            commit(AUTH_ERROR, error);
                            localStorage.removeItem("access_token");
                            localStorage.removeItem("refresh_token");
                            localStorage.removeItem("user_name");
                            localStorage.removeItem("user");
                            let errors = errorExtractor(error);
                            reject(errors);
                        });

                });
            },
        [AUTH_LOGOUT]:
            ({commit}) => {
                return new Promise(resolve => {
                    axios.post('/api/v1/auth/logout')
                        .then(({data}) => {
                            commit(AUTH_LOGOUT);
                            localStorage.removeItem("access_token");
                            localStorage.removeItem("refresh_token");
                            localStorage.removeItem("user_name");
                            localStorage.removeItem("user");
                            resolve(data);
                        })
                        .catch(err => {
                            reject(err);
                        });

                });
            }
    }
}


