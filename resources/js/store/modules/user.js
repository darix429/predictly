import { USER_REQUEST, USER_ERROR, USER_SUCCESS } from "../actions/user";
import { AUTH_LOGOUT } from "../actions/auth";
import Vue from "vue";

export default {
    state: {
        name: localStorage.getItem("user_name") || '',
		id: '',
        verified: (JSON.parse(localStorage.getItem("user")) && JSON.parse(localStorage.getItem("user")).verified) || '',
        currentUser: {},
        status: "",
        profile: "",
        role: (JSON.parse(localStorage.getItem("user")) && JSON.parse(localStorage.getItem("user")).role) || '',
        subscription_count: (JSON.parse(localStorage.getItem("user")) && JSON.parse(localStorage.getItem("user")).subscription_count) || 0,
        subscription_end_count: (JSON.parse(localStorage.getItem("user")) && JSON.parse(localStorage.getItem("user")).subscription_end_count) || 0,
        freeUser: (JSON.parse(localStorage.getItem("user")) && JSON.parse(localStorage.getItem("user")).free) || 0
    },
    getters: {
        getName: state => state.name,
		getId: state => state.id,
        getProfile: state => state.profile,
        isProfileLoaded: state => !!state.profile.name,
        isVerified: state => state.verified,
        isAdmin: state => state.role === "administrator",
        hasActiveSubscription: state => state.subscription_count > 0,
        getSubscriptionCount: state => state.subscription_count,
        getSubscriptionEndCount: state => state.subscription_end_count,
        getCurrentUser: state => state.currentUser,
        isFreeUser: state => state.freeUser
    },
    mutations: {
        [USER_REQUEST]: state => {
            state.status = "loading";
        },
        [USER_SUCCESS]: (state, data) => {
            state.status = "success";
            state.name = data.result.name;
			state.id = data.result.id;
            state.verified = data.result.verified;
            state.role = data.result.role;
            state.currentUser = data.result;
            state.subscription_count = data.result.subscription_count;
            state.subscription_end_count = data.result.subscription_end_count;
            state.hasActiveSubscription = data.result.subscription_count > 0;
            state.freeUser = data.result.free;
            Vue.set(state, "profile", data.result);
        },
        [USER_ERROR]: state => {
            state.status = "error";
        },
        [AUTH_LOGOUT]: state => {
            state.profile = {};
            state.name = ""
        }
    },
    actions: {
        [USER_REQUEST]: ({ commit, dispatch }) => {
            commit(USER_REQUEST);
            axios.get('/api/v1/me')
                .then(({data}) => {
                    localStorage.setItem("user_name", data.result.name);
                    localStorage.setItem("user", JSON.stringify(data.result));
                    console.log("USER REQUEST DATA", data);
                    commit(USER_SUCCESS, data);
                })
                .catch(() => {
                    localStorage.removeItem("user_name");
                    localStorage.removeItem("user");
                    commit(USER_ERROR);
                    // if resp is unauthorized, logout, to
                    dispatch(AUTH_LOGOUT);
                });
        }
    }
}




