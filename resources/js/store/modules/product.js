import {
    PRODUCT_STORE,
    PRODUCT_STORE_SUCCESS,
    PRODUCT_STORE_ERROR,
    PRODUCT_UPDATE,
    PRODUCT_UPDATE_SUCCESS,
    PRODUCT_UPDATE_ERROR,
    PRODUCT_GET,
    PRODUCT_GET_SUCCESS,
    PRODUCT_GET_ERROR,
    PRODUCT_DELETE,
    PRODUCT_DELETE_SUCCESS,
    PRODUCT_DELETE_ERROR
} from "../actions/product";
import Vue from "vue";
import {errorExtractor} from "../reusable-functions";

export default {
    state: {
        products: [],
        status: "",
    },
    getters: {
        getProducts: state => state.products
    },
    mutations: {
        [PRODUCT_STORE]: state => {
            state.status = "loading";
        },
        [PRODUCT_STORE_ERROR]: state => {
            state.status = "error";
        },
        [PRODUCT_STORE_SUCCESS]: (state, data) => {
            state.status = "success";
            state.products.push(data);
        },
        [PRODUCT_UPDATE]: state => {
            state.status = "loading";
        },
        [PRODUCT_UPDATE_ERROR]: state => {
            state.status = "error";
        },
        [PRODUCT_UPDATE_SUCCESS]: (state, data) => {
            state.status = "success";
            const index = state.products.findIndex((e) => e.id === data.id);
            Vue.set(state.products, index, data);

        },
        [PRODUCT_GET]: state => {
            state.status = "loading";
        },
        [PRODUCT_GET_ERROR]: state => {
            state.status = "error";
        },
        [PRODUCT_GET_SUCCESS]: (state, data) => {
            state.status = "success";
            state.products = data;
        },
        [PRODUCT_DELETE]: state => {
            state.status = "loading";
        },
        [PRODUCT_DELETE_ERROR]: state => {
            state.status = "error";
        },
        [PRODUCT_DELETE_SUCCESS]: (state, data) => {
            state.status = "success";
        },

    },
    actions: {
        [PRODUCT_GET]: ({commit}) => {
            commit(PRODUCT_GET);
            return new Promise((resolve, reject) => {
                axios.get('/api/v1/products')
                    .then(({data}) => {
                        commit(PRODUCT_GET_SUCCESS, data.result);
                        resolve(data);
                    })
                    .catch((error) => {
                        commit(PRODUCT_GET_ERROR);
                        reject(error.response);
                    });

            });
        },
        [PRODUCT_STORE]: ({ commit}, {...form}) => {
            commit(PRODUCT_STORE);
            return new Promise((resolve, reject) => {
                axios.post('/api/v1/products', form)
                    .then(({data}) => {
                        commit(PRODUCT_STORE_SUCCESS, data.result);
                        resolve(data);
                    })
                    .catch((error) => {
                        commit(PRODUCT_STORE_ERROR);
                        let errors = errorExtractor(error);
                        reject(errors);
                    });

            });

        },
        [PRODUCT_UPDATE]: ({ commit}, data) => {
            commit(PRODUCT_UPDATE);
            return new Promise((resolve, reject) => {
                axios.put(`/api/v1/products/${data.id}`, data.form)
                    .then(({data}) => {
                        commit(PRODUCT_UPDATE_SUCCESS, data.result);
                        resolve(data);
                    })
                    .catch((error) => {
                        commit(PRODUCT_UPDATE_ERROR);
                        let errors = errorExtractor(error);
                        reject(errors);
                    });

            });

        },
        [PRODUCT_DELETE]: ({dispatch, commit}, data) => {
            commit(PRODUCT_DELETE);
            return new Promise((resolve, reject) => {
                axios.delete(`/api/v1/products/${data.id}`)
                    .then(({data}) => {
                        commit(PRODUCT_DELETE_SUCCESS);
                        dispatch(PRODUCT_GET);
                        resolve(data);
                    })
                    .catch((error) => {
                        commit(PRODUCT_DELETE_ERROR);
                        reject(error.response);
                    });

            });

        },

    }
}
