import { reject } from "lodash";
import {
    PAYMENT,
    PAYMENT_SUCCESS,
    PAYMENT_ERROR,
    CANCEL_PAYMENT,
    CANCEL_PAYMENT_SUCCESS,
    CANCEL_PAYMENT_ERROR,
    PAYMENT_OPTIONS,
    SAVE_OPTION
} from "../actions/payment";
// import Vue from "vue";
// import {errorExtractor} from "../reusable-functions";

export default {
    state: {
        data: null,
        status: "",
        payment_options: []
    },
    getters: {
        getPaymentData: state => state.data,
        getPaymentStatus: state => state.status,
        getPaymentOptions: state => state.payment_options
    },
    actions: {
        [PAYMENT]: ({commit}, postData) => {
            commit(PAYMENT);
            return new Promise((resolve, reject) => {
                axios.post('/api/v1/payment', postData)
                    .then(({data}) => {
                        commit(PAYMENT_SUCCESS, data.result);
                        resolve(data);
                    })
                    .catch((error) => {
                        commit(PAYMENT_ERROR);
                        reject(error.response);
                    });

            });

        },
        [PAYMENT_OPTIONS]: ({commit}) => {
            commit(PAYMENT);
            return new Promise((resolve, reject) => {
                axios.get('/api/v1/paymentoptions')
                .then(({data})=>{
                    commit(PAYMENT_OPTIONS, data.result)
                    resolve(data);
                })
                .catch((error)=>{
                    reject(error.response);
                })
            })
        },
        [SAVE_OPTION]: ({commit}, data) => {
            commit(PAYMENT);
            return new Promise((resolve, reject) => {
                axios.patch('/api/v1/paymentoption', data)
                .then(({data})=>{
                    commit(SAVE_OPTION, data.result);
                    resolve(data);
                })
                .catch((error)=>{
                    reject(error.response);
                })
            });
        },
        [CANCEL_PAYMENT]: ({commit}, postData) => {
            commit(CANCEL_PAYMENT);
            return new Promise((resolve, reject) => {
                axios.post('/api/v1/cancelpayment', postData)
                    .then(({data}) => {
                        commit(CANCEL_PAYMENT_SUCCESS, data.result);
                        resolve(data);
                    })
                    .catch((error) => {
                        commit(CANCEL_PAYMENT_ERROR);
                        reject(error.response);
                    });

            });

        },
    },
    mutations: {
        [PAYMENT]: state => {
            state.status = "processing";
        },
        [PAYMENT_SUCCESS]: (state, data) => {
            state.status = data.status;
            state.data = data.remarks;
        },
        [PAYMENT_ERROR]: state => {
            state.status = "error";
        },
        [CANCEL_PAYMENT]: state => {
            state.status = "processing";
        },
        [CANCEL_PAYMENT_SUCCESS]: (state, data) => {
            state.status = data.status;
            state.data = data.remarks;
        },
        [CANCEL_PAYMENT_ERROR]: state => {
            state.status = "error";
        },
        [PAYMENT_OPTIONS]: (state, data) => {
            state.status = "";
            state.payment_options = data;
        },
        [SAVE_OPTION]: (state) => {
            state.status = "processing"
        },
        clear(state) {
            state.status = "";
            state.data = null;
        }
    }
}