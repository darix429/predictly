import {
    PREDICTIONS_GET,
    PREDICTIONS_GET_SUCCESS,
    PREDICTIONS_GET_ERROR,
    PREDICTION_ACTIVE,
    SESSION_ATTACH_PREDICTION,
    SESSION_PREDICTION_SUCCESS,
    SESSION_PREDICTION_ERROR
} from "../actions/prediction";
import Vue from "vue";

export default {
    state: {
        predictions: [],
        status: ""
    },
    getters: {
        getPredictions: state => state.predictions,
    },
    mutations: {
        [PREDICTIONS_GET]: state => {
            state.status = "loading";
        },
        [PREDICTIONS_GET_SUCCESS]: (state, data) => {
            state.predictions = data;
            state.predictions.map(function (item) {
                if(item.name === 'Average') {
                    item.class = 'active';
                } else {
                    item.class = '';
                }

                return item
            });
            state.status = "success";
        },
        [PREDICTION_ACTIVE]: (state, projection) => {
            const newPredictions = state.predictions.map(item => {
                item.class =  projection.id === item.id ? 'active' : '';
                return item;
            });
            Vue.set(state, 'predictions', newPredictions);
        },
        [PREDICTIONS_GET_ERROR]: state => {
            state.status = "error";
        },
        [SESSION_PREDICTION_SUCCESS]: (state, data) => {
            state.status = "success";
        },
        [SESSION_PREDICTION_ERROR]: state => {
            state.status = "error";
        },
    },
    actions: {
        [PREDICTIONS_GET]: ({commit}) => {
            commit(PREDICTIONS_GET);
            return new Promise((resolve, reject) => {
                axios.get('/api/v1/predictions')
                    .then(({data}) => {
                        commit(PREDICTIONS_GET_SUCCESS, data.result);
                        resolve(data);
                    })
                    .catch(() => {
                        commit(PREDICTIONS_GET_ERROR);
                        reject(error.response);
                    });

            });
        },
        [SESSION_ATTACH_PREDICTION]:
            ({commit, dispatch, getters}, data) => {
                return new Promise((resolve, reject) => {
                    axios.patch(`/api/v1/sessions/${data.sessionId}/predictions`, {predictionId: data.predictionId})
                        .then(({data}) => {
                            commit(SESSION_PREDICTION_SUCCESS, data);
                            resolve(data);
                        })
                        .catch(error => {
                            commit(SESSION_PREDICTION_ERROR, error);
                            reject(error.response);
                        });

                });
            },
    }
}
