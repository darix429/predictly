import { GET_MARKETPLACES, MARKETPLACES_ERROR, SESSION_ATTACH_MARKETPLACE, SESSION_MARKETPLACE_SUCCESS, SESSION_MARKETPLACE_ERROR } from "../actions/marketplace";

export default {
    state: {
        marketplaces: [],
        status: ""
    },
    getters: {
        getMarketplaces: state => state.marketplaces,
    },
    mutations: {
        [GET_MARKETPLACES]: (state, data) => {
            state.marketplaces = data.result;
            state.status = "success";
        },
        [MARKETPLACES_ERROR]: state => {
            state.status = "error";
        },
        [SESSION_MARKETPLACE_SUCCESS]: (state, data) => {
            state.status = "success";
        },
        [SESSION_MARKETPLACE_ERROR]: state => {
            state.status = "error";
        },
    },
    actions: {
        [GET_MARKETPLACES]: ({ commit}) => {
            axios.get('/api/v1/marketplaces')
                .then(({data}) => {
                    commit(GET_MARKETPLACES, data);
                })
                .catch(() => {
                    commit(MARKETPLACES_ERROR);
                });
        },
        [SESSION_ATTACH_MARKETPLACE]:
            ({commit, dispatch, getters}, data) => {
                return new Promise((resolve, reject) => {
                    axios.patch(`/api/v1/sessions/${data.sessionId}/marketplaces`, {marketplaceId: data.marketplace.id})
                        .then(({data}) => {
                            commit(SESSION_MARKETPLACE_SUCCESS, data);
                            resolve(data);
                        })
                        .catch(error => {
                            commit(SESSION_MARKETPLACE_ERROR, error);
                            reject(error.response);
                        });

                });
            },
    }
}
