import {
    SESSION_ERROR,
    SESSION_ACTIVE,
    SESSION_SUCCESS,
    SESSION_ATTACH_PRODUCT,
    SESSION_DETACH_PRODUCT,
    SESSION_CALCULATE,
    SESSION_CALCULATE_SUCCESS,
    SESSION_CALCULATE_ERROR,
    SESSION_LAST,
    SESSION_TUNE,
    SESSION_SELECT_PRODUCT,
    SESSION_DELETE_LAST_PRODUCT,
    SHOW_APPLY_BUTTON
} from "../actions/session";
import {errorExtractor} from "../reusable-functions";

export default {
    state: {
        sessionId: "",
        status: "",
        statusCalculate: "",
        sessionProducts: [],
        sessionMarketplace: null,
        sessionMarketplaceId: null,
        sessionPrediction: null,
        sessionPredictionId: null,
        sessionPredictionName: null,
        calculateResult: null,
        lastSession: null,
        lastPredictionName: null,
        sessionResults: null,
        lastSessionProducts: [],
        lastMarketplace: null,
        lastPrediction: null,
        showApplyButton: false
    },
    getters: {
        getSessionId: state => state.sessionId,
        getSessionProducts: state => state.sessionProducts,
        getSessionMarketplace: state => state.sessionMarketplace,
        getSessionMarketplaceId: state => state.sessionMarketplaceId,
        getSessionPrediction: state => state.sessionPrediction,
        getSessionPredictionId: state => state.sessionPredictionId,
        getSessionPredictionName: state => state.sessionPredictionName,
        getCalculateResult: state => state.calculateResult,
        getStatusCalculate: state => state.statusCalculate,
        getLastSession: state => state.lastSession,
        getLastPredictionName: state => state.lastPredictionName,
        getSessionResults: state => state.sessionResults,
        getLastSessionProducts: state => state.lastSessionProducts,
        getLastMarketplace: state => state.lastMarketplace,
        getLastPrediction: state => state.lastPrediction,
        getShowApplyButton: state => state.showApplyButton
    },
    mutations: {
        [SESSION_ACTIVE]:
            state => {
                state.status = "loading";
            },
        [SESSION_ATTACH_PRODUCT]:
            state => {
                state.status = "loading";
            },
        [SESSION_SELECT_PRODUCT]:(state, products) => {
            state.lastSessionProducts.push(...products);
            state.showApplyButton = true;
        },
        [SESSION_DELETE_LAST_PRODUCT]:(state, product) => {
            const index = state.lastSessionProducts.findIndex((elem) => elem.id === product.id);
            state.lastSessionProducts.splice(index, 1);

            const indexSession = state.lastSession.sessionResults.findIndex((elem) => elem.product.id === product.id);
            state.lastSession.sessionResults.splice(indexSession, 1);

            state.showApplyButton = false;
        },
        [SHOW_APPLY_BUTTON]: (state, bool) => {
            state.showApplyButton = bool;
        },
        [SESSION_DETACH_PRODUCT]:
            state => {
                state.status = "loading";
            },
        [SESSION_SUCCESS]:
            (state, data) => {
                state.status = "success";
                state.sessionId = data.result.id;
                state.sessionMarketplace = data.result.marketplace;
                state.sessionMarketplaceId = data.result.marketplace ? data.result.marketplace.id : 1;
                state.sessionProducts = data.result.products;
                state.sessionPrediction = data.result.prediction;
                state.sessionPredictionId = data.result.prediction ? data.result.prediction.id : null;
                state.sessionPredictionName = data.result.prediction ? data.result.prediction.name : null;
            },
        [SESSION_ERROR]:
            state => {
                state.status = "error";
            },
        [SESSION_CALCULATE]:
            state => {
                state.statusCalculate = "loading";
            },
        [SESSION_CALCULATE_SUCCESS]:
            (state, data) => {
                state.statusCalculate = "success";
                state.calculateResult = data.result;
                state.sessionResults = data.result.sessionResults
            },
        [SESSION_CALCULATE_ERROR]:
            state => {
                state.statusCalculate = "error";
            },
        [SESSION_LAST]:
            (state, data) => {
                state.status = "success";
                state.lastSession = data.result;
                state.lastSessionProducts = data.result.products;
                state.lastPredictionName = data.result.prediction ? data.result.prediction.name : null;
                state.lastPrediction = data.result.prediction;
                state.lastMarketplace = data.result.marketplace;
                state.sessionResults = data.result.sessionResults
            },
        [SESSION_TUNE]:
            (state, data) => {

            },
    },
    actions: {
        [SESSION_ACTIVE]:
            ({commit}) => {
                return new Promise((resolve, reject) => {
                    commit(SESSION_ACTIVE);
                    axios.get('/api/v1/sessions/active')
                        .then(({data}) => {
                            commit(SESSION_SUCCESS, data);
                            resolve(data);
                        })
                        .catch(error => {
                            commit(SESSION_ERROR, error);
                            reject(error.response);
                        });

                });
            },
        [SESSION_ATTACH_PRODUCT]:
            ({commit}, data) => {
                return new Promise((resolve, reject) => {
                    commit(SESSION_ATTACH_PRODUCT);
                    axios.patch(`/api/v1/sessions/${data.sessionId}/products`, {productId: data.product.id})
                        .then(({data}) => {
                            commit(SESSION_SUCCESS, data);
                            resolve(data);
                        })
                        .catch(error => {
                            commit(SESSION_ERROR, error);
                            let errors = errorExtractor(error);
                            reject(errors);
                        });

                });
            },
        [SESSION_DETACH_PRODUCT]:
            ({commit}, data) => {
                return new Promise((resolve, reject) => {
                    commit(SESSION_DETACH_PRODUCT);
                    axios.delete(`/api/v1/sessions/${data.sessionId}/products`, {data: {productId: data.product.id}})
                        .then(({data}) => {
                            commit(SESSION_SUCCESS, data);
                            resolve(data);
                        })
                        .catch(error => {
                            commit(SESSION_ERROR, error);
                            reject(error.response);
                        });

                });
            },
        [SESSION_CALCULATE]:
            ({commit, dispatch}, data) => {
                return new Promise((resolve, reject) => {
                    commit(SESSION_CALCULATE);
                    axios({
                        method: 'post',
                        url: `/api/v1/sessions/${data.sessionId}/calculate`,
                        timeout: 180000, // Let's say you want to wait at least 180 seconds
                    }).then(({data}) => {
                        setTimeout(function(){
                            commit(SESSION_CALCULATE_SUCCESS, data);
                            resolve(data);
                        }, 1000);
                        })
                        .catch(error => {
                            commit(SESSION_CALCULATE_ERROR, error);
                            reject(error.response);
                        });

                });
            },
        [SESSION_TUNE]:
            ({commit, dispatch}, data) => {
                return new Promise((resolve, reject) => {
                    axios.post(`/api/v1/sessions/${data.sessionId}/tune`, {factors: data.factors})
                        .then(({data}) => {
                            // let sessionId =  data.result.id;
                            // dispatch(SESSION_CALCULATE, { sessionId });
                            resolve(data);
                        })
                        .catch(error => {
                            reject(error.response);
                        });

                });
            },
        [SESSION_LAST]:
            ({commit}) => {
                return new Promise((resolve, reject) => {
                    axios.get('/api/v1/sessions/last')
                        .then(({data}) => {
                            commit(SESSION_LAST, data);
                            // commit(SESSION_SUCCESS, data);
                            resolve(data);
                        })
                        .catch(error => {
                            commit(SESSION_ERROR, error);
                            reject(error.response);
                        });

                });
            },
    }
}
