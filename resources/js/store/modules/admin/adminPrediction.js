import {GET_ADMIN_PREDICTIONS, GET_SINGLE_PREDICTION} from "../../actions/admin/adminPrediction";

export default {
    state: {
        predictions: [],
        singlePrediction: {}
    },
    getters: {
        getAdminPredictions: state => state.predictions,
        getSinglePrediction: state => state.singlePrediction
    },
    mutations: {
        [GET_ADMIN_PREDICTIONS]: (state, data) => {
            state.predictions = data.result;
        },
        [GET_SINGLE_PREDICTION]: (state, data) => {
            state.singlePrediction = data.result;
        }
    },
    actions:{
        [GET_ADMIN_PREDICTIONS]: ({commit}, category) => {
            return new Promise((resolve, reject) => {
                axios.get(`/api/v1/admin/predictions?category=${category}`)
                    .then(({data}) => {
                        commit(GET_ADMIN_PREDICTIONS, data);
                        resolve(data);
                    })
                    .catch(error => {
                        reject(error.response);
                    });

            });
        },
        [GET_SINGLE_PREDICTION]: ({commit}, id) => {
            return new Promise((resolve, reject) => {
                axios.get(`/api/v1/admin/predictions/${id}`, )
                    .then(({data}) => {
                        commit(GET_SINGLE_PREDICTION, data);
                        resolve(data);
                    })
                    .catch(error => {
                        reject(error.response);
                    });

            });
        }
    }
}
