import {GET_ADMIN_MARKETPLACES, STORE_MARKETPLACE, GET_SINGLE_MARKETPLACE} from "../../actions/admin/adminMarketplace";

export default {
    state: {
        marketplaces: [],
        singleMarketplace: {}
    },
    getters: {
        getAdminMarketplaces: state => state.marketplaces,
        getSingleMerketplace: state => state.singleMarketplace
    },
    mutations: {
        [GET_ADMIN_MARKETPLACES]: (state, data) => {
            state.marketplaces = data.result;
        },
        [GET_SINGLE_MARKETPLACE]: (state, data) => {
            state.singleMarketplace = data.result;
        }
    },
    actions:{
        [GET_ADMIN_MARKETPLACES]: ({commit}, category) => {
            return new Promise((resolve, reject) => {
                axios.get(`/api/v1/admin/marketplaces?category=${category}`)
                    .then(({data}) => {
                        commit(GET_ADMIN_MARKETPLACES, data);
                        resolve(data);
                    })
                    .catch(error => {
                        reject(error.response);
                    });

            });
        },
        [STORE_MARKETPLACE]: ({commit, dispatch}, data) => {
            return new Promise((resolve, reject) => {
                axios.post(`/api/v1/admin/marketplaces`, data)
                    .then(({data}) => {
                        dispatch(GET_ADMIN_MARKETPLACES, data.result.category);
                        resolve(data);
                    })
                    .catch(error => {
                        reject(error.response);
                    });

            });
        },
        [GET_SINGLE_MARKETPLACE]: ({commit}, id) => {
            return new Promise((resolve, reject) => {
                axios.get(`/api/v1/admin/marketplaces/${id}`, )
                    .then(({data}) => {
                        commit(GET_SINGLE_MARKETPLACE, data);
                        resolve(data);
                    })
                    .catch(error => {
                        reject(error.response);
                    });

            });
        }
    }
}
