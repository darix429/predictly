import {GET_USERS, VERIFY_USER, UNVERIFY_USER, GET_SINGLE_USER, DELETE_USER, SWITCH_FREE_USER} from "../../actions/admin/adminUsers";

export default {
    state: {
        users: [],
        user: {},
        getUserParam: {
            limit: 10,
            offset: 1
        },
    },
    getters: {
        getUsers: state => state.users,
        getUser: state => state.user
    },
    mutations: {
        [GET_USERS]: (state, data) => {
            state.users = data.result;
        },
        [GET_SINGLE_USER]: (state, data) => {
            state.user = data.result;
        },
    },
    actions: {
        [GET_USERS]: ({commit}, data) => {
            return new Promise((resolve, reject) => {
                axios.get(`/api/v1/admin/users?offset=${data.offset}&limit=${data.limit}`)
                    .then(({data}) => {
                        commit(GET_USERS, data);
                        resolve(data);
                    })
                    .catch(error => {
                        reject(error.response);
                    });

            });
        },
        [GET_SINGLE_USER]: ({commit}, id) => {
            return new Promise((resolve, reject) => {
                axios.get(`/api/v1/admin/users/${id}`)
                    .then(({data}) => {
                        commit(GET_SINGLE_USER, data);
                        resolve(data);
                    })
                    .catch(error => {
                        reject(error.response);
                    });

            });
        },
        [VERIFY_USER]: ({dispatch, state}, user) => {
            return new Promise((resolve, reject) => {
                axios.patch(`/api/v1/admin/users/${user.id}/verify`)
                    .then(({data}) => {
                        resolve(data);
                    })
                    .catch(error => {
                        reject(error.response);
                    });

            });
        },
        [UNVERIFY_USER]: ({dispatch, state}, user) => {
            return new Promise((resolve, reject) => {
                axios.patch(`/api/v1/admin/users/${user.id}/unverify`)
                    .then(({data}) => {
                        resolve(data);
                    })
                    .catch(error => {
                        reject(error.response);
                    });

            });
        },
        [SWITCH_FREE_USER]: ({dispatch, state}, user) => {
            return new Promise((resolve, reject) => {
                axios.patch(`/api/v1/admin/users/${user.id}/switchFree`)
                .then(({data})=>{
                    resolve(data);
                })
                .catch(error => {
                    reject(error.response);
                });
            })
        },
        [DELETE_USER]: ({dispatch, state}, user) => {
            return new Promise((resolve, reject) => {
                axios.delete(`/api/v1/admin/users/${user.id}`)
                    .then(({data}) => {
                        resolve(data);
                    })
                    .catch(error => {
                        reject(error.response);
                    });

            });
        }
    }
}
