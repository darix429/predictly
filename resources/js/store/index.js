import Vue from "vue";
import Vuex from "vuex";
import user from "./modules/user";
import auth from "./modules/auth";
import marketplace from "./modules/marketplace";
import product from "./modules/product";
import session from "./modules/session";
import prediction from "./modules/prediction";
import payment from "./modules/payment";
import adminUsers from "./modules/admin/adminUsers";
import adminMarketplace from "./modules/admin/adminMarketplace";
import adminPrediction from "./modules/admin/adminPrediction";
Vue.use(Vuex);


export default new Vuex.Store({
    modules: {
        user,
        auth,
        marketplace,
        product,
        payment,
        session,
        prediction,
        adminUsers,
        adminMarketplace,
        adminPrediction
    },
});
