import Vue from 'vue';
import VueI18n from 'vue-i18n';
import APP_CONFIG from '../config/appConfig.js';
import TranslationService from '../services/TranslationService';

Vue.use(VueI18n);

export const i18n = new VueI18n({
    locale: APP_CONFIG.TRANSLATIONS.DEFAULT_LOCALE,
    fallbackLocale: APP_CONFIG.TRANSLATIONS.FALLBACK_LOCALE,
});

export const loadTranslation = () => {
    return TranslationService.loadTranslation()
        .then(data => {
            if (!data) {
                return;
            }

            Object.entries(data).forEach(([key, messages]) => {
                i18n.setLocaleMessage(key, messages);
            });
        });
};
