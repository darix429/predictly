export function isNumberCommaInsteadDot(n) {
    n = n.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$&,')
    return n;
}
