export default Object.freeze({
    TRANSLATIONS: Object.freeze({
        URL: '/dist/translations.json',
        DEFAULT_LOCALE: 'en',
        FALLBACK_LOCALE: 'en',
    }),
    ICONS_SPRITE_LOCATION: '/dist/images/sprite.svg',
});
