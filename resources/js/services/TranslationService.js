import axios from "axios";
import APP_CONFIG from '../config/appConfig';

const TranslationService = {
    loadTranslation() {
        // return axios.get(APP_CONFIG.TRANSLATIONS.URL)
        //     .then(res => res.data);
        return Promise.resolve(translation);
    },
};

export default TranslationService;


const translation = {
    "en": {
        "modal": {
            "header": "MODAL HEADER",
        },
    }
};
