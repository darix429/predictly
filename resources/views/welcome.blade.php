<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Predict your costs, sales, profit, and more with PredictlyPredict your costs, sales, profit, and more with Predictly</title>
    <meta name="description" content="Save on cost. Make more profit. Get x-ray insight on your product research with Predictly" />
    <!-- Fonts -->
    <link rel="icon" href="{{ asset('dist/images/favicon.png') }}" type="image/png"/>
    <link rel="stylesheet" href="{{ mix('dist/css/app.css') }}" >
</head>
<body>
<div id="app">
    <app></app>
</div>
<!-- Stripe -->
<script src="https://js.stripe.com/v3/"></script>
<!-- Chatbot -->
<!-- <script type="text/javascript">!function(e,t,n){function a(){var e=t.getElementsByTagName("script")[0],n=t.createElement("script");n.type="text/javascript",n.async=!0,n.src="https://beacon-v2.helpscout.net",e.parentNode.insertBefore(n,e)}if(e.Beacon=n=function(t,n,a){e.Beacon.readyQueue.push({method:t,options:n,data:a})},n.readyQueue=[],"complete"===t.readyState)return a();e.attachEvent?e.attachEvent("onload",a):e.addEventListener("load",a,!1)}(window,document,window.Beacon||function(){});</script> -->
<!-- <script type="text/javascript">window.Beacon('init', '55e840cf-c935-423b-a2a0-e1ee91e13fc1')</script> -->
<!-- Hotjar Tracking Code for www.predictly.io -->
<!-- <script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:1853661,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script> -->
<!-- ActiveCampain -->
<!-- <script type="text/javascript">
    (function(e,t,o,n,p,r,i){e.visitorGlobalObjectAlias=n;e[e.visitorGlobalObjectAlias]=e[e.visitorGlobalObjectAlias]||function(){(e[e.visitorGlobalObjectAlias].q=e[e.visitorGlobalObjectAlias].q||[]).push(arguments)};e[e.visitorGlobalObjectAlias].l=(new Date).getTime();r=t.createElement("script");r.src=o;r.async=true;i=t.getElementsByTagName("script")[0];i.parentNode.insertBefore(r,i)})(window,document,"https://diffuser-cdn.app-us1.com/diffuser/diffuser.js","vgo");
    vgo('setAccount', '90195134');
    vgo('setTrackByDefault', true);

    vgo('process');
</script> -->
<script src="{{ mix('dist/js/app.js') }}"></script>
</body>
</html>

