<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Processing Authorization....</title>
</head>
<body>
<script>
    const data = {
        type: 'facebook.auth.completed',
        data: {
            code: '{{ $code }}',
        },
    };

    const origin = '{{ $origin }}';

    // alert(JSON.stringify({
    //     message: data,
    //     origin:  origin,
    // }));

    console.log('Sending Post Message', {
        message: data,
        origin:  origin,
    });

    window.opener.postMessage(data, origin);
</script>
</body>
</html>
