@component('mail::message')
    New user {{ $name }} was registered!

    Thanks,

    {{ config('app.name') }}
@endcomponent
