@component('mail::message')
    {{ $email }}

    {{ $question }}

    Thanks,

    {{ config('app.name') }}
@endcomponent
