### Setup

- setup localhost DNS

```bash
echo "127.0.0.1    predictly.loc" | sudo tee -a /etc/hosts
```

- add localhost override of predictly.com in order to be able to use google auth

```bash
echo "127.0.0.1    predictly.com" | sudo tee -a /etc/hosts
```

- setup environment 

```bash
cp .env.example.docker .env
```

- start containers

```bash
docker-compose up --build
```

- install dependencies

```bash
docker-compose exec main composer install
docker-compose exec main yarn install
```

- generate application key

```bash
docker-compose exec main php artisan key:generate
docker-compose exec main php artisan storage:link
```

- setup database

```bash
docker-compose exec main php artisan migrate
docker-compose exec main php artisan db:seed

chmod 777 -R storage bootstrap/cache
```

- create user

```bash
docker-compose exec main php artisan user:create
```

- attach roles and permissions

Command attaches roles and permissions for specific user.\

Next command attach 'administrator' role by default.

```bash
docker-compose exec main php artisan permission:attach {userId}
```

Additional information
```bash
php artisan help permission:attach
```

- build frontend

```bash
docker-compose exec main yarn dev
```

### Flush Docker

```bash
docker-compose rm -f -s -v
```

### Endpoints

- Site: http://predictly.loc
- Site: http://predictly.com
- MailHog: http://localhost:8025
- PHPMyAdmin: http://localhost:8080
