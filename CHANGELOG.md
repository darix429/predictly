## 2020 April 07

    ⚠ Migration Required

##### Updates

- Added Property `competitors` to `Product` model


## 2020 April 06

##### Updates

- Added Property `providers` to `/v1/me` method in order to be able to check linked social accounts

##### New Features

- Added API method `/v1/auth/social/link` to provide ability to link social account

## 2020 April 03 

##### Updates

- Moved API method `/v1/auth/signup` to `/v1/auth/social/signup`
- Moved API method `/v1/auth/signin` to `/v1/auth/social/signin`
- Removed `DevelopmentSeeder` so now database seed can be initiated by command `php artisan db:seed`


##### New Features

- Added API method `/v1/auth/sign-up` to provide ability to sign-up using email and password
- Added API method `/v1/auth/sign-in` to provide ability to sign-in using email and password


- Added API method `/v1/auth/social/sign-up` that duplicates `/v1/auth/social/signup`
- Added API method `/v1/auth/social/sign-in` that duplicates `/v1/auth/social/signin`


- Added Console command `user:create` to provide ability to create users from console

##### Deprecations

- API Method `/v1/auth/social/signup` should be replaced by `/v1/auth/social/sign-up`
- API Method `/v1/auth/social/signin` should be replaced by `/v1/auth/social/sign-in`
