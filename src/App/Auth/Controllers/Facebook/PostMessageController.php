<?php

declare(strict_types=1);

namespace App\Auth\Controllers\Facebook;

use App\Core\Controllers\Controller;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;

class PostMessageController extends Controller
{
    /**
     * @var \Illuminate\Contracts\Config\Repository
     */
    private Repository $repository;

    /**
     * PostMessageController constructor.
     *
     * @param \Illuminate\Contracts\Config\Repository $repository
     */
    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request): Renderable
    {
        return view('auth.facebook.post-message', [
            'code'   => $request->input('code'),
            'origin' => $this->repository->get('app.url'),
        ]);
    }
}
