<?php

declare(strict_types=1);

namespace App\Docs\Controllers;

use App\Core\Controllers\Controller;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;

class SwaggerController extends Controller
{
    private const PATH_TO_YAML_FILE = 'v0.0.1/v0.0.1.yaml';

    /**
     * @var \Illuminate\Foundation\Application
     */
    private Application $application;

    /**
     * @var \Illuminate\Contracts\View\Factory
     */
    private Factory $viewFactory;

    /**
     * @var \Illuminate\Contracts\Routing\UrlGenerator
     */
    private UrlGenerator $urlGenerator;

    /**
     * SwaggerController constructor.
     *
     * @param \Illuminate\Foundation\Application         $application
     * @param \Illuminate\Contracts\View\Factory         $viewFactory
     * @param \Illuminate\Contracts\Routing\UrlGenerator $urlGenerator
     */
    public function __construct(Application $application, Factory $viewFactory, UrlGenerator $urlGenerator)
    {
        $this->application  = $application;
        $this->viewFactory  = $viewFactory;
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * Show the Documentation UI
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index(): View
    {
        $yamlUrl           = $this->urlGenerator->route('docs.swagger.file', [self::PATH_TO_YAML_FILE]);
//        $oAuth2RedirectUrl = $this->urlGenerator->route('docs.swagger.oauth2-redirect');

        return $this->viewFactory->make('docs.swagger.index', [
            'yamlUrl'           => $yamlUrl,
//            'oAuth2RedirectUrl' => $oAuth2RedirectUrl,
        ]);
    }

    /**
     * Render YAML Config File
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $filename
     *
     * @return string
     */
    public function file(Request $request, string $filename): string
    {
        $path    = $this->application->resourcePath("docs/swagger/{$filename}");
        $content = file_get_contents($path);

        $apiEndpoint = env('APP_URL') . '/api';


        $content = str_replace([
            'predictly.api.endpoint',
        ], [
            $apiEndpoint,
        ], $content);

        return $content;
    }
}
