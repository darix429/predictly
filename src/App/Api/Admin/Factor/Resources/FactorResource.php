<?php

declare(strict_types=1);

namespace App\Api\Admin\Factor\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property \Domain\Factor\Factor $resource
 */
class FactorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                   => $this->resource->id,
            'salesPriceAssumption' => $this->resource->sales_price_assumption,
            'salesPriceIncrease'   => $this->resource->sales_price_increase,
            'monthlySales'         => $this->resource->monthly_sales,
            'salesGrowth_Rate'     => $this->resource->sales_growth_rate,
            'stableGrowthRate'     => $this->resource->sales_growth_rate,
        ];
    }
}
