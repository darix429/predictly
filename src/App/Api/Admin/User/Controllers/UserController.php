<?php

declare(strict_types=1);

namespace App\Api\Admin\User\Controllers;

use App\Api\Admin\User\Requests\ListRequest;
use App\Api\Admin\User\Resources\UserResource;
use App\Core\Controllers\Controller;
use App\Core\Response;
use Domain\User\Domain\UserDomain;
use Domain\User\Exception\DeleteYourselfException;
use Domain\User\Exception\UnverifyYourselfException;
use Domain\User\Exception\UserAlreadyVerifiedException;
use Domain\User\Repository\UserRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * @var \Domain\User\Repository\UserRepositoryInterface
     */
    private UserRepositoryInterface $userRepository;

    /**
     * @var \Domain\User\Domain\UserDomain
     */
    private UserDomain $userDomain;

    /**
     * UserController constructor.
     *
     * @param \Domain\User\Repository\UserRepositoryInterface $userRepository
     * @param \Domain\User\Domain\UserDomain                  $userDomain
     */
    public function __construct(UserRepositoryInterface $userRepository, UserDomain $userDomain)
    {
        $this->userRepository = $userRepository;
        $this->userDomain = $userDomain;
    }

    /**
     * @param \App\Api\Admin\User\Requests\ListRequest $request
     *
     * @return \App\Core\Response
     */
    public function index(ListRequest $request): Response
    {
        $users = $this->userRepository->findAll($request);

        $resource = UserResource::collection($users);

        return Response::make($resource);
    }

    /**
     * @param int $userId
     *
     * @return \App\Core\Response
     */
    public function show(int $userId): Response
    {
        $user = $this->userRepository->findOrFail($userId);

        $resource = new UserResource($user);

        return Response::make($resource);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param int                      $userId
     *
     * @return \App\Core\Response
     */
    public function verify(Request $request, int $userId): Response
    {
        $user = $this->userRepository->findOrFail($userId);

        if ($user->verified) {
            throw (new UserAlreadyVerifiedException())->setUser($user);
        }

        $this->userDomain->verify($user);

        return Response::make(true);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param int                      $userId
     *
     * @return \App\Core\Response
     */
    public function unverify(Request $request, int $userId): Response
    {
        $user = $this->userRepository->findOrFail($userId);

        if ($request->user()->id === $user->id) {
            throw new UnverifyYourselfException('You can\'t unverify yourself');
        }

        if (!$user->verified) {
            return Response::make(true);
        }

        $this->userDomain->unverify($user);

        return Response::make(true);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param int                      $userId
     *
     * @return \App\Core\Response
     * @throws \Exception
     */
    public function destroy(Request $request, int $userId): Response
    {
        $user = $this->userRepository->findOrFail($userId);

        if ($request->user()->id === $user->id) {
            throw new DeleteYourselfException('You can\'t delete yourself');
        }

        $this->userDomain->delete($user);

        return Response::make(true);
    }

    
    public function switchFree(Request $request, int $userId): Response
    {
        $user = $this->userRepository->findOrFail($userId);

        $this->userDomain->switchFree($user);

        return Response::make($user["free"]);
    }
}
