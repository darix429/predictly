<?php

declare(strict_types=1);

namespace App\Api\Admin\User\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property \Domain\User\User $resource
 */
class UserResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'            => $this->resource->id,
            'name'          => $this->resource->name,
            'email'         => $this->resource->email,
            'verified'      => $this->resource->verified,
            'free'          => $this->resource->free,
            'registered_at' => $this->resource->created_at->format('d-m-Y'),
        ];
    }
}
