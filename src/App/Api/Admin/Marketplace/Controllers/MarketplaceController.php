<?php

declare(strict_types=1);

namespace App\Api\Admin\Marketplace\Controllers;

use App\Api\Admin\Marketplace\Requests\ListRequest;
use App\Api\Admin\Marketplace\Requests\StoreRequest;
use App\Api\Admin\Marketplace\Requests\UpdateRequest;
use App\Api\Admin\Marketplace\Resources\MarketplaceResource;
use App\Core\Controllers\Controller;
use App\Core\Response;
use Domain\Marketplace\Domain\MarketplaceDomain;
use Domain\Marketplace\Repository\MarketplaceRepositoryInterface;

class MarketplaceController extends Controller
{
    /**
     * @var \Domain\Marketplace\Domain\MarketplaceDomain
     */
    private MarketplaceDomain $marketplaceDomain;

    /**
     * @var \Domain\Marketplace\Repository\MarketplaceRepositoryInterface
     */
    private MarketplaceRepositoryInterface $marketplaceRepository;

    /**
     * MarketplaceController constructor.
     *
     * @param \Domain\Marketplace\Domain\MarketplaceDomain                  $marketplaceDomain
     * @param \Domain\Marketplace\Repository\MarketplaceRepositoryInterface $marketplaceRepository
     */
    public function __construct(
        MarketplaceDomain $marketplaceDomain,
        MarketplaceRepositoryInterface $marketplaceRepository
    ) {
        $this->marketplaceDomain     = $marketplaceDomain;
        $this->marketplaceRepository = $marketplaceRepository;
    }

    /**
     * @param \App\Api\Admin\Marketplace\Requests\ListRequest $request
     *
     * @return \App\Core\Response
     */
    public function index(ListRequest $request): Response
    {
        $category = $request->getCategory();

        if (!$category) {
            $marketplaces = $this->marketplaceRepository->findAll($request);
        } else {
            $marketplaces = $this->marketplaceRepository->findByCategory($request, $category);
        }

        $resource = MarketplaceResource::collection($marketplaces);

        return Response::make($resource);
    }

    /**
     * @param int $marketplaceId
     *
     * @return \App\Core\Response
     */
    public function show(int $marketplaceId): Response
    {
        $marketplace = $this->marketplaceRepository->findOrFail($marketplaceId);

        $resource = new MarketplaceResource($marketplace);

        return Response::make($resource);
    }

    /**
     * @param \App\Api\Admin\Marketplace\Requests\StoreRequest $request
     *
     * @return \App\Core\Response
     */
    public function store(StoreRequest $request): Response
    {
        $marketplace = $this->marketplaceDomain->create($request);

        $resource = new MarketplaceResource($marketplace);

        return Response::make($resource);
    }

    /**
     * @param \App\Api\Admin\Marketplace\Requests\UpdateRequest $request
     * @param int                                               $marketplaceId
     *
     * @return \App\Core\Response
     */
    public function update(UpdateRequest $request, int $marketplaceId): Response
    {
        $marketplace = $this->marketplaceRepository->findOrFail($marketplaceId);

        $result = $this->marketplaceDomain->update($marketplace, $request);

        return Response::make($result);
    }
}
