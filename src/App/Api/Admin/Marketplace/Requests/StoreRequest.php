<?php

declare(strict_types=1);

namespace App\Api\Admin\Marketplace\Requests;

use Domain\Marketplace\Marketplace;
use Domain\Marketplace\Model\StoreMarketplaceModel;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\In;

class StoreRequest extends FormRequest implements StoreMarketplaceModel
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'name'     => ['required', 'string', 'min:3', 'max: 64'], //todo: Check validation
            'fee'      => ['required', 'integer', 'min:1', 'max:100'],
            'category' => ['required', 'string', new In(Marketplace::CATEGORIES)],
        ];
    }

    public function getName(): string
    {
        return (string)$this->input('name');
    }

    /**
     * @inheritDoc
     */
    public function getFee(): int
    {
        return (int)$this->input('fee');
    }

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return (string)$this->input('category');
    }
}
