<?php

declare(strict_types=1);

namespace App\Api\Admin\Marketplace\Requests;

use Domain\Marketplace\Model\UpdateMarketplaceModel;

class UpdateRequest extends StoreRequest implements UpdateMarketplaceModel
{

}
