<?php

declare(strict_types=1);

namespace App\Api\Admin\Prediction\Requests;

use Domain\Prediction\Model\UpdatePredictionModel;

class UpdateRequest extends StoreRequest implements UpdatePredictionModel
{

}
