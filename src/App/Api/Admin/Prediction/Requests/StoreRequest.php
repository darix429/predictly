<?php

declare(strict_types=1);

namespace App\Api\Admin\Prediction\Requests;

use Domain\Prediction\Model\StorePredictionModel;
use Domain\Prediction\Prediction;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\In;

class StoreRequest extends FormRequest implements StorePredictionModel
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        //todo: Check validation rules
        return [
            'name'                 => ['required', 'string', 'min:3', 'max:64'],
            'description'          => ['required', 'string', 'min:3', 'max:64'],
            'category'             => ['required', 'string', new In(Prediction::CATEGORIES)],
            'salesPriceAssumption' => ['required', 'integer', 'min:1', 'max: 100'],
            'salesPriceIncrease'   => ['required', 'integer', 'min:1', 'max: 100'],
            'monthlySales'         => ['required', 'integer', 'min:1', 'max: 100'],
            'salesGrowthRate'      => ['required', 'integer', 'min:1', 'max: 100'],
            'stableGrowthRate'     => ['required', 'integer', 'min:1', 'max: 100'],
        ];
    }

    /**
     * @inheritDoc
     */
    public function getFactorableId(): ?int
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return (string)$this->input('name');
    }

    /**
     * @inheritDoc
     */
    public function getDescription(): string
    {
        return (string)$this->input('description');
    }

    /**
     * @inheritDoc
     */
    public function getCategory(): string
    {
        return (string)$this->input('category');
    }

    /**
     * @inheritDoc
     */
    public function getSalesPriceAssumption(): int
    {
        return (int)$this->input('salesPriceAssumption');
    }

    /**
     * @inheritDoc
     */
    public function getSalesPriceIncrease(): int
    {
        return (int)$this->input('salesPriceIncrease');
    }

    /**
     * @inheritDoc
     */
    public function getMonthlySales(): int
    {
        return (int)$this->input('monthlySales');
    }

    /**
     * @inheritDoc
     */
    public function getSalesGrowsRate(): int
    {
        return (int)$this->input('salesGrowthRate');
    }

    /**
     * @inheritDoc
     */
    public function getStableGrowthRate(): int
    {
        return (int)$this->input('stableGrowthRate');
    }

    /**
     * @inheritDoc
     */
    public function hasProductCost(): bool
    {
        return $this->has('productCost');
    }

    /**
     * @inheritDoc
     */
    public function getProductCost(): ?int
    {
        if (!$this->hasProductCost()) {
            return null;
        }

        return $this->input('productCost');
    }

    /**
     * @inheritDoc
     */
    public function hasMinimumOrderQuantity(): bool
    {
       return $this->has('minimumOrderQuantity');
    }

    /**
     * @inheritDoc
     */
    public function getMinimumOrderQuantity(): ?int
    {
        if (!$this->hasMinimumOrderQuantity()) {
            return null;
        }

        return $this->input('minimumOrderQuantity');
    }
}
