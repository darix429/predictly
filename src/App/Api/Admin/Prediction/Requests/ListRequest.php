<?php

declare(strict_types=1);

namespace App\Api\Admin\Prediction\Requests;

use App\Api\Core\Requests\LimitedRequest;
use App\Api\Core\Requests\OffsetRequest;
use Domain\Prediction\Model\IndexModelInterface;
use Domain\Prediction\Prediction;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\In;

class ListRequest extends FormRequest implements IndexModelInterface
{
    use LimitedRequest, OffsetRequest;

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return \array_merge(
            ['category' => ['string', new In(Prediction::CATEGORIES)]],
            $this->getLimitRules(),
            $this->getOffsetRules());
    }

    /**
     * @return bool
     */
    public function hasCategory(): bool
    {
        return $this->has('category');
    }

    /**
     * @return string|null
     */
    public function getCategory(): ?string
    {
        if (!$this->hasCategory()) {
            return null;
        }

        return (string)$this->input('category');
    }
}
