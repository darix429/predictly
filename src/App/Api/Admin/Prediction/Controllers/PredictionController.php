<?php

declare(strict_types=1);

namespace App\Api\Admin\Prediction\Controllers;

use App\Api\Admin\Prediction\Requests\ListRequest;
use App\Api\Admin\Prediction\Requests\StoreRequest;
use App\Api\Admin\Prediction\Requests\UpdateRequest;
use App\Api\Admin\Prediction\Resources\PredictionResource;
use App\Core\Controllers\Controller;
use App\Core\Response;
use Domain\Prediction\Domain\PredictionDomain;
use Domain\Prediction\Repository\PredictionRepositoryInterface;
use http\Env\Request;

class PredictionController extends Controller
{
    /**
     * @var \Domain\Prediction\Domain\PredictionDomain
     */
    private PredictionDomain $predictionDomain;

    /**
     * @var \Domain\Prediction\Repository\PredictionRepositoryInterface
     */
    private PredictionRepositoryInterface $predictionRepository;

    /**
     * PredictionController constructor.
     *
     * @param \Domain\Prediction\Domain\PredictionDomain                  $predictionDomain
     * @param \Domain\Prediction\Repository\PredictionRepositoryInterface $predictionRepository
     */
    public function __construct(PredictionDomain $predictionDomain, PredictionRepositoryInterface $predictionRepository)
    {
        $this->predictionDomain = $predictionDomain;
        $this->predictionRepository = $predictionRepository;
    }

    /**
     * @param \App\Api\Admin\Prediction\Requests\ListRequest $request
     *
     * @return \App\Core\Response
     */
    public function index(ListRequest $request): Response
    {
        $category = $request->getCategory();

        //todo: Think about filters
        if (!$category) {
            $predictions = $this->predictionRepository->findAll($request);
        } else {
            $predictions = $this->predictionRepository->findByCategory($request, $category);
        }

        $resource = PredictionResource::collection($predictions);

        return Response::make($resource);
    }

    /**
     * @param int $predictionId
     *
     * @return \App\Core\Response
     */
    public function show(int $predictionId): Response
    {
        $prediction = $this->predictionRepository->findOrFail($predictionId);

        $resource = new PredictionResource($prediction);

        return Response::make($resource);
    }

    /**
     * @param \App\Api\Admin\Prediction\Requests\StoreRequest $request
     *
     * @return \App\Core\Response
     */
    public function store(StoreRequest $request): Response
    {
        $prediction = $this->predictionDomain->create($request);

        $resource = new PredictionResource($prediction);

        return Response::make($resource);
    }

    /**
     * @param \App\Api\Admin\Prediction\Requests\UpdateRequest $request
     * @param int                                              $predictionId
     *
     * @return \App\Core\Response
     */
    public function update(UpdateRequest $request, int $predictionId): Response
    {
        $prediction = $this->predictionRepository->findOrFail($predictionId);

        $result = $this->predictionDomain->update($prediction, $request);

        return Response::make($result);
    }

    /**
     * @param int $predictionId
     *
     * @return \App\Core\Response
     * @throws \Exception
     */
    public function destroy(int $predictionId): Response
    {
        $prediction = $this->predictionRepository->findOrFail($predictionId);

        $this->predictionDomain->delete($prediction);

        return Response::make(true);
    }
}
