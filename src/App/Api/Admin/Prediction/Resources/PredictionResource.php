<?php

declare(strict_types=1);

namespace App\Api\Admin\Prediction\Resources;

use App\Api\Admin\Factor\Resources\FactorResource;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property \Domain\Prediction\Prediction $resource
 */
class PredictionResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->resource->id,
            'name'        => $this->resource->name,
            'description' => $this->resource->description,
            'category'    => $this->resource->category,
            'factors'     => $this->getFactors(),
        ];
    }

    /**
     * @return \App\Api\Admin\Factor\Resources\FactorResource
     */
    private function getFactors(): ?FactorResource
    {
        $factor = $this->resource->factor()->first();

        if (!$factor) {
            return null;
        }

        return new FactorResource($factor);
    }
}
