<?php

declare(strict_types=1);

namespace App\Api\Support\Requests;

use Domain\Support\Enum\Topic;
use Domain\Support\Model\SendFeedbackModel;
use Illuminate\Foundation\Http\FormRequest;

class SendFeedbackRequest extends FormRequest implements SendFeedbackModel
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'topic'   => ['required', 'string', Topic::getValidationRule()],
            'message' => ['required', 'string', 'min:3', 'max:320'],
        ];
    }

    /**
     * @inheritDoc
     */
    public function getTopic(): string
    {
        return (string)$this->input('topic');
    }

    /**
     * @inheritDoc
     */
    public function getMessage(): string
    {
        return (string)$this->input('message');
    }
}
