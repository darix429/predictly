<?php

declare(strict_types=1);

namespace App\Api\Support\Requests;

use Domain\Support\Model\SendQuestionModel;
use Illuminate\Foundation\Http\FormRequest;

class SendQuestionRequest extends FormRequest implements SendQuestionModel
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email'    => ['required', 'string', 'email'],
            'question' => ['required', 'string', 'min:3', 'max:320'],
        ];
    }

    /**
     * @inheritDoc
     */
    public function getEmail(): string
    {
        return (string)$this->input('email');
    }

    /**
     * @inheritDoc
     */
    public function getQuestion(): string
    {
        return (string)$this->input('question');
    }
}
