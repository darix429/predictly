<?php

declare(strict_types=1);

namespace App\Api\Support\Controllers;

use App\Api\Support\Mail\FeedbackSent;
use App\Api\Support\Requests\SendFeedbackRequest;
use App\Api\Support\Requests\SendQuestionRequest;
use App\Api\Support\Resources\FeedbackTopicsResource;
use App\Core\Controllers\Controller;
use App\Core\Response;
use Domain\Mail\Service\MailSenderInterface;
use Domain\Support\Enum\Topic;
use Illuminate\Support\Collection;

class SupportController extends Controller
{
    /**
     * @var \Domain\Mail\Service\MailSenderInterface
     */
    private MailSenderInterface $mailSender;

    /**
     * SupportController constructor.
     *
     * @param \Domain\Mail\Service\MailSenderInterface $mailSender
     */
    public function __construct(MailSenderInterface $mailSender)
    {
        $this->mailSender = $mailSender;
    }

    /**
     * @return \App\Core\Response
     */
    public function feedbackList(): Response
    {
        $resource = FeedbackTopicsResource::collection(Topic::getCollection());

        return Response::make($resource);
    }

    /**
     * @param \App\Api\Support\Requests\SendFeedbackRequest $request
     *
     * @return \App\Core\Response
     */
    public function sendFeedback(SendFeedbackRequest $request): Response
    {
        $this->mailSender->sendSupportFeedbackMessage($request);

        return Response::make(true);
    }

    /**
     * @param \App\Api\Support\Requests\SendQuestionRequest $request
     *
     * @return \App\Core\Response
     */
    public function sendQuestion(SendQuestionRequest $request): Response
    {
        $this->mailSender->sendQuestionMessage($request);

        return Response::make(true);
    }
}
