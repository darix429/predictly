<?php

declare(strict_types=1);

namespace App\Api\Support\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property-read  \Domain\Support\Enum\Topic $resource
 */
class FeedbackTopicsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->resource->getValue(),
        ];
    }
}
