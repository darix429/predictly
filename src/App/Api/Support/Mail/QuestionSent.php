<?php

declare(strict_types=1);

namespace App\Api\Support\Mail;

use Domain\Support\Model\SendQuestionModel;
use Illuminate\Mail\Mailable;

class QuestionSent extends Mailable
{
    /**
     * @var \Domain\Support\Model\SendQuestionModel
     */
    private SendQuestionModel $model;

    /**
     * Create a new message instance.
     *
     * @param \Domain\Support\Model\SendQuestionModel $model
     */
    public function __construct(SendQuestionModel $model)
    {
        $this->model = $model;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('Received New Question From '.$this->model->getEmail())
            ->markdown('emails.support.question')
            ->with([
                'email'    => $this->model->getEmail(),
                'question' => $this->model->getQuestion(),
            ]);
    }
}
