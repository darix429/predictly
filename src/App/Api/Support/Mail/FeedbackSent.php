<?php

declare(strict_types=1);

namespace App\Api\Support\Mail;

use Domain\Support\Model\SendFeedbackModel;
use Illuminate\Mail\Mailable;

class FeedbackSent extends Mailable
{
    /**
     * @var \Domain\Support\Model\SendFeedbackModel
     */
    private SendFeedbackModel $model;

    /**
     * Create a new message instance.
     *
     * @param \Domain\Support\Model\SendFeedbackModel $model
     */
    public function __construct(SendFeedbackModel $model)
    {
        $this->model = $model;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject($this->model->getTopic())
            ->markdown('emails.support.feedback')
            ->with([
                'message' => $this->model->getMessage(),
            ]);
    }
}
