<?php

namespace App\Api\Payment\Controllers;

use App\Core\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Core\Response;
use Config;

use Domain\Subscription\Subscription;

class PaymentController extends Controller
{
    private $stripe;

    public function __construct() {

        \Stripe\Stripe::setApiKey(config('stripe.key'));
        $this->stripe = new \Stripe\StripeClient(config('stripe.secret'));
    }

    public function paymentOptions(Request $request) {
        $collection = Subscription::all();

        return Response::make($collection);
    }

    public function updatePaymentOption(Request $request) {

        $data = $request->input();

        $paymentOption = Subscription::where("id",$data["id"])
        ->update($data);

        return Response::make($paymentOption);
    }

    public function payment(Request $request) {
            $data = $request->input();
            $user = $request->user();
            $token = $data["token"];

            $subscription = DB::table('subscriptions')->find($data["subscription_id"]);

            if ($this->isSubscribed($user, $data["subscription_id"])) {
                return Response::make(array(
                    "status"=>"WARNING",
                    "remarks"=>"You have already paid for this product."
                ));
            }

            $price = str_replace(".", "", $subscription->price); $remarks = null; $status = "SUCCESS";

            try {
                $customer = $this->stripe->customers->create([
                    "email"=>$user["email"],
                    "source"=>$token["id"]
                ]);

                $charge = $this->stripe->charges->create([
                    "amount"=>$price,
                    "currency"=>"usd",
                    "description"=>$subscription->name,
                    "customer"=>$customer->id
                ]);


                if ($charge->error) {
                    Response::make($charge);
                }


                if ($charge->paid) {
                    DB::table('stripe_payment')->insert([
                        "user_id"=>$user["id"],
                        "subscription_id"=>$subscription->id,
                        "reference_id"=>$charge->id,
                        "status"=>$status,
                        "remarks"=>$remarks
                    ]);

                    $expiry = Date("Y-m-d H:i:s", strtotime("+". $subscription->duration_days ." days"));
                    $user_subscription_id = DB::table('user_subscriptions')->insertGetId([
                        "user_id"=>$user["id"],
                        "subscription_id"=>$subscription->id,
                        "status"=>"ACTIVE",
                        "expiry_date"=>$expiry
                    ]);

                    $remarks = "Success! Your ". $subscription->name . " starts today.";
                } else {
                    $status = "FAILED";
                    $remarks = "Credit Card Payment Failed. Please contact administrator.";
                }

            } catch(\Stripe\Exception\CardException $e) {
                $status = "CARD DENIED";
                $remarks = $e->getError()->message;
            } catch (\Stripe\Exception\InvalidRequestException $e) {
                $status = "FAILED";
                $remarks = $e->getError()->message;
            } catch (\Stripe\Exception\AuthenticationException $e) {
                $status = "FAILED";
                $remarks = $e->getError()->message;
            } catch (\Stripe\Exception\ApiConnectionException $e) {
                $status = "FAILED";
                $remarks = $e->getError()->message;
            } catch (\Illuminate\Database\QueryException $e) {
                $status = "ERROR";
                $remarks = $e;
            } catch (Exception $e) {
                $status = "ERROR";
                $remarks = $e->getError()->message;
            }
            
            return Response::make(array(
                "status"=>$status,
                "remarks"=>$remarks
            ));
                

    }

    public function processCancellation(Request $request) {
        $data = $request->input();
        $user = $request->user();
        $subscription_id = $data["subscription_id"];
        $status = "SUCCESS"; $remarks = "";
        try {

            $filter = [
                "user_id"=>$user["id"],
                "subscription_id"=>$subscription_id,
                "status"=>"SUCCESS"
            ];

            $payments = DB::table('stripe_payment')->where($filter)->first();
    
            $refund = $this->stripe->refunds->create([
                'charge'=>$payments->reference_id
            ]);

            $updatePayment = DB::table('stripe_payment')->where($filter)->update(["status"=>"REFUNDED"]);
            $filter["status"] = "ACTIVE";
            $updateSubscription = DB::table('user_subscriptions')->where($filter)->update(["status"=>"CANCELLED"]);

            $remarks = "$subscription_id : Cancellation Success";

        } catch (\Stripe\Exception\InvalidRequestException $e) {
            $status = "ERROR";
            $remarks = "Charge has already been refunded";
        } catch (Exception $e) {
            $status = "ERROR";
            $remarks = $e->getError()->message;
        }

        return Response::make(array(
            "status"=>$status,
            "remarks"=>$remarks
        ));
    }

    public function isSubscribed($user, $subscription_id) {
        return DB::table('user_subscriptions')->where([
            "user_id"=>$user["id"],
            "subscription_id"=>$subscription_id,
            "status"=>"ACTIVE"
        ])->count();
    }

}
