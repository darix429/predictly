<?php

declare(strict_types=1);

namespace App\Api\Marketplace\Resources;

use Domain\Marketplace\Marketplace;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property \Domain\Marketplace\Marketplace $resource
 */
class MarketplaceResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'       => $this->resource->id,
            'name'     => $this->resource->name,
            'fee'      => $this->resource->fee,
            'category' => $this->resource->category,
        ];
    }
}
