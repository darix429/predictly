<?php

declare(strict_types=1);

namespace App\Api\Marketplace\Controllers;

use App\Api\Marketplace\Requests\ListRequest;
use App\Api\Marketplace\Resources\MarketplaceResource;
use App\Core\Controllers\Controller;
use App\Core\Response;
use Domain\Marketplace\Repository\MarketplaceRepository;
use Domain\Marketplace\Repository\MarketplaceRepositoryInterface;

/**
 * Class MarketplaceController
 *
 * @package App\Api\Marketplace\Controllers
 */
class MarketplaceController extends Controller
{
    /**
     * @var \Domain\Marketplace\Repository\MarketplaceRepositoryInterface
     */
    private MarketplaceRepositoryInterface $marketplaceRepository;

    /**
     * MarketplaceController constructor.
     *
     * @param \Domain\Marketplace\Repository\MarketplaceRepositoryInterface $marketplaceRepository
     */
    public function __construct(MarketplaceRepositoryInterface $marketplaceRepository)
    {
        $this->marketplaceRepository = $marketplaceRepository;
    }

    /**
     * @param \App\Api\Marketplace\Requests\ListRequest $request
     *
     * @return \App\Core\Response
     */
    public function index(ListRequest $request): Response
    {
        $marketplaces = $this->marketplaceRepository->findAll($request);

        $resource = MarketplaceResource::collection($marketplaces);

        return Response::make($resource);
    }
}
