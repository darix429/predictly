<?php

declare(strict_types=1);

namespace App\Api\Session\Resources;

use App\Api\Marketplace\Resources\MarketplaceResource;
use App\Api\Prediction\Resources\PredictionResource;
use App\Api\Product\Resources\ProductResource;
use App\Api\Session\Resources\SessionResult\SessionResultResource;
use Domain\Session\Result\SessionResult;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property-read \Domain\Session\Session $resource
 */
class SessionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'             => $this->resource->id,
            'marketplace'    => $this->getMarketplace(),
            'prediction'     => $this->getPrediction(),
            'products'       => $this->getProducts(),
            'sessionResults' => $this->getSessionResults($request),
        ];
    }

    /**
     * @return \App\Api\Marketplace\Resources\MarketplaceResource|null
     */
    private function getMarketplace(): ?MarketplaceResource
    {
        /** @var \Domain\Marketplace\Marketplace $marketplace */
        $marketplace = $this->resource->marketplace()->first();

        if (!$marketplace) {
            return null;
        }

        return new MarketplaceResource($marketplace);
    }

    /**
     * @return \App\Api\Prediction\Resources\PredictionResource|null
     */
    public function getPrediction(): ?PredictionResource
    {
        /** @var \Domain\Prediction\Prediction $prediction */
        $prediction = $this->resource->prediction()->first();

        if (!$prediction) {
            return null;
        }

        return new PredictionResource($prediction);
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    private function getProducts(): AnonymousResourceCollection
    {
        return ProductResource::collection($this->resource->products()->get());
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    private function getSessionResults(Request $request): array
    {
        $sessionResults = $this->resource->sessionResults()->get();

        return $sessionResults->map(function (SessionResult $sessionResult) use ($request) {
            return (new SessionResultResource($sessionResult))->toArray($request);
        })->toArray();
    }
}
