<?php

declare(strict_types=1);

namespace App\Api\Session\Resources\SessionResult;

use Domain\Core\CurrencyTransformer;
use Illuminate\Http\Resources\Json\JsonResource;

class ShortSessionResultResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'                   => $this->resource->id,
            'initialInvestment'    => CurrencyTransformer::centsToDollars($this->resource->initial_investment),
            'investmentReturnIn'   => $this->resource->investment_return_in,
            'selfSustainabilityIn' => $this->resource->self_sustainability_in,
            'totalProfit'          => CurrencyTransformer::centsToDollars($this->resource->total_profit),
            'updated_at'           => $this->resource->updated_at
        ];
    }
}
