<?php

declare(strict_types=1);

namespace App\Api\Session\Resources\SessionResult;

use App\Api\Product\Resources\ProductResource;
use Domain\Core\CurrencyTransformer;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property \Domain\Session\Result\SessionResult $resource
 */
class SessionResultResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'                     => $this->resource->id,
            'product'                => $this->getProduct(),
            'cost'                   => CurrencyTransformer::centsToDollars($this->resource->cost),
            'minimumOrderQuantity'   => $this->resource->minimum_order_quantity,
            'breakEvenSalesPrice'    => CurrencyTransformer::centsToDollars($this->resource->break_even_sales_price),
            'breakEvenSalesQuantity' => $this->resource->break_even_sales_quantity,
            'initialInvestment'      => CurrencyTransformer::centsToDollars($this->resource->initial_investment),
            'investmentReturnIn'     => $this->resource->investment_return_in,
            'selfSustainabilityIn'   => $this->resource->self_sustainability_in,
            'totalProfit'            => CurrencyTransformer::centsToDollars($this->resource->total_profit),
            'cashConversionCycle'    => $this->resource->cash_conversion_cycle,
            'profitTimeline'         => $this->resource->profit_timeline,
        ];
    }

    /**
     * @return \App\Api\Product\Resources\ProductResource
     */
    private function getProduct(): ProductResource
    {
        /** @var \Domain\Product\Product $product */
        $product = $this->resource->product()->first();

        return new ProductResource($product);
    }
}

