<?php

declare(strict_types=1);

namespace App\Api\Session\Requests;

use Domain\Marketplace\Marketplace;
use Domain\Session\Model\UpdateSessionMarketplaceModel;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Exists;

class UpdateSessionMarketplaceRequest extends FormRequest implements UpdateSessionMarketplaceModel
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'marketplaceId' => ['required', 'integer', new Exists(Marketplace::class, 'id')],
        ];
    }

    /**
     * @inheritDoc
     */
    public function getMarketplaceId(): int
    {
        return (int)$this->input('marketplaceId');
    }
}
