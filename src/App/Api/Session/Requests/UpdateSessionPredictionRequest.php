<?php

declare(strict_types=1);

namespace App\Api\Session\Requests;

use Domain\Prediction\Prediction;
use Domain\Session\Model\UpdateSessionPredictionModel;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Exists;

class UpdateSessionPredictionRequest extends FormRequest implements UpdateSessionPredictionModel
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'predictionId' => ['required', 'integer', new Exists(Prediction::class, 'id')],
        ];
    }

    /**
     * @return int
     */
    public function getPredictionId(): int
    {
        return (int)$this->input('predictionId');
    }
}
