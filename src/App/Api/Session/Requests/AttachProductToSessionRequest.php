<?php

declare(strict_types=1);

namespace App\Api\Session\Requests;

use Domain\Product\Product;
use Domain\Session\Model\AttachroductToSessionModel;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Exists;

class AttachProductToSessionRequest extends FormRequest implements AttachroductToSessionModel
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'productId' => ['required', 'integer', new Exists(Product::class, 'id')],
        ];
    }

    /**
     * @inheritDoc
     */
    public function getProductId(): int
    {
        return (int)$this->input('productId');
    }
}
