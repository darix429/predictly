<?php

declare(strict_types=1);

namespace App\Api\Session\Controllers;

use App\Api\Session\Requests\AttachProductToSessionRequest;
use App\Api\Session\Requests\UpdateSessionMarketplaceRequest;
use App\Api\Session\Requests\UpdateSessionPredictionRequest;
use App\Api\Session\Resources\SessionResource;
use App\Core\Controllers\Controller;
use App\Core\Response;
use Domain\Marketplace\Repository\MarketplaceRepositoryInterface;
use Domain\Prediction\Repository\PredictionRepositoryInterface;
use Domain\Product\Domain\ProductDomain;
use Domain\Product\Repository\ProductRepositoryInterface;
use Domain\Session\Domain\SessionDomain;
use Domain\Session\Repository\SessionRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SessionController extends Controller
{
    /**
     * @var \Domain\Session\Domain\SessionDomain
     */
    private SessionDomain $sessionDomain;

    /**
     * @var \Domain\Session\Repository\SessionRepositoryInterface
     */
    private SessionRepositoryInterface $sessionRepository;

    /**
     * @var \Domain\Marketplace\Repository\MarketplaceRepositoryInterface
     */
    private MarketplaceRepositoryInterface $marketplaceRepository;

    /**
     * @var \Domain\Product\Repository\ProductRepositoryInterface
     */
    private ProductRepositoryInterface $productRepository;

    /**
     * @var \Domain\Prediction\Repository\PredictionRepositoryInterface
     */
    private PredictionRepositoryInterface $predictionRepository;

    /**
     * @var \Domain\Product\Domain\ProductDomain
     */
    private ProductDomain $productDomain;

    /**
     * SessionController constructor.
     *
     * @param \Domain\Session\Domain\SessionDomain                          $sessionDomain
     * @param \Domain\Session\Repository\SessionRepositoryInterface         $sessionRepository
     * @param \Domain\Marketplace\Repository\MarketplaceRepositoryInterface $marketplaceRepository
     * @param \Domain\Product\Repository\ProductRepositoryInterface         $productRepository
     * @param \Domain\Prediction\Repository\PredictionRepositoryInterface   $predictionRepository
     * @param \Domain\Product\Domain\ProductDomain                          $productDomain
     */
    public function __construct(
        SessionDomain $sessionDomain,
        SessionRepositoryInterface $sessionRepository,
        MarketplaceRepositoryInterface $marketplaceRepository,
        ProductRepositoryInterface $productRepository,
        PredictionRepositoryInterface $predictionRepository,
        ProductDomain $productDomain
    ) {
        $this->sessionDomain         = $sessionDomain;
        $this->sessionRepository     = $sessionRepository;
        $this->marketplaceRepository = $marketplaceRepository;
        $this->productRepository     = $productRepository;
        $this->predictionRepository  = $predictionRepository;
        $this->productDomain         = $productDomain;
    }

    /**
     * @return \App\Core\Response
     */
    public function active(): Response
    {
        /** @var \Domain\User\User $user */
        $user = Auth::user();

        $session = $this->sessionDomain->getActiveForUser($user);

        $resource = new SessionResource($session);

        return Response::make($resource);
    }

    /**
     * @param \App\Api\Session\Requests\UpdateSessionMarketplaceRequest $request
     * @param int                                                       $sessionId
     *
     * @return \App\Core\Response
     */
    public function updateMarketplace(UpdateSessionMarketplaceRequest $request, int $sessionId): Response
    {
        /** @var \Domain\User\User $user */
        $user = $request->user();

        $session     = $this->sessionRepository->findActiveByUserAndSessionId($user, $sessionId);
        $marketplace = $this->marketplaceRepository->findOrFail($request->getMarketplaceId());

        $this->sessionDomain->updateMarketplace($session, $marketplace);

        $resource = new SessionResource($session);

        return Response::make($resource);
    }

    /**
     * @param \App\Api\Session\Requests\UpdateSessionPredictionRequest $request
     * @param int                                                      $sessionId
     *
     * @return \App\Core\Response
     */
    public function updatePrediction(UpdateSessionPredictionRequest $request, int $sessionId): Response
    {
        /** @var \Domain\User\User $user */
        $user = $request->user();

        $session    = $this->sessionRepository->findActiveByUserAndSessionId($user, $sessionId);
        $prediction = $this->predictionRepository->findOrFail($request->getPredictionId());

        $this->sessionDomain->updatePrediction($session, $prediction);
        $this->productDomain->updateFactorsFromSession($session);

        $resource = new SessionResource($session);

        return Response::make($resource);
    }

    /**
     * @param \App\Api\Session\Requests\AttachProductToSessionRequest $request
     * @param int                                                     $sessionId
     *
     * @return \App\Core\Response
     */
    public function attachProduct(AttachProductToSessionRequest $request, int $sessionId): Response
    {
        /** @var \Domain\User\User $user */
        $user = $request->user();

        $session = $this->sessionRepository->findActiveByUserAndSessionId($user, $sessionId);
        $product = $this->productRepository->findByUserAndId($user, $request->getProductId());

        $this->sessionDomain->attachProduct($session, $product);

        $resource = new SessionResource($session);

        return Response::make($resource);
    }

    /**
     * @param \App\Api\Session\Requests\AttachProductToSessionRequest $request
     * @param int                                                     $sessionId
     *
     * @return \App\Core\Response
     */
    public function detachProduct(AttachProductToSessionRequest $request, int $sessionId): Response
    {
        /** @var \Domain\User\User $user */
        $user = $request->user();

        $session = $this->sessionRepository->findActiveByUserAndSessionId($user, $sessionId);
        $product = $this->productRepository->findByUserAndId($user, $request->getProductId());

        $this->sessionDomain->detachProduct($session, $product);

        $resource = new SessionResource($session);

        return Response::make($resource);
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Core\Response
     */
    public function last(Request $request): Response
    {
        /** @var \Domain\User\User $user */
        $user = $request->user();

        $session = $this->sessionRepository->findLastCalculatedForUser($user);

        if (!$session) {
            throw new ModelNotFoundException('Session not found');
        }

        $resource = new SessionResource($session);

        return Response::make($resource);
    }
}
