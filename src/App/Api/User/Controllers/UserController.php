<?php

declare(strict_types=1);

namespace App\Api\User\Controllers;

use App\Api\User\Resources\MeResource;
use App\Core\Controllers\Controller;
use App\Core\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Core\Response
     */
    public function me(Request $request): Response
    {
        /** @var \Domain\User\User $user */
        $user = $request->user();

        $resource = new MeResource($user);

        return Response::make($resource);
    }
}
