<?php

declare(strict_types=1);

namespace App\Api\User\Middleware;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class IsUserVerified
{
    /**
     * The URIs that should be reachable.
     *
     * @var array
     */
    protected array $except = [
        'api/v1/me',
        'api/v1/auth/logout'
    ];

    /**
     * @param          $request
     * @param \Closure $next
     *
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        /** @var \Illuminate\Http\Request $request */
        if ($request->user()->verified) {
            return $next($request);
        }

        if ($this->inExceptArray($request)) {
            return $next($request);
        }

        throw new HttpException(Response::HTTP_FORBIDDEN, 'Thanks for signing up to our private beta!');
    }

    /**
     * Determine if the request has a URI that should be accessible.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function inExceptArray($request)
    {
        foreach ($this->except as $except) {
            if ($except !== '/') {
                $except = trim($except, '/');
            }

            if ($request->fullUrlIs($except) || $request->is($except)) {
                return true;
            }
        }

        return false;
    }
}
