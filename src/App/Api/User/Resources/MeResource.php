<?php

declare(strict_types=1);

namespace App\Api\User\Resources;

use Domain\Provider\Provider;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property \Domain\User\User $resource
 */
class MeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'       => $this->resource->id,
            'name'     => $this->resource->name,
            'email'    => $this->resource->email,
            'verified' => $this->resource->verified,
            'free'     => $this->resource->free,
            'role'     => $this->getRoleName(),
            'subscription_count' => $this->resource->subscriptions()->where(["status"=>"ACTIVE"])->get()->count(),
            'subscription_end_count' => $this->resource->subscriptions()->whereIn("status",["ENDED","CANCELLED"])->get()->count(),
            'providers' => $this->resource->providers()->get()->map(static function (Provider $provider) {
                return $provider->name;
            }),
        ];
    }

    /**
     * @return string|null
     */
    private function getRoleName(): ?string
    {
        $role = $this->resource->roles()->first();

        if (!$role) {
            return null;
        }

        return $role->name;
    }
}
