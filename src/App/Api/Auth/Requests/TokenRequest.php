<?php

declare(strict_types=1);

namespace App\Api\Auth\Requests;

use Domain\OAuth2\Model\TokenModelInterface;
use Domain\Provider\Provider;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Exists;

/**
 * Class LoginRequest
 *
 * @package app\Http\Controllers\Requests\Auth
 */
class TokenRequest extends FormRequest implements TokenModelInterface
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'provider' => ['required', 'string', new Exists(Provider::class, 'name')],
            'code'     => ['required', 'string'],
        ];
    }

    /**
     * @inheritDoc
     */
    public function getCode(): string
    {
        return (string)$this->input('code');
    }

    /**
     * @inheritDoc
     */
    public function hasProvider(): bool
    {
        return $this->has('provider');
    }

    /**
     * @inheritDoc
     */
    public function getProvider(): ?string
    {
        if (!$this->hasProvider()) {
            return null;
        }

        return (string)$this->input('provider');
    }

    public function getGoogleUser() {
        return $this->input('googleUser');
    }
}
