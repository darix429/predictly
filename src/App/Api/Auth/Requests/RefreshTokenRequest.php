<?php

declare(strict_types=1);

namespace App\Api\Auth\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RefreshTokenRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'refreshToken' => ['required', 'string'],
        ];
    }

    /**
     * @return string
     */
    public function getRefreshToken(): string
    {
        return (string)$this->input('refreshToken');
    }
}
