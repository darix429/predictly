<?php

declare(strict_types=1);

namespace App\Api\Auth\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Unique;

class SignUpRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'email'    => ['required', 'string', 'email', new Unique('users', 'email')],
            'name'     => ['required', 'string'],
            'password' => ['required', 'string'],
        ];
    }

    /**
     * @return string
     */
    public function getFormEmail(): string
    {
        return $this->input('email');
    }


    /**
     * @return string
     */
    public function getFormName(): string
    {
        return $this->input('name');
    }


    /**
     * @return string
     */
    public function getFormNickname(): string
    {
        return $this->input('nickname');
    }

    /**
     * @return string
     */
    public function getFormPassword(): string
    {
        return $this->input('password');
    }
}
