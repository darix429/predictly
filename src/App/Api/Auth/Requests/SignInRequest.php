<?php

declare(strict_types=1);

namespace App\Api\Auth\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SignInRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            'email'    => ['required', 'string', 'email'],
            'password' => ['required', 'string'],
        ];
    }

    /**
     * @return string
     */
    public function getFormEmail(): string
    {
        return $this->input('email');
    }

    /**
     * @return string
     */
    public function getFormPassword(): string
    {
        return $this->input('password');
    }
}
