<?php

namespace App\Api\Auth\Mail;

use Domain\User\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserRegistred extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var \Domain\User\User
     */
    private User $user;

    /**
     * Create a new message instance.
     *
     * @param \Domain\User\User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('New User Registered')
            ->markdown('emails.user.registered')
            ->with([
                'name' => $this->user->name,
            ]);
    }
}
