<?php

declare(strict_types=1);

namespace App\Api\Auth\Controllers;

use App\Api\Auth\Requests\RefreshTokenRequest;
use App\Api\Auth\Requests\SignInRequest;
use App\Api\Auth\Requests\SignUpRequest;
use App\Core\KernelProxy;
use App\Core\Response;
use Domain\OAuth2\Bridge\RefreshTokenGrant;
use Domain\User\Domain\UserDomain;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Http\Request;
use Laravel\Passport\Token;

class AuthController extends BaseController
{
    /**
     * @var \Domain\User\Domain\UserDomain
     */
    private UserDomain $userDomain;

    /**
     * AuthController constructor.
     *
     * @param \App\Core\KernelProxy                   $proxy
     * @param \Illuminate\Contracts\Config\Repository $configRepository
     * @param \Domain\User\Domain\UserDomain          $userDomain
     */
    public function __construct(KernelProxy $proxy, Repository $configRepository, UserDomain $userDomain)
    {
        parent::__construct($proxy, $configRepository);
        $this->userDomain = $userDomain;
    }

    /**
     * @param \App\Api\Auth\Requests\SignInRequest $request
     *
     * @return \App\Core\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function signIn(SignInRequest $request): Response
    {
        $grantType   = 'password';
        $credentials = $this->getCredentials($grantType);

        $params = [
            'client_id'     => $credentials['client_id'],
            'client_secret' => $credentials['client_secret'],
            'username'      => $request->getFormEmail(),
            'password'      => $request->getFormPassword(),
        ];

        return $this->authenticate($request, $grantType, $params);
    }

    /**
     * @param \App\Api\Auth\Requests\SignUpRequest $request
     *
     * @return \App\Core\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function signUp(SignUpRequest $request): Response
    {

        $data = array(
            'email'    => $request->getFormEmail(),
            'name'     => $request->getFormName(),
            'password' => $request->getFormPassword(),
            'avatar'   => null,
        );
        $user = $this->userDomain->createFromArray($data);  
        $this->userDomain->verify($user);

        $grantType   = 'password';
        $credentials = $this->getCredentials($grantType);


        $params = [
            'client_id'     => $credentials['client_id'],
            'client_secret' => $credentials['client_secret'],
            'username'      => $request->getFormEmail(),
            'password'      => $request->getFormPassword(),
        ];

        return $this->authenticate($request, $grantType, $params);
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Core\Response
     */
    public function logout(Request $request): Response
    {
        /** @var \Domain\User\User $user */
        $user = $request->user();

        $user->tokens()->each(static function (Token $token) {
            $token->revoke();
        });

        return Response::make(true);
    }

    /**
     * @param \App\Api\Auth\Requests\RefreshTokenRequest $request
     *
     * @return \App\Core\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function refreshToken(RefreshTokenRequest $request): Response
    {
        $refreshToken = $request->getRefreshToken();

        $credentials = $this->getCredentials(RefreshTokenGrant::GRANT_TYPE);

        return $this->authenticate(
            $request,
            RefreshTokenGrant::GRANT_TYPE,
            array_merge($credentials, ['refresh_token' => $refreshToken])
        );
    }
}
