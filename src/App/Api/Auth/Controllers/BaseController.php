<?php

declare(strict_types=1);

namespace App\Api\Auth\Controllers;

use App\Core\Controllers\Controller;
use App\Core\KernelProxy;
use App\Core\Response;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response as LaravelResponse;
use Symfony\Component\HttpFoundation;

class BaseController extends Controller
{
    use AuthenticatesUsers;

    /**
     * @var \App\Core\KernelProxy
     */
    private KernelProxy $proxy;

    /**
     * @var array $credentials
     */
    private array $credentials;

    /**
     * @param \App\Core\KernelProxy                   $proxy
     * @param \Illuminate\Contracts\Config\Repository $configRepository
     */
    public function __construct(KernelProxy $proxy, Repository $configRepository)
    {
        $this->proxy       = $proxy;
        $this->credentials = $configRepository->get('auth.passport.grant');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param string                   $grantType
     * @param array                    $params
     *
     * @return \App\Core\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function authenticate(Request $request, string $grantType, array $params): Response
    {
        $oAuthTokenRoute = route('api.passport.token');

        $params = array_replace($params, [
            'grant_type' => $grantType,
            'scopes'     => '[*]',
        ]);

        $response = $this->proxy->post($oAuthTokenRoute, $params);

        if ($response->getStatusCode() === HttpFoundation\Response::HTTP_OK) {
            $this->clearLoginAttempts($request);
            $authorization = $response->getContent();
            $authorization = json_decode($authorization, true, 512, JSON_THROW_ON_ERROR);

            return Response::make($authorization);
        }

        $this->incrementLoginAttempts($request);

        // $content = json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);
        return Response::make()->setStatusCode($response->getStatusCode())->addErrorMessage($response->content());

        // return $this->makeAuthenticationResponse($response, $content);
    }

    /**
     * @param string $grant
     *
     * @return array
     */
    protected function getCredentials(string $grant): array
    {
        return [
            'client_id'     => $this->credentials[$grant]['client_id'],
            'client_secret' => $this->credentials[$grant]['client_secret']
        ];
    }

    /**
     * @param \Illuminate\Http\Response $response
     * @param array                     $content
     *
     * @return \App\Core\Response
     */
    protected function makeAuthenticationResponse(LaravelResponse $response, array $content): Response
    {
        return Response::make()
            ->setStatusCode($response->getStatusCode())
            ->addErrorMessage($content['message'], $response->getStatusCode());
    }
}
