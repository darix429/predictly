<?php

declare(strict_types=1);

namespace App\Api\Auth\Controllers\Social;

use App\Api\Auth\Controllers\BaseController;
use App\Api\Auth\Requests\TokenRequest;
use App\Core\KernelProxy;
use App\Core\Response;
use Domain\OAuth2\Bridge\SocialGrant;
use Domain\User\Domain\UserDomain;
use Domain\User\Repository\UserRepositoryInterface;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Laravel\Socialite\Contracts\Factory;
use Laravel\Socialite\SocialiteManager;
use Symfony\Component\HttpFoundation;
use Domain\User\User;
use Socialite;
use Google\Client as Google_Client;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class AuthController extends BaseController
{
    /**
     * @var \Laravel\Socialite\Contracts\Factory
     */
    private Factory $socialiteFactory;

    /**
     * @var \Laravel\Socialite\SocialiteManager
     */
    private SocialiteManager $socialiteManager;

    /**
     * @var \Domain\User\Repository\UserRepositoryInterface
     */
    private UserRepositoryInterface $userRepository;

    /**
     * @var \Domain\User\Domain\UserDomain
     */
    private UserDomain $userDomain;

    /**
     * AuthController constructor.
     *
     * @param \App\Core\KernelProxy                           $proxy
     * @param \Illuminate\Contracts\Config\Repository         $configRepository
     * @param \Laravel\Socialite\Contracts\Factory            $socialiteFactory
     * @param \Laravel\Socialite\SocialiteManager             $socialiteManager
     * @param \Domain\User\Repository\UserRepositoryInterface $userRepository
     * @param \Domain\User\Domain\UserDomain                  $userDomain
     */
    public function __construct(
        KernelProxy $proxy,
        Repository $configRepository,
        Factory $socialiteFactory,
        SocialiteManager $socialiteManager,
        UserRepositoryInterface $userRepository,
        UserDomain $userDomain
    ) {
        parent::__construct($proxy, $configRepository);

        $this->socialiteFactory = $socialiteFactory;
        $this->socialiteManager = $socialiteManager;
        $this->userRepository   = $userRepository;
        $this->userDomain       = $userDomain;
    }

    /**
     * @param \App\Api\Auth\Requests\TokenRequest $request
     *
     * @return \App\Core\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function socialSignIn(TokenRequest $request): Response
    {

        try {
            $provider = $request->getProvider();

            // GET TOKEN
            $driver = $this->socialiteFactory->driver($provider);
            $token = $driver->getAccessTokenResponse($request->getCode());

            // $client = new Google_Client();
            // $client->setClientId("12229886690-vt7g2mnf5md1p0ru48bu3f8sq8fmko72.apps.googleusercontent.com");
            // $client->setClientSecret("kwlLsPaUpMevElndHojzCiRB");
            // $client->setRedirectUri("https://localhost");
            // $client->setScopes("https://www.googleapis.com/oauth2/v4/token");
            // $token = $client->fetchAccessTokenWithAuthCode($request->getCode());

            // AUTHORIZE
            // $params = [
            //     'client_id'     => "12229886690-vt7g2mnf5md1p0ru48bu3f8sq8fmko72.apps.googleusercontent.com",
            //     'client_secret' => "kwlLsPaUpMevElndHojzCiRB",
            //     'redirect_uri'  => "https://app.predictly.io",
            //     'provider'      => "google",
            //     'code'          => $request->getCode()
            // ];
            // return Response::make($token);
            $credentials = $this->getCredentials(SocialGrant::SOCIAL_GRANT);
            $params = [
                'client_id'     => $credentials["client_id"],
                'client_secret' => $credentials["client_secret"],
                'provider'      => $provider,
                'access_token'  => $token['access_token'],
            ];

            $user = $this->socialiteManager->driver($provider)->userFromToken($token["access_token"]);
            if ($this->userRepository->countByEmail($user["email"]) === 0) {
                // register
                $createdUser = $this->userDomain->createFromSocialite($user, $provider);
                $this->userDomain->verify($createdUser);
            }

            $this->userRepository->findByProviderAndId($provider, $user->getId());
            // return $this->authenticate($request, 'authorization_code', $params);
            return $this->authenticate($request, SocialGrant::SOCIAL_GRANT, $params);



            
            // $provider = Socialite::driver("google");
            // $accessToken = $provider->getAccessTokenResponse($request->getCode());

            // $user = Socialite::driver('google')->userFromToken($token["access_token"]);
            // return Response::make($user);

            $socialUser = $this->socialiteManager->driver("google")->userFromToken($token['access_token']);
            
            // $this->userDomain->link($user, $socialiteUser, $provider);

            if ($this->userRepository->countByEmail($socialUser["email"]) === 0) {
                // register
                $this->userDomain->createFromSocialite($socialUser, $provider);
            }

            return $this->token($request, $provider, $token["access_token"]);

        } catch (Exception $e) {
            return Response::make($e->getMessage());
        }

    }

    /**
     * @param \App\Api\Auth\Requests\TokenRequest $request
     *
     * @return \App\Core\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    // public function signIn(TokenRequest $request): Response
    // {
    //     $provider = $request->getProvider();

    //     try {
    //         $accessToken = $this->getAccessToken($request);

    //         /** @var \Laravel\Socialite\Two\User $user */
    //         $user = $this->socialiteManager->driver($provider)->userFromToken($accessToken);

    //         $this->userRepository->findByProviderAndId($provider, $user->getId());

    //         return $this->token($request, $provider, $accessToken);
    //     } catch (ModelNotFoundException|RequestException $ex) {
    //         return Response::make()
    //             ->setCode(HttpFoundation\Response::HTTP_UNPROCESSABLE_ENTITY)
    //             ->setStatusCode(HttpFoundation\Response::HTTP_UNPROCESSABLE_ENTITY)
    //             ->addErrorMessage('Sorry, we didn’t recognize that account.');
    //     }
    // }

    /**
     * @param \App\Api\Auth\Requests\TokenRequest $request
     *
     * @return \App\Core\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function link(TokenRequest $request): Response
    {
        /** @var \Domain\User\User $user */
        $user = $request->user();

        $provider    = $request->getProvider();
        $accessToken = $this->getAccessToken($request);

        /** @var \Laravel\Socialite\Two\User $socialiteUser */
        $socialiteUser = $this->socialiteManager->driver($provider)->userFromToken($accessToken);

        $this->userDomain->link($user, $socialiteUser, $provider);

        return Response::make(1);
    }

    /**
     * @param \App\Api\Auth\Requests\TokenRequest $request
     *
     * @return string
     */
    protected function getAccessToken(TokenRequest $request): string
    {
        $code     = $request->getCode();
        $googleUser = $request->getGoogleUser();
        $provider = $request->getProvider();

        /** @var \Laravel\Socialite\Two\AbstractProvider $provider */
        $provider = $this->socialiteFactory->driver($provider);

        $accessToken = $provider->getAccessTokenResponse($code);

        return $accessToken['access_token'];
    }

    /**
     * @param \App\Api\Auth\Requests\TokenRequest $request
     * @param string                              $provider
     * @param string                              $accessToken
     *
     * @return \App\Core\Response
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    private function token(TokenRequest $request, string $provider, string $accessToken): Response
    {
        $credentials = $this->getCredentials(SocialGrant::SOCIAL_GRANT);


        $params = [
            'client_id'     => $credentials["client_id"],
            'client_secret' => $credentials["client_secret"],
            'provider'      => $provider,
            'access_token'  => $accessToken
        ];


        return $this->authenticate($request, SocialGrant::SOCIAL_GRANT, $params);
    }
}
