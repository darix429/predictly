<?php

declare(strict_types=1);

namespace App\Api\Core\Requests;

trait LimitedRequest
{
    /**
     * @return int
     */
    public function getLimit(): int
    {
        return (int)$this->input('limit', '10');
    }

    /**
     * @return array
     */
    public function getLimitRules(): array
    {
        return [
            'limit' => ['integer', 'min:10', 'max:100'],
        ];
    }
}
