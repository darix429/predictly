<?php

declare(strict_types=1);

namespace App\Api\Core\Requests;

trait OffsetRequest
{
    /**
     * @return int
     */
    public function getOffset(): int
    {
        return (int)$this->input('offset', '0');
    }

    /**
     * @return array
     */
    public function getOffsetRules(): array
    {
        return [
            'offset' => ['integer'],
        ];
    }
}
