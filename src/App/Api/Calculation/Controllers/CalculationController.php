<?php

declare(strict_types=1);

namespace App\Api\Calculation\Controllers;

use App\Api\Calculation\Requests\TuneRequest;
use App\Api\Session\Resources\SessionResource;
use App\Core\Controllers\Controller;
use App\Core\Response;
use Domain\Calculation\Domain\CalculationDomain;
use Domain\Factor\Domain\FactorDomain;
use Domain\Session\Domain\SessionDomain;
use Domain\Session\Repository\SessionRepositoryInterface;
use Illuminate\Http\Request;

class CalculationController extends Controller
{
    /**
     * @var \Domain\Calculation\Domain\CalculationDomain
     */
    private CalculationDomain $domain;

    /**
     * @var \Domain\Session\Repository\SessionRepositoryInterface
     */
    private SessionRepositoryInterface $sessionRepository;

    /**
     * @var \Domain\Factor\Domain\FactorDomain
     */
    private FactorDomain $factorDomain;

    /**
     * @var \Domain\Session\Domain\SessionDomain
     */
    private SessionDomain $sessionDomain;

    /**
     * CalculationController constructor.
     *
     * @param \Domain\Calculation\Domain\CalculationDomain          $domain
     * @param \Domain\Session\Repository\SessionRepositoryInterface $sessionRepository
     * @param \Domain\Factor\Domain\FactorDomain                    $factorDomain
     * @param \Domain\Session\Domain\SessionDomain                  $sessionDomain
     */
    public function __construct(
        CalculationDomain $domain,
        SessionRepositoryInterface $sessionRepository,
        FactorDomain $factorDomain,
        SessionDomain $sessionDomain
    ) {
        $this->domain            = $domain;
        $this->sessionRepository = $sessionRepository;
        $this->factorDomain      = $factorDomain;
        $this->sessionDomain     = $sessionDomain;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param int                      $sessionId
     *
     * @return \App\Core\Response
     * @throws \Exception
     */
    public function calculate(Request $request, int $sessionId): Response
    {
        // 1. Get auth user
        /** @var \Domain\User\User $user */
        $user = $request->user();

        // 2. Check if session exists
        $session = $this->sessionRepository->findActiveByUserAndSessionId($user, $sessionId);

        // 3. If session exist, begin calculation
        $this->domain->calculate($session);

        $resource = new SessionResource($session);

        return Response::make($resource);
    }

    /**
     * @param \App\Api\Calculation\Requests\TuneRequest $request
     *
     * @param int                                       $sessionId
     *
     * @return \App\Core\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function tune(TuneRequest $request, int $sessionId): Response
    {
        /** @var \Domain\User\User $user */
        $user    = $request->user();
        $factors = $request->getFactors();

        $calculatedSession = $this->sessionRepository->findOrFailCalculatedForUser($user, $sessionId);

        $session  = $this->sessionDomain->tune($user, $calculatedSession, $factors);
        $resource = new SessionResource($session);

        return Response::make($resource);
    }
}
