<?php

declare(strict_types=1);

namespace App\Api\Calculation\Requests;

use Domain\Factor\Model\FactorModel;
use Domain\Factor\Model\TuneModel;
use Domain\Product\Product;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Exists;

class TuneRequest extends FormRequest implements TuneModel
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'factors'                        => ['required', 'array'],
            'factors.*.productId'            => ['required', 'integer', new Exists(Product::class, 'id')],
            'factors.*.salesPriceAssumption' => ['required', 'integer', 'min:1', 'max:100'],
            'factors.*.salesPriceIncrease'   => ['required', 'integer', 'max:100'],
            'factors.*.monthlySales'         => ['required', 'integer', 'min:1', 'max:100'],
            'factors.*.salesGrowsRate'       => ['required', 'integer', 'max:100'],
            'factors.*.productCost'          => ['numeric', 'min:0.1', 'max:9999999'],
            'factors.*.minimumOrderQuantity' => ['integer', 'min:1', 'max:99999'],
        ];
    }

    /**
     * @return \Domain\Factor\Model\FactorModelInterface[]
     */
    public function getFactors(): array
    {
        $factors = [];

        foreach ($this->input('factors') as $factor) {
            $factors[] = FactorModel::makeForProductFromArray($factor);
        }

        return $factors;
    }
}
