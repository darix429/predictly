<?php

declare(strict_types=1);

namespace App\Api\Product\Resources;

use App\Api\Admin\Factor\Resources\FactorResource;
use App\Api\Prediction\Resources\PredictionResource;
use App\Api\Session\Resources\SessionResult\ShortSessionResultResource;
use Domain\Core\CurrencyTransformer;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property \Domain\Product\Product $resource
 */
class ProductResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'                => $this->resource->id,
            'name'              => $this->resource->name,
            'cost'              => CurrencyTransformer::centsToDollars($this->resource->cost),
            'monthlySales'      => $this->resource->monthly_sales,
            'salesRank'         => $this->resource->sales_rank,
            'reviews'           => $this->resource->reviews,
            'avatar'            => $this->resource->getFirstMediaUrl('avatars'),
            'competitors'       => $this->resource->competitors,
            'lastSessionResult' => $this->getShortResult(),
            'factor'            => $this->getFactor(), // TODO: remove this during refactoring
            'prediction'        => $this->getPrediction(),
            'updated_at'        => $this->resource->updated_at
        ];
    }

    /**
     * @return \App\Api\Session\Resources\SessionResult\ShortSessionResultResource|null
     */
    private function getShortResult(): ?ShortSessionResultResource
    {
        $sessionResult = $this->resource->sessionResults()->latest()->first();

        if (!$sessionResult) {
            return null;
        }

        return new ShortSessionResultResource($sessionResult);
    }

    /**
     * @return \App\Api\Prediction\Resources\PredictionResource|null
     */
    private function getPrediction(): ?PredictionResource
    {
        $session = $this->resource->sessions()->latest()->first();

        if (!$session) {
            return null;
        }

        $prediction = $session->prediction()->latest()->first();

        if (!$prediction) {
            return null;
        }

        return new PredictionResource($prediction);
    }

    /**
     * @return \App\Api\Admin\Factor\Resources\FactorResource|null
     */
    private function getFactor(): ?FactorResource
    {
        $factor = $this->resource->factor()->latest()->first();

        if (!$factor) {
            return null;
        }

        return new FactorResource($factor);
    }

}
