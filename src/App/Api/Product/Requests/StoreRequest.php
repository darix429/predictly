<?php

declare(strict_types=1);

namespace App\Api\Product\Requests;

use Domain\Core\CurrencyTransformer;
use Domain\Product\Model\StoreProductModel;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest implements StoreProductModel
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name'         => ['required', 'string', 'min:3', 'max:255'],
            'cost'         => ['required', 'numeric', 'min:1', 'max:9999999'],
            'monthlySales' => ['required', 'integer', 'min:1', 'max:99999'],
            'salesRank'    => ['nullable', 'integer', 'min:0', 'max:1000000'],
            'reviews'      => ['nullable', 'integer', 'min:0', 'max: 1000000'],
        ];
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return (string)$this->input('name');
    }

    /**
     * @inheritDoc
     */
    public function getCost(): int
    {
        return CurrencyTransformer::dollarsToCents((int)$this->input('cost'));
    }

    /**
     * @inheritDoc
     */
    public function getMonthlySales(): int
    {
        return (int)$this->input('monthlySales');
    }

    /**
     * @inheritDoc
     */
    public function hasSalesRank(): bool
    {
        return $this->has('salesRank');
    }

    /**
     * @inheritDoc
     */
    public function getSalesRank(): ?int
    {
        if (!$this->hasSalesRank()) {
            return null;
        }

        return (int)$this->input('salesRank');
    }

    /**
     * @inheritDoc
     */
    public function hasReviews(): bool
    {
        return $this->has('reviews');
    }

    /**
     * @inheritDoc
     */
    public function getReviews(): ?int
    {
        if (!$this->hasReviews()) {
            return null;
        }

        return (int)$this->input('reviews');
    }

    /**
     * @return bool
     */
    public function hasCompetitorsCount(): bool
    {
        return false;
    }

    /**
     * @return int|null
     */
    public function getCompetitorsCount(): ?int
    {
        return null;
    }
}
