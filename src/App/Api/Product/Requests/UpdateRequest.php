<?php

declare(strict_types=1);

namespace App\Api\Product\Requests;

use Domain\Product\Model\UpdateProductModel;

class UpdateRequest extends StoreRequest implements UpdateProductModel
{

}
