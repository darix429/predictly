<?php

declare(strict_types=1);

namespace App\Api\Product\Requests;

use Domain\Marketplace\Marketplace;
use Domain\Product\Model\UploadProductModel;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\UploadedFile;
use Illuminate\Validation\Rules\Exists;

class UploadRequest extends FormRequest implements UploadProductModel
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'marketplaceId' => ['required', 'integer', new Exists(Marketplace::class, 'id')],
            'file'          => ['required', 'mimetypes:csv/text,text/plain'],
        ];
    }

    /**
     * @inheritDoc
     */
    public function getMarketplaceId(): int
    {
        return (int)$this->input('marketplaceId');
    }

    /**
     * @inheritDoc
     */
    public function getFile(): UploadedFile
    {
        return $this->file('file');
    }
}
