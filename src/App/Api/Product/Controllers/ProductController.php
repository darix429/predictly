<?php

declare(strict_types=1);

namespace App\Api\Product\Controllers;

use App\Api\Product\Requests\StoreRequest;
use App\Api\Product\Requests\UpdateRequest;
use App\Api\Product\Requests\UploadRequest;
use App\Api\Product\Resources\ProductResource;
use App\Core\Controllers\Controller;
use App\Core\Response;
use Illuminate\Http\Request;
use Domain\Product\Domain\ProductDomain;
use Domain\Product\Repository\ProductRepositoryInterface;
use Domain\User\User;
use Domain\Product\Product;

use RuntimeException;
use League\Csv\Reader;
use League\Csv\Exception as LeagueCSVException;

use Intervention\Image\Image;
use Laravolt\Avatar\Facade as Avatar;

class ProductController extends Controller
{
    /**
     * @var \Domain\Product\Domain\ProductDomain
     */
    private ProductDomain $productDomain;

    /**
     * @var \Domain\Product\Repository\ProductRepositoryInterface
     */
    private ProductRepositoryInterface $productRepository;

    /**
     * ProductController constructor.
     *
     * @param \Domain\Product\Domain\ProductDomain                  $productDomain
     * @param \Domain\Product\Repository\ProductRepositoryInterface $productRepository
     */
    public function __construct(ProductDomain $productDomain, ProductRepositoryInterface $productRepository)
    {
        $this->productDomain     = $productDomain;
        $this->productRepository = $productRepository;
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \App\Core\Response
     */
    public function index(Request $request): Response
    {
        /** @var \Domain\User\User $user */
        $user = $request->user();

        $products = $this->productRepository->findAllByUser($user);

        $resource = ProductResource::collection($products);

        return Response::make($resource);
    }

    /**
     * @param \App\Api\Product\Requests\StoreRequest $request
     *
     * @return \App\Core\Response
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded
     */
    public function store(StoreRequest $request): Response
    {
        /** @var \Domain\User\User $user */
        $user = $request->user();

        $product = $this->productDomain->create($user, $request);

        $resource = new ProductResource($product);

        return Response::make($resource);
    }

    /**
     * @param \App\Api\Product\Requests\UploadRequest $request
     *
     * @return \App\Core\Response
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded
     * @throws \Exception
     */
    public function upload(UploadRequest $request): Response
    {
        $user = $request->user();
        // try {
        //     $product = $this->productDomain->parseCSV($user, $request);
        // } catch (LeagueCSVException $exception) {
        //     throw new RuntimeException('File can not be parsed!');
        // }

        // $resource = new ProductResource($product);
        $parse = $this->customParse($user, $request);
        if (!$parse["error"]) {
            return Response::make($parse);
        } else {
            return Response::make($parse);
        }

    }

    public function customParse(User $user, UploadRequest $request)
    {
        try {
            $csv = Reader::createFromPath($request->getFile()->getRealPath());
            $csv->setHeaderOffset(0);
            $headers = $csv->getHeader();
            $records = $csv->getRecords();
            
            $type = "generic";
            $result = [];
            
            if (strpos($headers[0], "Export date:") !== false) {
                $result = $this->parseJungleScoutCSV($user, $headers, $records);
            } else {
                $result = $this->parseGenericCSV($user, $records);
            }
            

            return array("error"=>false, "body"=>$result);
        } catch (League\Csv\SyntaxError $e) {
            return array("error"=>true, "body"=>"Spreadsheet file syntax error");
        } catch (LeagueCSVException $e) {
            return array("error"=>true, "body"=>"Spreadsheet file error");
        } catch (Exception $e) {
            return array("error"=>true, "body"=>$e->getMessage());
        }
    }

    public function parseGenericCSV($user, $records): array {
        $nameCols = ["Name", "Title", "Product Name"];
        $priceCols = ["Price", "Cost"];
        $monthlySalesCols = ["Mo. Sales", "Sales", "Est. Sales"];
        $salesRankCols = ["Rank", "Product Score"];
        $reviewCols = ["Reviews", "# of Reviews", "Review Count"];

        $result = [];
        
        foreach($records as $record){
            $data = array(
                "name"=>"",
                "cost"=>0,
                "monthly_sales"=>0,
                "sales_rank"=>0,
                "reviews"=>0
            );
            
            foreach($nameCols as $col) {
                if(!empty($record[$col])) {
                    $name = explode(",", $record[$col]);
                    $data["name"] = $name[0];
                }
            }
            foreach($priceCols as $col) {
                if(!empty($record[$col])) {
                    $data["cost"] = intval($record[$col]);
                }
            }
            foreach($monthlySalesCols as $col) {
                if(!empty($record[$col])) {
                    $data["monthly_sales"] = intval($record[$col]);
                }
            }
            foreach($salesRankCols as $col) {
                if(!empty($record[$col])) {
                    $data["sales_rank"] = intval($record[$col]);
                }
            }
            foreach($reviewCols as $col) {
                if(!empty($record[$col])) {
                    $data["reviews"] = intval($record[$col]);
                }
            }

            $product = new Product($data);
            $product->user()->associate($user);
            $product->save();

            // avatar
            // $filename = \uniqid(\str_replace(' ', '', $product->name), true);
            $filename = preg_replace('/[^A-Za-z0-9\-]/', '', $product->name);
            $avatar = Avatar::create($product->name)->save(\storage_path("app/public/{$filename}.png"));
            $product->addMedia($avatar->basePath())->toMediaCollection('avatars');

            array_push($result, $product);

        }

        return $result;
        
    }

    public function parseJungleScoutCSV($user, $headers, $records): array {

        $type = "junglescout";
        $recordsArray = [];
        foreach($records as $record) {
            if(!empty($record[$headers[0]]))
            array_push($recordsArray, $record[$headers[0]]);
        }


        $name = "search term: ";
        $cost = "average price: ";
        $monthly_sales = "average sales: ";
        $sales_rank = "average sales rank: ";
        $reviews = "average reviews: ";
        $data = array(
            "name"=>"",
            "cost"=>0,
            "monthly_sales"=>0,
            "sales_rank"=>0,
            "reviews"=>0
        );
        foreach($recordsArray as $i) {
            $i = strtolower($i);
            if (strpos($i, $name) !== false) {
                $data["name"] = str_replace($name,"",$i);
            }
            else if (strpos($i, $cost) !== false) {
                $data["cost"] = str_replace($cost,"",$i);
                $data["cost"] = preg_replace('/[^0-9.]/', '', $data["cost"]);
                $data["cost"] = intval($data["cost"]);
            }
            else if (strpos($i, $monthly_sales) !== false) {
                $data["monthly_sales"] = str_replace($monthly_sales,"",$i);
                $data["monthly_sales"] = preg_replace('/[^0-9.]/', '', $data["monthly_sales"]);
            }
            else if (strpos($i, $sales_rank) !== false) {
                $data["sales_rank"] = str_replace($sales_rank,"",$i);
                $data["sales_rank"] = preg_replace('/[^0-9.]/', '', $data["sales_rank"]);
            }
            else if (strpos($i, $reviews) !== false) {
                $data["reviews"] = str_replace($reviews,"",$i);
                $data["reviews"] = preg_replace('/[^0-9.]/', '', $data["reviews"]);
            }
        }


        $product = new Product($data);
        $product->user()->associate($user);
        $product->save();

        // avatar
        $filename = preg_replace('/[^A-Za-z0-9\-]/', '', $product->name);
        $avatar = Avatar::create($product->name)->save(\storage_path("app/public/{$filename}.png"));
        $product->addMedia($avatar->basePath())->toMediaCollection('avatars');

        return [$product];

    }

    /**
     * @param \App\Api\Product\Requests\UpdateRequest $request
     * @param int                                     $productId
     *
     * @return \App\Core\Response
     */
    public function update(UpdateRequest $request, int $productId): Response
    {
        /** @var \Domain\User\User $user */
        $user = $request->user();

        $product = $this->productRepository->findByUserAndId($user, $productId);

        $product = $this->productDomain->update($product, $request);

        $resource = new ProductResource($product);

        return Response::make($resource);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param int                      $productId
     *
     * @return \App\Core\Response
     * @throws \Exception
     */
    public function destroy(Request $request, int $productId): Response
    {
        /** @var \Domain\User\User $user */
        $user = $request->user();

        $product = $this->productRepository->findByUserAndId($user, $productId);

        $this->productDomain->delete($product);

        return Response::make(true);
    }
}
