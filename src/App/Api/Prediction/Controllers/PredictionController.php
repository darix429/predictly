<?php

declare(strict_types=1);

namespace App\Api\Prediction\Controllers;

use App\Api\Prediction\Requests\ListRequest;
use App\Api\Prediction\Resources\PredictionResource;
use App\Core\Controllers\Controller;
use App\Core\Response;
use Domain\Prediction\Repository\PredictionRepositoryInterface;

class PredictionController extends Controller
{
    /**
     * @var \Domain\Prediction\Repository\PredictionRepositoryInterface
     */
    private PredictionRepositoryInterface $predictionRepository;

    /***
     * PredictionController constructor.
     *
     * @param \Domain\Prediction\Repository\PredictionRepositoryInterface $predictionRepository
     */
    public function __construct(PredictionRepositoryInterface $predictionRepository)
    {
        $this->predictionRepository = $predictionRepository;
    }

    /**
     * @param \App\Api\Prediction\Requests\ListRequest $request
     *
     * @return \App\Core\Response
     */
    public function index(ListRequest $request): Response
    {
        $predictions = $this->predictionRepository->findAll($request);

        $resource = PredictionResource::collection($predictions);

        return Response::make($resource);
    }
}
