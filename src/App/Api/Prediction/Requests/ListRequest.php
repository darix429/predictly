<?php

declare(strict_types=1);

namespace App\Api\Prediction\Requests;

use App\Api\Core\Requests\LimitedRequest;
use App\Api\Core\Requests\OffsetRequest;
use Domain\Core\Model\ListModelInterface;
use Illuminate\Foundation\Http\FormRequest;

class ListRequest extends FormRequest implements ListModelInterface
{
    use OffsetRequest, LimitedRequest;

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return \array_merge($this->getLimitRules(), $this->getOffsetRules());
    }
}
