<?php

namespace App\Core\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use Laravel\Passport\Passport;
use Laravel\Passport\RouteRegistrar;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App';

    /**
     * The path to the "home" route for your application.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapStatelessRoutes();

        $this->mapWebRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->as('api.')
            ->middleware(['api'])
            ->namespace($this->namespace)
            ->group(function () {
                Passport::routes(function (RouteRegistrar $registrar) {
                    $registrar->forAccessTokens();
                });
            });

        Route::prefix('api/v1')
            ->middleware('api')
            ->namespace("{$this->namespace}\\Api")
            ->group(function () {
                Route::middleware(['auth:api', 'user.verified'])
                    ->group(\base_path('routes/api/v1/protected.php'));
            });

        Route::prefix('api/v1')
            ->middleware('api')
            ->namespace("{$this->namespace}\\Api")
            ->group(function () {
                Route::middleware(['api'])
                    ->group(\base_path('routes/api/v1/public.php'));
            });

        Route::prefix('api/v1/admin')
            ->as('admin.')
            ->middleware('api')
            ->namespace("{$this->namespace}\\Api\\Admin")
            ->group(function () {
                Route::middleware(['auth:api', 'user.verified', 'role:administrator'])
                    ->group(\base_path('routes/api/v1/admin.php'));
            });
    }

    /**
     * Define the "stateless" routes for the application.
     *
     * @return void
     */
    protected function mapStatelessRoutes()
    {
        Route::namespace($this->namespace)
            ->group(base_path('routes/stateless.php'));
    }
}
