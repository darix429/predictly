<?php

declare(strict_types=1);

namespace App\Core;

use Illuminate\Contracts\Support\Responsable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Symfony\Component\HttpFoundation;

class Response implements Responsable
{
    /**
     * @var int
     */
    private int $status;

    /**
     * @var array
     */
    private array $headers;

    /**
     * @var int
     */
    private int $options;

    /**
     * @var mixed
     */
    private $data;

    /**
     * @var int
     */
    private int $code;

    /**
     * @var array
     */
    private array $messages = [];

    /**
     * @var mixed
     */
    private $validation;

    /**
     * @var mixed
     */
    private $backtrace;

    /**
     * @var mixed
     */
    private $sentryEventId;

    /**
     * @var float|null
     */
    private ?float $duration;

    /**
     * @var float|null
     */
    private ?float $memory;

    /**
     * @var float|null
     */
    private ?float $memoryPeak;

    /**
     * @param mixed  $data
     * @param int    $status
     * @param array  $headers
     * @param int    $options
     */
    protected function __construct(
        $data = null,
        $status = 200,
        array $headers = [],
        $options = 0
    ) {
        $this->status  = $status;
        $this->headers = $headers;
        $this->options = $options;
        $this->data    = $data;
    }

    /**
     * @param mixed $data
     * @param int   $status
     * @param array $headers
     * @param int   $options
     *
     * @return \App\Core\Response
     */
    public static function make($data = null, $status = 200, array $headers = [], $options = 0): Response
    {
        return new static($data, $status, $headers, $options);
    }

    /**
     * @param int $code
     *
     * @return \App\Core\Response
     */
    public function setCode(int $code): Response
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @param string $text
     * @param mixed  $code
     *
     * @return \App\Core\Response
     */
    public function addErrorMessage(string $text, $code = 0): Response
    {
        return $this->addMessage('error', $text, $code);
    }

    /**
     * @param string $text
     *
     * @return bool
     */
    public function hasErrorMessage(string $text): bool
    {
        foreach ($this->messages as $message) {
            if ($message['severity'] === 'error' && $message['text'] === $text) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $severity
     * @param string $text
     * @param mixed  $code
     *
     * @return \App\Core\Response
     */
    public function addMessage(string $severity, string $text, $code = 0): Response
    {
        $message = [
            'severity' => $severity,
            'text'     => $text,
            'code'     => $code,
        ];

        $this->messages[] = $message;

        return $this;
    }

    /**
     * @param string $text
     * @param mixed  $code
     *
     * @return \App\Core\Response
     */
    public function addSuccessMessage(string $text, $code = 0): Response
    {
        return $this->addMessage('success', $text, $code);
    }

    /**
     * @param int $status
     *
     * @return \App\Core\Response
     */
    public function setStatusCode(int $status): Response
    {
        $this->status = $status;

        return $this;
    }


    /**
     * @param array $fields
     *
     * @return \App\Core\Response
     */
    public function setValidation(array $fields): Response
    {
        $this->validation = $fields;

        return $this;
    }

    /**
     * @param array $backtrace
     *
     * @return \App\Core\Response
     */
    public function setBacktrace(array $backtrace): Response
    {
        $this->backtrace = $backtrace;

        return $this;
    }

    /**
     * @param string $sentryEventId
     *
     * @return \App\Core\Response
     */
    public function setSentryEventId($sentryEventId): Response
    {
        $this->sentryEventId = $sentryEventId;

        return $this;
    }

    /**
     * @param float $duration
     *
     * @return \App\Core\Response
     */
    public function setDuration(float $duration): Response
    {
        $this->duration = round($duration, 4);

        return $this;
    }

    /**
     * @param float $memory
     *
     * @return \App\Core\Response
     */
    public function setMemory(float $memory): Response
    {
        $this->memory = round($memory, 4);

        return $this;
    }

    /**
     * @param float $memoryPeak
     *
     * @return \App\Core\Response
     */
    public function setMemoryPeak(float $memoryPeak): Response
    {
        $this->memoryPeak = round($memoryPeak, 4);

        return $this;
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function toResponse($request): JsonResponse
    {
        $this->normalizeData($request);

        if (defined('LARAVEL_START')) {
            $this->setDuration(microtime(true) - LARAVEL_START);
        }

        $this->setMemory(memory_get_usage() / 1024 / 1024);
        $this->setMemoryPeak(memory_get_peak_usage() / 1024 / 1024);

        $response = $this->prepareResponse();

        return new JsonResponse($response, $this->status, $this->headers, $this->options);
    }

    /**
     * @return array
     */
    protected function prepareResponse(): array
    {
        $response = [];

        if (in_array($this->status, [HttpFoundation\Response::HTTP_OK, HttpFoundation\Response::HTTP_CREATED])) {
            $response['result'] = $this->data;
        }

        if (!empty($this->code)) {
            $response['code'] = $this->code;
        }

        if (!empty($this->messages)) {
            $response['messages'] = $this->messages;
        }

        if (!empty($this->validation)) {
            $response['validation'] = $this->validation;
        }

        if (!empty($this->backtrace)) {
            $response['backtrace'] = $this->backtrace;
        }

        if (!empty($this->sentryEventId)) {
            $response['sentryEventId'] = $this->sentryEventId;
        }

        if ($this->duration !== null) {
            $response['duration'] = $this->duration;
        }

        if ($this->memory !== null) {
            $response['memory'] = $this->memory;
        }

        if ($this->memoryPeak !== null) {
            $response['memoryPeak'] = $this->memoryPeak;
        }

        return $response;
    }

    /**
     * @param \Illuminate\Http\Request $request
     */
    private function normalizeData(Request $request): void
    {
        if (is_bool($this->data)) {
            $this->data = intval($this->data);
        } elseif ($this->data instanceof JsonResource || $this->data instanceof ResourceCollection) {
            $this->data = $this->data->toArray($request);
        }
    }
}
