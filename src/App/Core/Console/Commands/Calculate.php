<?php

/** @noinspection PhpMissingFieldTypeInspection */

declare(strict_types=1);

namespace App\Core\Console\Commands;

use App\Api\Product\Requests\UploadRequest;
use Domain\Calculation\Domain\CalculationDomain;
use Domain\Marketplace\Marketplace;
use Domain\Prediction\Prediction;
use Domain\Product\Domain\ProductDomain;
use Domain\Product\Parser\AmazonParser;
use Domain\Session\Domain\SessionDomain;
use Domain\User\User;
use Illuminate\Console\Command;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\UploadedFile;
use RuntimeException;
use Symfony\Component\HttpFoundation\File\UploadedFile as SymfonyUploadedFile;

class Calculate extends Command
{
    /**
     * @var string
     */
    public $signature = 'calculate {path}';

    /**
     * @var string
     */
    public $name = 'Calculate and Output Result';

    /**
     * @param \Illuminate\Contracts\Foundation\Application $application
     * @param \Domain\Product\Parser\AmazonParser          $parser
     * @param \Domain\Session\Domain\SessionDomain         $sessionDomain
     * @param \Domain\Calculation\Domain\CalculationDomain $calculationDomain
     * @param \Domain\Product\Domain\ProductDomain         $productDomain
     *
     * @return void
     * @throws \Illuminate\Validation\ValidationException
     * @throws \League\Csv\Exception
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded
     */
    public function handle(
        Application $application,
        AmazonParser $parser,
        SessionDomain $sessionDomain,
        CalculationDomain $calculationDomain,
        ProductDomain $productDomain
    ): void {

        if (!$application->environment('local')) {
            throw new RuntimeException('This command is only for local usage!');
        }

        /** @var \Domain\User\User $user */
        $user = User::query()->firstOrFail();

        /** @var \Domain\Marketplace\Marketplace $marketplace */
        $marketplace = Marketplace::query()->firstOrFail();

        /** @var \Domain\Prediction\Prediction $prediction */
        $prediction = Prediction::query()->firstOrFail();

        $path         = $this->argument('path');
        $file         = new SymfonyUploadedFile($path, 'product.csv');
        $uploadedFile = UploadedFile::createFromBase($file);

        $request = UploadRequest::create('/', 'GET', [
            'marketplaceId' => $marketplace->id,
        ], [], [
            'file' => $uploadedFile,
        ]);

        $user->products()->delete();

        $product = $productDomain->parseCSV($user, $request);
        $session = $sessionDomain->getActiveForUser($user);

        $sessionDomain->attachProduct($session, $product);
        $sessionDomain->updateMarketplace($session, $marketplace);
        $sessionDomain->updatePrediction($session, $prediction);
        $productDomain->updateFactorsFromSession($session);

        $result = $calculationDomain->calculate($session);



        dump($result[0]->cash_conversion_cycle);
    }
}
