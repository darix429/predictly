<?php

namespace App\Core\Console\Commands;

use Domain\User\Repository\UserRepositoryInterface;
use Domain\User\User;
use Illuminate\Console\Command;
use Spatie\Permission\Exceptions\PermissionDoesNotExist;
use Spatie\Permission\Exceptions\RoleDoesNotExist;
use Spatie\Permission\Models\Role;

class AttachRoleToUser extends Command
{
    /**
     * @var \Domain\User\Repository\UserRepositoryInterface
     */
    private UserRepositoryInterface $userRepository;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'permission:attach
                            {user : The ID of the user}
                            {--role=administrator : The role name for the user}
                            {--permissions=* : List of permissions for the user}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Attach permissions to user';

    /**
     * AttachRoleToUser constructor.
     *
     * @param \Domain\User\Repository\UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        parent::__construct();

        $this->userRepository = $userRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = $this->getUser();

        $this->attachRoleToUser($user);
        $this->attachPermissionsToUser($user);
    }

    /**
     * @return \Domain\User\User
     */
    private function getUser(): User
    {
        $userId = $this->argument('user');

        return $this->userRepository->findOrFail($userId);
    }

    /**
     * @param \Domain\User\User $user
     */
    private function attachRoleToUser(User $user): void
    {
        $role = $this->option('role');

        if ($user->hasRole($role)) {
            return;
        }

        try {
            Role::findByName($role, null);
        } catch (RoleDoesNotExist $exception) {
            $this->info($exception->getMessage());
            exit();
        }

        $user->assignRole($role);
        $this->info("Role '{$role}' was attached to {$user->name} (email: {$user->email})");
    }

    /**
     * @param \Domain\User\User $user
     */
    private function attachPermissionsToUser(User $user): void
    {
        $permissions = $this->option('permissions');

        foreach ($permissions as $permission) {
            try {
                if ($user->hasPermissionTo($permission)) {
                    continue;
                }
            } catch (PermissionDoesNotExist $exception) {
                $this->info($exception->getMessage());
                continue;
            }

            $user->givePermissionTo($permission);
            $this->info("Permission '{$permission}' was attached to {$user->name} (email: {$user->email})");
        }
    }
}
