<?php

declare(strict_types=1);

namespace App\Core\Console\Commands;

use Domain\User\Domain\UserDomain;
use Illuminate\Console\Command;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create User';
    /**
     * @var \Domain\User\Domain\UserDomain
     */
    private UserDomain $userDomain;

    /**
     * @param \Domain\User\Domain\UserDomain $userDomain
     */
    public function __construct(UserDomain $userDomain)
    {
        $this->userDomain = $userDomain;

        parent::__construct();
    }

    /**
     * @return void
     */
    public function handle(): void
    {
        $email    = $this->ask('Please enter E-mail');
        $password = $this->ask('Please enter password');

        $name     = $this->ask('Please enter name');
        $nickname = $this->ask('Please enter nickname');

        $user = $this->userDomain->createFromArray([
            'email'    => $email,
            'password' => $password,
            'name'     => $name,
            'nickname' => $nickname,
            'avatar'   => null,
        ]);

        $this->userDomain->verify($user);

        $this->info("Successfully created user with email: {$email}");
    }
}
