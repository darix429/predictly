<?php

/** @noinspection PhpMissingFieldTypeInspection */

declare(strict_types=1);

namespace App\Core\Console\Commands;

use Domain\Product\Parser\AmazonParser;
use Illuminate\Console\Command;

class Parse extends Command
{
    /**
     * @var string
     */
    public $signature = 'parse {path}';

    /**
     * @var string
     */
    public $name = 'Parse and Output Result';

    /**
     * @param \Domain\Product\Parser\AmazonParser $parser
     *
     * @return void
     * @throws \League\Csv\Exception
     */
    public function handle(AmazonParser $parser): void
    {
        $path   = $this->argument('path');
        $result = $parser->parse($path);

        dump([
            'name'        => $result->getName(),
            'competitors' => $result->getCompetitorsCount(),
            'sales'       => $result->getAvgMonthlySales(),
            'reviews'     => $result->getAvgReviews(),
            'rank'        => $result->getAvgSalesRank(),
            'price'       => $result->getAvgPrice(),
        ]);
    }
}
