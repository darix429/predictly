<?php

declare(strict_types=1);

namespace Domain\OAuth2\Model;

interface TokenModelInterface
{
    /**
     * @return string
     */
    public function getCode(): string;

    /**
     * @return bool
     */
    public function hasProvider(): bool;

    /**
     * @return string
     */
    public function getProvider(): ?string;
}
