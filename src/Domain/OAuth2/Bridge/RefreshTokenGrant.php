<?php

declare(strict_types=1);

namespace Domain\OAuth2\Bridge;

use League\OAuth2\Server\Grant\RefreshTokenGrant as LaravelRefreshTokenGrant;

class RefreshTokenGrant extends LaravelRefreshTokenGrant
{
    public const GRANT_TYPE = 'refresh_token';

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return self::GRANT_TYPE;
    }
}
