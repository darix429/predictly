<?php

declare(strict_types=1);

namespace Domain\OAuth2\Bridge;

use DateInterval;
use Domain\OAuth2\Provider\SocialUserProvider;
use Domain\Socialite\Domain\SocialiteDomain;
use Illuminate\Contracts\Config\Repository;
use League\OAuth2\Server\Entities\UserEntityInterface;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\Grant\AbstractGrant;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use League\OAuth2\Server\RequestEvent;
use League\OAuth2\Server\ResponseTypes\ResponseTypeInterface;
use Psr\Http\Message\ServerRequestInterface;

class SocialGrant extends AbstractGrant
{
    public const SOCIAL_GRANT = 'social';

    /**
     * @var \Domain\Socialite\Domain\SocialiteDomain
     */
    private SocialiteDomain $socialUserDomain;

    /**
     * @var \Illuminate\Contracts\Config\Repository
     */
    private Repository $configRepository;

    /**
     * Create a Social Grant instance.
     *
     * @param \Domain\Socialite\Domain\SocialiteDomain $socialiteDomain
     * @param RefreshTokenRepositoryInterface          $refreshTokenRepository
     *
     * @param \Illuminate\Contracts\Config\Repository  $configRepository
     *
     * @throws \Exception
     */
    public function __construct(
        SocialiteDomain $socialiteDomain,
        RefreshTokenRepositoryInterface $refreshTokenRepository,
        Repository $configRepository
    ) {
        $this->socialUserDomain = $socialiteDomain;
        $this->setRefreshTokenRepository($refreshTokenRepository);
        $this->refreshTokenTTL  = new \DateInterval('P1M');
        $this->configRepository = $configRepository;
    }

    /**
     * @param \Psr\Http\Message\ServerRequestInterface                  $request
     * @param \League\OAuth2\Server\ResponseTypes\ResponseTypeInterface $responseType
     * @param \DateInterval                                             $accessTokenTTL
     *
     * @return \League\OAuth2\Server\ResponseTypes\ResponseTypeInterface
     * @throws \League\OAuth2\Server\Exception\OAuthServerException
     * @throws \League\OAuth2\Server\Exception\UniqueTokenIdentifierConstraintViolationException
     */
    public function respondToAccessTokenRequest(
        ServerRequestInterface $request,
        ResponseTypeInterface $responseType,
        DateInterval $accessTokenTTL
    ) {
        $client = $this->validateClient($request);

        $scopes = $this->validateScopes($this->getRequestParameter('scope', $request, $this->defaultScope));

        // Validates the user against social network provider
        $user = $this->validateUser($request);

        // Finalize the requested scopes
        $finalizedScopes = $this->scopeRepository->finalizeScopes($scopes, $this->getIdentifier(), $client,
            $user->getIdentifier());

        // Issue and persist new tokens
        $accessToken  = $this->issueAccessToken($accessTokenTTL, $client, $user->getIdentifier(), $finalizedScopes);
        $refreshToken = $this->issueRefreshToken($accessToken);

        // Send events to emitter
        $this->getEmitter()->emit(new RequestEvent(RequestEvent::ACCESS_TOKEN_ISSUED, $request));
        $this->getEmitter()->emit(new RequestEvent(RequestEvent::REFRESH_TOKEN_ISSUED, $request));

        // Inject tokens into response
        $responseType->setAccessToken($accessToken);
        $responseType->setRefreshToken($refreshToken);

        return $responseType;
    }

    /**
     * Validate the user.
     *
     * @param ServerRequestInterface $request
     *
     * @return UserEntityInterface
     * @throws OAuthServerException
     *
     */
    protected function validateUser(ServerRequestInterface $request): UserEntityInterface
    {
        $provider = $this->getRequestParameter('provider', $request);

        if (is_null($provider)) {
            throw OAuthServerException::invalidRequest('provider');
        }

        if (!$this->isProviderSupported($provider)) {
            throw OAuthServerException::invalidRequest('provider', 'Invalid provider');
        }

        $accessToken = $this->getRequestParameter('access_token', $request);

        if (is_null($accessToken)) {
            throw OAuthServerException::invalidRequest('access_token');
        }

        // Get user from social network provider
        $user = $this->socialUserDomain->getUserEntityByAccessToken($provider, $accessToken);

        if ($user instanceof UserEntityInterface === false) {
            $this->getEmitter()->emit(new RequestEvent(RequestEvent::USER_AUTHENTICATION_FAILED, $request));

            throw OAuthServerException::invalidCredentials();
        }

        return $user;
    }

    /**
     * Determine if the provider is supported.
     *
     * @param string $provider
     *
     * @return bool
     */
    protected function isProviderSupported($provider)
    {
        return in_array($provider, $this->configRepository->get('auth.social.providers'));
    }

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return self::SOCIAL_GRANT;
    }
}
