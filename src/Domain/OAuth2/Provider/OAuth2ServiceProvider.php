<?php

declare(strict_types=1);

namespace Domain\OAuth2\Provider;

use Carbon\Laravel\ServiceProvider;
use Domain\OAuth2\Bridge\RefreshTokenGrant;
use Domain\OAuth2\Bridge\SocialGrant;
use Domain\Socialite\Domain\SocialiteDomain;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Support\DeferrableProvider;
use Laravel\Passport\Bridge\RefreshTokenRepository;
use Laravel\Passport\Passport;
use League\OAuth2\Server\AuthorizationServer;
use League\OAuth2\Server\Grant\GrantTypeInterface;


class OAuth2ServiceProvider extends ServiceProvider implements DeferrableProvider
{
    public function provides()
    {
        return [
            AuthorizationServer::class,
        ];
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->extend(AuthorizationServer::class, function ($server, $app) {
            return tap($server, function (AuthorizationServer $server) {
                $server->enableGrantType(
                    $grantType = $this->makeSocialGrant(), Passport::tokensExpireIn()
                );

                $server->enableGrantType(
                    $this->makeRefreshTokenGrant(),
                    Passport::tokensExpireIn()
                );
            });
        });

        // Bind the SocialiteDomain to its implementation
        $this->app->singleton('social', SocialiteDomain::class);
    }

    /**
     * Create and configure an instance of Social Grant.
     *
     * @return \Domain\OAuth2\Bridge\SocialGrant
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     * @throws \Exception
     */
    protected function makeSocialGrant()
    {
        $grant = new SocialGrant(
            $this->app->make(SocialiteDomain::class),
            $this->app->make(RefreshTokenRepository::class),
            $this->app->make(Repository::class)
        );

        $grant->setRefreshTokenTTL(Passport::refreshTokensExpireIn());

        return $grant;
    }

    /**
     * @return mixed
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function makeRefreshTokenGrant()
    {
        $repository = $this->app->make(RefreshTokenRepository::class);

        return tap(new RefreshTokenGrant($repository), function (GrantTypeInterface $grant) {
            $grant->setRefreshTokenTTL(Passport::refreshTokensExpireIn());
        });
    }
}
