<?php

declare(strict_types=1);

namespace Domain\Product\Domain;

use Intervention\Image\Image;
use Laravolt\Avatar\Facade as Avatar;

class ProductAvatarCreator
{
    /**
     * @param string $name
     *
     * @return \Intervention\Image\Image
     */
    public function createFromName(string $name): Image
    {
        $filename = $this->generateFileName($name);

        return Avatar::create($name)->save(\storage_path("app/public/{$filename}.png"));
    }

    /**
     * @param string $prefix
     *
     * @return string
     */
    private function generateFileName(string $prefix): string
    {
        return \uniqid(\str_replace(' ', '_', $prefix), true);
    }
}
