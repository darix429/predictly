<?php

declare(strict_types=1);

namespace Domain\Product\Domain\Validator;

use Domain\Product\Repository\ProductRepositoryInterface;
use Domain\Session\Repository\SessionRepositoryInterface;
use Domain\User\User;
use Illuminate\Validation\ValidationException;

class CreateProductValidator
{
    /**
     * @var \Domain\Product\Repository\ProductRepositoryInterface
     */
    private ProductRepositoryInterface $productRepository;
    private SessionRepositoryInterface $sessionRepository;

    /**
     * ProductCreateValidator constructor.
     *
     * @param \Domain\Product\Repository\ProductRepositoryInterface $productRepository
     */
    public function __construct(ProductRepositoryInterface $productRepository, SessionRepositoryInterface $sessionRepository)
    {
        $this->productRepository = $productRepository;
        $this->sessionRepository = $sessionRepository;
    }

    /**
     * @param \Domain\User\User $user
     *
     * @return void
     * @throws \Illuminate\Validation\ValidationException
     */
    public function validate(User $user): void
    {
        $this->validateCount($user);
    }

    /**
     * @param \Domain\User\User $user
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    private function validateCount(User $user): void
    {
        // $count = $this->productRepository->countForUser($user);
        $session = $this->sessionRepository->findActiveForUser($user);
        $count = $this->productRepository->countForSession($session);

        // $freeProducts = $this->config['free']['sessions']['products']['count'];
        $freeProducts = 5;

        if ($count >= $freeProducts) {
            throw ValidationException::withMessages(["You can't add more than $freeProducts products to compare"]);
        }
    }
}
