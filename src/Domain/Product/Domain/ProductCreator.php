<?php

declare(strict_types=1);

namespace Domain\Product\Domain;

use Domain\Marketplace\Repository\MarketplaceRepository;
use Domain\Product\Domain\Validator\CreateProductValidator;
use Domain\Product\Model\StoreProductModel;
use Domain\Product\Product;
use Domain\User\User;

class ProductCreator
{
    /**
     * @var \Domain\Marketplace\Repository\MarketplaceRepository
     */
    private MarketplaceRepository $marketplaceRepository;

    /**
     * @var \Domain\Product\Domain\ProductAvatarCreator
     */
    private ProductAvatarCreator $avatarCreator;

    /**
     * @var \Domain\Product\Domain\Validator\CreateProductValidator
     */
    private CreateProductValidator $validator;

    /**
     * ProductCreator constructor.
     *
     * @param \Domain\Marketplace\Repository\MarketplaceRepository    $marketplaceRepository
     * @param \Domain\Product\Domain\ProductAvatarCreator             $avatarCreator
     * @param \Domain\Product\Domain\Validator\CreateProductValidator $validator
     */
    public function __construct(
        MarketplaceRepository $marketplaceRepository,
        ProductAvatarCreator $avatarCreator,
        CreateProductValidator $validator
    ) {
        $this->marketplaceRepository = $marketplaceRepository;
        $this->avatarCreator         = $avatarCreator;
        $this->validator             = $validator;
    }

    /**
     * @param \Domain\User\User                       $user
     * @param \Domain\Product\Model\StoreProductModel $model
     *
     * @return \Domain\Product\Product
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded
     * @throws \Illuminate\Validation\ValidationException
     * @noinspection DuplicatedCode
     */
    public function create(User $user, StoreProductModel $model): Product
    {
        $this->validator->validate($user);

        /** @var \Domain\Product\Product $product */
        $product = new Product();

        $product->name          = $model->getName();
        $product->cost          = $model->getCost();
        $product->monthly_sales = $model->getMonthlySales();

        if ($model->hasSalesRank()) {
            $product->sales_rank = $model->getSalesRank();
        }

        if ($model->hasReviews()) {
            $product->reviews = $model->getReviews();
        }

        if ($model->hasCompetitorsCount()) {
            $product->competitors = $model->getCompetitorsCount();
        }

        $product->user()->associate($user);

        $product->save();

        $this->createAvatar($product);

        return $product;
    }

    /**
     * @param \Domain\Product\Product $product
     *
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded;
     */
    private function createAvatar(Product $product): void
    {
        /** @var \Intervention\Image\Image $avatar */
        $avatar = $this->avatarCreator->createFromName($product->name);
        $product->addMedia($avatar->basePath())->toMediaCollection('avatars');
    }
}
