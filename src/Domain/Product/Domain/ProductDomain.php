<?php

declare(strict_types=1);

namespace Domain\Product\Domain;

use Domain\Marketplace\Repository\MarketplaceRepositoryInterface;
use Domain\Product\Model\ParsedProduct;
use Domain\Product\Model\StoreProductModel;
use Domain\Product\Model\UpdateProductModel;
use Domain\Product\Model\UploadProductModel;
use Domain\Product\Parser\ParserResolverInterface;
use Domain\Product\Product;
use Domain\Session\Session;
use Domain\User\User;

class ProductDomain
{
    /**
     * @var \Domain\Product\Domain\ProductCreator
     */
    private ProductCreator $creator;

    /**
     * @var \Domain\Product\Domain\ProductUpdater
     */
    private ProductUpdater $updater;

    /**
     * @var \Domain\Product\Domain\ProductDeleter
     */
    private ProductDeleter $deleter;

    /**
     * @var \Domain\Marketplace\Repository\MarketplaceRepositoryInterface
     */
    private MarketplaceRepositoryInterface $marketplaceRepository;

    /**
     * @var \Domain\Product\Parser\ParserResolverInterface
     */
    private ParserResolverInterface $parserResolver;

    /**
     * ProductDomain constructor.
     *
     * @param \Domain\Product\Domain\ProductCreator                         $creator
     * @param \Domain\Product\Domain\ProductUpdater                         $updater
     * @param \Domain\Product\Domain\ProductDeleter                         $deleter
     * @param \Domain\Marketplace\Repository\MarketplaceRepositoryInterface $marketplaceRepository
     * @param \Domain\Product\Parser\ParserResolverInterface                $parserResolver
     */
    public function __construct(
        ProductCreator $creator,
        ProductUpdater $updater,
        ProductDeleter $deleter,
        MarketplaceRepositoryInterface $marketplaceRepository,
        ParserResolverInterface $parserResolver
    ) {
        $this->creator               = $creator;
        $this->updater               = $updater;
        $this->deleter               = $deleter;
        $this->marketplaceRepository = $marketplaceRepository;
        $this->parserResolver        = $parserResolver;
    }

    /**
     * @param \Domain\User\User                       $user
     * @param \Domain\Product\Model\StoreProductModel $model
     *
     * @return \Domain\Product\Product
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded
     */
    public function create(User $user, StoreProductModel $model): Product
    {
        return $this->creator->create($user, $model);
    }

    /**
     * @param \Domain\User\User                        $user
     * @param \Domain\Product\Model\UploadProductModel $model
     *
     * @return \Domain\Product\Product
     * @throws \Illuminate\Validation\ValidationException
     * @throws \League\Csv\Exception
     * @throws \Spatie\MediaLibrary\Exceptions\FileCannotBeAdded
     */
    public function parseCSV(User $user, UploadProductModel $model): Product
    {
        $marketplace = $this->marketplaceRepository->findOrFail($model->getMarketplaceId());
        $filePath    = $model->getFile()->getRealPath();

        $parser = $this->parserResolver->getCsvParserForMarketPlace($marketplace);

        $parsedResult  = $parser->parse($filePath);
        $parsedProduct = ParsedProduct::makeFromParsedResult($parsedResult);

        return $this->create($user, $parsedProduct);
    }

    /**
     * @param \Domain\Product\Product                  $product
     * @param \Domain\Product\Model\UpdateProductModel $model
     *
     * @return \Domain\Product\Product
     */
    public function update(Product $product, UpdateProductModel $model): Product
    {
        return $this->updater->update($product, $model);
    }

    /**
     * @param \Domain\Product\Product $product
     *
     * @throws \Exception
     */
    public function delete(Product $product): void
    {
        $this->deleter->delete($product);
    }

    /**
     * @param \Domain\Session\Session $session
     */
    public function updateFactorsFromSession(Session $session): void
    {
        $this->updater->updateFactors($session);
    }
}
