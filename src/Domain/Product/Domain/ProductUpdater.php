<?php

declare(strict_types=1);

namespace Domain\Product\Domain;

use Domain\Factor\Domain\FactorCreator;
use Domain\Product\Model\UpdateProductModel;
use Domain\Product\Product;
use Domain\Session\Session;

class ProductUpdater
{
    /**
     * @var \Domain\Factor\Domain\FactorCreator
     */
    private FactorCreator $factorCreator;

    /**
     * ProductUpdater constructor.
     *
     * @param \Domain\Factor\Domain\FactorCreator $factorCreator
     */
    public function __construct(FactorCreator $factorCreator)
    {
        $this->factorCreator = $factorCreator;
    }

    /**
     * @param \Domain\Product\Product                  $product
     * @param \Domain\Product\Model\UpdateProductModel $model
     *
     * @return \Domain\Product\Product
     */
    public function update(Product $product, UpdateProductModel $model): Product
    {
        $product->name          = $model->getName();
        $product->cost          = $model->getCost();
        $product->monthly_sales = $model->getMonthlySales();

        if ($model->hasSalesRank()) {
            $product->sales_rank = $model->getSalesRank();
        }

        if ($model->hasReviews()) {
            $product->reviews = $model->getReviews();
        }

        if ($model->hasCompetitorsCount()) {
            $product->competitors = $model->getCompetitorsCount();
        }

        $product->save();

        return $product;
    }

    /**
     * @param \Domain\Session\Session $session
     */
    public function updateFactors(Session $session): void
    {
        /** @var \Domain\Prediction\Prediction $prediction */
        $prediction = $session->prediction()->firstOrFail();
        /** @var \Domain\Factor\Factor $factor */
        $factor = $prediction->factor()->firstOrFail();

        $products = $session->products()->get();

        foreach ($products as $product) {
            $this->factorCreator->replicate($factor, $product);
        }
    }
}
