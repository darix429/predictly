<?php

declare(strict_types=1);

namespace Domain\Product\Domain;

use Domain\Product\Product;

class ProductDeleter
{
    /**Ø
     * @param \Domain\Product\Product $product
     *
     * @return void
     * @throws \Exception
     */
    public function delete(Product $product): void
    {
        $product->delete();
    }
}
