<?php

declare(strict_types=1);

namespace Domain\Product\Parser;

use Domain\Product\Parser\Result\AmazonParsedResult;
use Domain\Product\Parser\Result\ParserResultInterface;
use League\Csv\Reader;
use League\Csv\Statement;

class AmazonParser implements ParserInterface
{
    private const HEADER_OFFSET = 0;
    private const ROW_OFFSET = 0;

    /**
     * @param string $path
     *
     * @return \Domain\Product\Parser\Result\ParserResultInterface
     * @throws \League\Csv\Exception
     */
    public function parse(string $path): ParserResultInterface
    {
        // try to remove incorrect delimiters
        $csv = file_get_contents($path);
        $csv = preg_replace('/(?!\"),(?!\")/', '', $csv);

        /** @var Reader $reader */
        $reader = Reader::createFromString($csv);
        $reader->setHeaderOffset(self::HEADER_OFFSET);
        $stmt = (new Statement())->offset(self::ROW_OFFSET);

        $records      = $stmt->process($reader);
        $parsedResult = new AmazonParsedResult();

        foreach ($records as $offset => $record) {
            $parsedResult->add($record);
        }

        $productName = $this->parseProductName($path);

        $parsedResult->setName($productName);

        return $parsedResult;
    }

    /**
     * @param string $path
     *
     * @return string
     * @throws \League\Csv\Exception
     */
    private function parseProductName(string $path): string
    {
        /** @var Reader $reader */
        $reader = Reader::createFromPath($path);

        $stmt = (new Statement())->limit(2);

        $data = $stmt->process($reader)->fetchOne(1);

        return \substr_replace($data[0], '', 0, \strlen('Search Term: '));
    }
}
