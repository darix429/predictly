<?php

declare(strict_types=1);

namespace Domain\Product\Parser\Result;

abstract class AbstractParsedResult implements ParserResultInterface
{
    /**
     * @var array
     */
    protected array $rows = [];

    /**
     * @var string
     */
    protected string $productName;

    /**
     * @param array $row
     */
    public function add(array $row): void
    {
        $this->rows[] = $this->formatRow($row);
    }

    /**
     * @return int
     */
    public function getAvgPrice(): int
    {
        $filtered = $this->filter('price');

        return (int)(array_sum($filtered) / count($filtered));
    }

    /**
     * @return int
     */
    public function getAvgMonthlySales(): int
    {
        $filtered = $this->filter('monthlySales');

        return (int)ceil((array_sum($filtered) / count($filtered)));
    }

    /**
     * @return int
     */
    public function getAvgSalesRank(): int
    {
        $filtered = $this->filter('salesRank');

        return (int)ceil((array_sum($filtered) / count($filtered)));
    }

    /**
     * @return int
     */
    public function getAvgReviews(): int
    {
        $filtered = array_column($this->rows, 'reviews');

        return (int)ceil((array_sum($filtered) / count($filtered)));
    }

    /**
     * @return int
     */
    public function getCompetitorsCount(): int
    {
        return count($this->rows);
    }

    /**
     * @param string $productName
     */
    public function setName(string $productName): void
    {
        $this->productName = $productName;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->productName;
    }

    /**
     * @param string $value
     *
     * @return mixed
     */
    abstract protected function preparePrice(string $value);

    /**
     * @param string $value
     *
     * @return mixed
     */
    abstract protected function prepareMonthlySales(string $value);

    /**
     * @param string $value
     *
     * @return mixed
     */
    abstract protected function prepareSalesRank(string $value);

    /**
     * @param string $value
     *
     * @return mixed
     */
    abstract protected function prepareReviews(string $value);

    /**
     * @param string $name
     *
     * @return array
     */
    private function filter(string $name): array
    {
        return array_filter(array_column($this->rows, $name), fn($value) => $value !== 0);
    }

    /**
     * @param array $row
     *
     * @return array
     */
    private function formatRow(array $row): array
    {
        $result = [];

        $result['price']        = $this->preparePrice($row['Price']);
        $result['monthlySales'] = $this->prepareMonthlySales($row['Mo. Sales']);
        $result['salesRank']    = $this->prepareSalesRank($row['Rank']);
        $result['reviews']      = $this->prepareReviews($row['Reviews']);

        return $result;
    }
}
