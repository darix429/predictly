<?php

declare(strict_types=1);

namespace Domain\Product\Parser\Result;

interface ParserResultInterface
{
    /**
     * @return int
     */
    public function getAvgPrice(): int;

    /**
     * @return int
     */
    public function getAvgMonthlySales(): int;

    /**
     * @return int
     */
    public function getAvgSalesRank(): int;

    /**
     * @return int
     */
    public function getAvgReviews(): int;

    /**
     * @return int
     */
    public function getCompetitorsCount(): int;

    /**
     * @param string $name
     */
    public function setName(string $name): void;

    /**
     * @return string
     */
    public function getName(): string;
}
