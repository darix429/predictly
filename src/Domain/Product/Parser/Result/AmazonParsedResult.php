<?php

declare(strict_types=1);

namespace Domain\Product\Parser\Result;

class AmazonParsedResult extends AbstractParsedResult
{
    /**
     * @param mixed $value
     *
     * @return int
     */
    protected function preparePrice(string $value): int
    {
        if (($position = strpos($value, '$')) === false) {
            return 0;
        }

        return (int)(substr($value, ++$position) * 100);
    }

    /**
     * @param string $monthlySales
     *
     * @return int
     */
    protected function prepareMonthlySales(string $monthlySales): int
    {
        return $this->prepareIntegerWithComma($monthlySales);
    }

    /**
     * @param string $salesRank
     *
     * @return int
     */
    protected function prepareSalesRank(string $salesRank): int
    {
        return $this->prepareIntegerWithComma($salesRank);
    }

    /**
     * @param string $reviews
     *
     * @return int
     */
    protected function prepareReviews(string $reviews): int
    {
        if ((int)$reviews === 0) {
            return 0;
        }

        return (int)$reviews;
    }

    /**
     * @param string $field
     *
     * @return int
     */
    private function prepareIntegerWithComma(string $field): int
    {
        $position = strpos($field, ',');

        if ($position === false && (int)$field === 0) {
            return 0;
        }

        if ($position === false) {
            return (int)$field;
        }

        return (int)substr_replace($field, '', $position, 1);
    }
}
