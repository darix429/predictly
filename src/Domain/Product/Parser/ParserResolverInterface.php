<?php

declare(strict_types=1);

namespace Domain\Product\Parser;

use Domain\Marketplace\Marketplace;

interface ParserResolverInterface
{
    /**
     * @param \Domain\Marketplace\Marketplace $marketplace
     *
     * @return \Domain\Product\Parser\ParserInterface
     */
    public function getCsvParserForMarketPlace(Marketplace $marketplace): ParserInterface;
}
