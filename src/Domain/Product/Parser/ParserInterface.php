<?php

declare(strict_types=1);

namespace Domain\Product\Parser;

use Domain\Product\Parser\Result\ParserResultInterface;

interface ParserInterface
{
    /**
     * @param string $path
     *
     * @return \Domain\Product\Parser\Result\ParserResultInterface
     * @throws \League\Csv\Exception
     */
    public function parse(string $path): ParserResultInterface;
}
