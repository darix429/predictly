<?php

declare(strict_types=1);

namespace Domain\Product\Parser;

use Domain\Marketplace\Marketplace;
use DomainException;
use Illuminate\Contracts\Container\Container;

class ParserResolver implements ParserResolverInterface
{
    /**
     * @var array
     */
    private array $config;

    /**
     * @var \Illuminate\Contracts\Container\Container
     */
    private Container $container;

    /**
     * @param array                                     $config
     * @param \Illuminate\Contracts\Container\Container $container
     */
    public function __construct(array $config, Container $container)
    {
        $this->config    = $config;
        $this->container = $container;
    }

    /**
     * @param \Domain\Marketplace\Marketplace $marketplace
     *
     * @return \Domain\Product\Parser\ParserInterface
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function getCsvParserForMarketPlace(Marketplace $marketplace): ParserInterface
    {
        $name = $marketplace->name;

        if (!isset($this->config[$name])) {
            throw new DomainException("Parser for marketplace [{$name}] not found!");
        }

        return $this->container->make($this->config[$name]['csv']['class']);
    }
}
