<?php

declare(strict_types=1);

namespace Domain\Product;

use Domain\Factor\Factor;
use Domain\Factor\Model\FactorableInterface;
use Domain\Session\Result\SessionResult;
use Domain\Session\Session;
use Domain\User\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

/**
 * @property int      $id
 * @property int      $user_id
 * @property string   $name
 * @property int      $cost
 * @property int      $monthly_sales
 * @property int      $sales_rank
 * @property int      $reviews
 * @property int|null $competitors
 * @property string   $updated_at
 */
class Product extends Model implements HasMedia, FactorableInterface
{
    use SoftDeletes, HasMediaTrait;

    public const RELATION_USER    = 'user';
    public const RELATION_SESSION = 'session';

    /** @var string */
    protected $table = 'products';

    /** @var array */
    protected $fillable = [
        'name',
        'cost',
        'monthly_sales',
        'sales_rank',
        'reviews',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id', self::RELATION_USER);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function sessions(): BelongsToMany
    {
        return $this->belongsToMany(
            Session::class,
            'product_session',
            'product_id',
            'session_id',
            'id',
            'id',
            self::RELATION_SESSION);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sessionResults(): HasMany
    {
        return $this->hasMany(SessionResult::class, 'product_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function factor(): MorphOne
    {
        return $this->morphOne(Factor::class, 'factorable');
    }
}
