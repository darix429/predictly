<?php

declare(strict_types=1);

namespace Domain\Product\Provider;

use Domain\Product\Parser\ParserResolver;
use Domain\Product\Parser\ParserResolverInterface;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class ParserResolverProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * @return array
     */
    public function provides(): array
    {
        return [
            ParserResolverInterface::class,
        ];
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ParserResolverInterface::class, function (Application $application) {
            $configRepository = $application->make(Repository::class);

            $config = $configRepository->get('product.parser.marketplaces');

            return new ParserResolver($config, $application);
        });
    }
}
