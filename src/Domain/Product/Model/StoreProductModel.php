<?php

declare(strict_types=1);

namespace Domain\Product\Model;

interface StoreProductModel
{
    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return int
     */
    public function getCost(): int;

    /**
     * @return int
     */
    public function getMonthlySales(): int;

    /**
     * @return bool
     */
    public function hasSalesRank(): bool;

    /**
     * @return int|null
     */
    public function getSalesRank(): ?int;

    /**
     * @return bool
     */
    public function hasReviews(): bool;

    /**
     * @return int|null
     */
    public function getReviews(): ?int;

    /**
     * @return bool
     */
    public function hasCompetitorsCount(): bool;

    /**
     * @return int|null
     */
    public function getCompetitorsCount(): ?int;
}
