<?php

declare(strict_types=1);

namespace Domain\Product\Model;

use Domain\Product\Parser\Result\ParserResultInterface;

class ParsedProduct implements StoreProductModel
{
    /**
     * @var string
     */
    private string $name;

    /**
     * @var int
     */
    private int $cost;

    /**
     * @var int
     */
    private int $monthlySales;

    /**
     * @var int
     */
    private int $salesRank;

    /**
     * @var int
     */
    private int $reviews;

    /**
     * @var int
     */
    private int $competitorsCount;

    /**
     * ParsedProduct constructor.
     *
     * @param string $name
     * @param int    $cost
     * @param int    $monthlySales
     * @param int    $salesRank
     * @param int    $reviews
     * @param int    $competitorsCount
     */
    private function __construct(
        string $name,
        int $cost,
        int $monthlySales,
        int $salesRank,
        int $reviews,
        int $competitorsCount
    ) {
        $this->name             = $name;
        $this->cost             = $cost;
        $this->monthlySales     = $monthlySales;
        $this->salesRank        = $salesRank;
        $this->reviews          = $reviews;
        $this->competitorsCount = $competitorsCount;
    }

    /**
     * @param \Domain\Product\Parser\Result\ParserResultInterface $parsedResult
     *
     * @return \Domain\Product\Model\ParsedProduct
     */
    public static function makeFromParsedResult(ParserResultInterface $parsedResult): ParsedProduct
    {
        return new ParsedProduct(
            $parsedResult->getName(),
            $parsedResult->getAvgPrice(),
            $parsedResult->getAvgMonthlySales(),
            $parsedResult->getAvgSalesRank(),
            $parsedResult->getAvgReviews(),
            $parsedResult->getCompetitorsCount()
        );
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @inheritDoc
     */
    public function getCost(): int
    {
        return $this->cost;
    }

    /**
     * @inheritDoc
     */
    public function getMonthlySales(): int
    {
        return $this->monthlySales;
    }

    /**
     * @inheritDoc
     */
    public function hasSalesRank(): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function getSalesRank(): int
    {
        return $this->salesRank;
    }

    /**
     * @inheritDoc
     */
    public function hasReviews(): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function getReviews(): int
    {
        return $this->reviews;
    }

    /**
     * @return bool
     */
    public function hasCompetitorsCount(): bool
    {
        return true;
    }

    /**
     * @return int
     */
    public function getCompetitorsCount(): int
    {
        return $this->competitorsCount;
    }
}
