<?php

declare(strict_types=1);

namespace Domain\Product\Model;

use Illuminate\Http\UploadedFile;

interface UploadProductModel
{
    /**
     * @return int
     */
    public function getMarketplaceId(): int;

    /**
     * @return \Illuminate\Http\UploadedFile
     */
    public function getFile(): UploadedFile;
}
