<?php

declare(strict_types=1);

namespace Domain\Product\Repository;

use Domain\Product\Product;
use Domain\Session\Session;
use Domain\User\User;
use Illuminate\Database\Eloquent\Collection;

interface ProductRepositoryInterface
{
    /**
     * @param int $id
     *
     * @return \Domain\Product\Product
     */
    public function findOrFail(int $id): Product;

    /**
     * @param \Domain\User\User $user
     * @param int               $productId
     *
     * @return \Domain\Product\Product
     */
    public function findByUserAndId(User $user, int $productId): Product;

    /**
     * @param \Domain\User\User $user
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findAllByUser(User $user): Collection;

    /**
     * @param \Domain\User\User $user
     *
     * @return int
     */
    public function countForUser(User $user): int;

    /**
     * @param \Domain\Product\Product $product
     * @param \Domain\Session\Session $session
     *
     * @return int
     */
    public function countForSessionByProductAndSession(Product $product, Session $session): int;

    /**
     * @param \Domain\Session\Session $session
     *
     * @return int
     */
    public function countForSession(Session $session): int;
}
