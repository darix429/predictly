<?php

declare(strict_types=1);

namespace Domain\Product\Repository;

use Domain\Product\Product;
use Domain\Session\Session;
use Domain\User\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Query\JoinClause;

class ProductRepository implements ProductRepositoryInterface
{
    /**
     * @param int $id
     *
     * @return \Domain\Product\Product
     */
    public function findOrFail(int $id): Product
    {
        /** @var \Domain\Product\Product $product */
        $product = Product::query()->findOrFail($id);

        return $product;
    }

    /**
     * @param \Domain\User\User $user
     * @param int               $productId
     *
     * @return \Domain\Product\Product
     */
    public function findByUserAndId(User $user, int $productId): Product
    {
        /** @var \Domain\Product\Product $product */
        $product = Product::query()
            ->where('user_id', $user->id)
            ->where('id', $productId)
            ->whereNull('deleted_at')
            ->first();

        if (!$product) {
            throw new ModelNotFoundException('Product not found');
        }

        return $product;
    }

    /**
     * @param \Domain\User\User $user
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findAllByUser(User $user): Collection
    {
        return Product::query()
            ->where('user_id', $user->id)
            ->whereNull('deleted_at')
            ->get();
    }

    /**
     * @param \Domain\User\User $user
     *
     * @return int
     */
    public function countForUser(User $user): int
    {
        return Product::query()->where('user_id', $user->id)->count();
    }

    /**
     * @param \Domain\Product\Product $product
     * @param \Domain\Session\Session $session
     *
     * @return int
     */
    public function countForSessionByProductAndSession(Product $product, Session $session): int
    {
        return Product::query()
            ->join('product_session', function (JoinClause $join) {
                $join->on('products.id', '=', 'product_session.product_id');
            })
            ->where('product_session.session_id', '=', $session->id)
            ->where('product_session.product_id', '=', $product->id)
            ->count();
    }

    /**
     * @param \Domain\Session\Session $session
     *
     * @return int
     */
    public function countForSession(Session $session): int
    {
        return Product::query()
            ->join('product_session', function (JoinClause $join) {
                $join->on('products.id', '=', 'product_session.product_id');
            })
            ->where('product_session.session_id', '=', $session->id)
            ->count();
    }
}
