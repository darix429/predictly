<?php

declare(strict_types=1);

namespace Domain\Prediction\Domain;

use Domain\Prediction\Model\StorePredictionModel;
use Domain\Prediction\Model\UpdatePredictionModel;
use Domain\Prediction\Prediction;

class PredictionDomain
{
    /**
     * @var \Domain\Prediction\Domain\PredictionCreator
     */
    private PredictionCreator $creator;

    /**
     * @var \Domain\Prediction\Domain\PredictionUpdater
     */
    private PredictionUpdater $updater;

    /**
     * @var \Domain\Prediction\Domain\PredictionDeleter
     */
    private PredictionDeleter $deleter;

    /**
     * PredictionDomain constructor.
     *
     * @param \Domain\Prediction\Domain\PredictionCreator $creator
     * @param \Domain\Prediction\Domain\PredictionUpdater $updater
     * @param \Domain\Prediction\Domain\PredictionDeleter $deleter
     */
    public function __construct(PredictionCreator $creator, PredictionUpdater $updater, PredictionDeleter $deleter)
    {
        $this->creator = $creator;
        $this->updater = $updater;
        $this->deleter = $deleter;
    }

    /**
     * @param \Domain\Prediction\Model\StorePredictionModel $model
     *
     * @return \Domain\Prediction\Prediction
     */
    public function create(StorePredictionModel $model): Prediction
    {
        return $this->creator->create($model);
    }

    /**
     * @param \Domain\Prediction\Prediction                  $prediction
     * @param \Domain\Prediction\Model\UpdatePredictionModel $model
     *
     * @return bool
     */
    public function update(Prediction $prediction, UpdatePredictionModel $model): bool
    {
        return $this->updater->update($prediction, $model);
    }

    /**
     * @param \Domain\Prediction\Prediction $prediction
     *
     * @throws \Exception
     */
    public function delete(Prediction $prediction): void
    {
        $this->deleter->delete($prediction);
    }
}
