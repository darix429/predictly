<?php

declare(strict_types=1);

namespace Domain\Prediction\Domain;

use Domain\Factor\Domain\FactorDomain;
use Domain\Prediction\Model\UpdatePredictionModel;
use Domain\Prediction\Prediction;

class PredictionUpdater
{
    /**
     * @var \Domain\Factor\Domain\FactorDomain
     */
    private FactorDomain $factorDomain;

    /**
     * PredictionUpdater constructor.
     *
     * @param \Domain\Factor\Domain\FactorDomain $factorDomain
     */
    public function __construct(FactorDomain $factorDomain)
    {
        $this->factorDomain = $factorDomain;
    }

    /**
     * @param \Domain\Prediction\Prediction                  $prediction
     * @param \Domain\Prediction\Model\UpdatePredictionModel $model
     *
     * @return bool
     */
    public function update(Prediction $prediction, UpdatePredictionModel $model): bool
    {
        /** @var \Domain\Factor\Factor $factor */
        $factor = $prediction->factor()->first();

        $this->factorDomain->update($factor, $model);

        $prediction->name        = $model->getName();
        $prediction->description = $model->getDescription();
        $prediction->category    = $model->getCategory();

        return $prediction->save();
    }
}
