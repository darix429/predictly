<?php

declare(strict_types=1);

namespace Domain\Prediction\Domain;

use Domain\Prediction\Prediction;

class PredictionDeleter
{
    /**
     * @param \Domain\Prediction\Prediction $prediction
     *
     * @throws \Exception
     */
    public function delete(Prediction $prediction): void
    {
        $prediction->factor()->delete();
        $prediction->delete();
    }
}
