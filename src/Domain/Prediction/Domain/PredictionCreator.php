<?php

declare(strict_types=1);

namespace Domain\Prediction\Domain;

use Domain\Factor\Domain\FactorDomain;
use Domain\Prediction\Model\StorePredictionModel;
use Domain\Prediction\Prediction;

class PredictionCreator
{
    /**
     * @var \Domain\Factor\Domain\FactorDomain
     */
    private FactorDomain $factorDomain;

    /**
     * PredictionCreator constructor.
     *
     * @param \Domain\Factor\Domain\FactorDomain $factorDomain
     */
    public function __construct(FactorDomain $factorDomain)
    {
        $this->factorDomain = $factorDomain;
    }

    /**
     * @param \Domain\Prediction\Model\StorePredictionModel $model
     *
     * @return \Domain\Prediction\Prediction
     */
    public function create(StorePredictionModel $model): Prediction
    {
        $prediction = new Prediction();

        $prediction->name        = $model->getName();
        $prediction->description = $model->getDescription();
        $prediction->category    = $model->getCategory();

        $prediction->save();

        $factor = $this->factorDomain->create($model, $prediction);
        $factor->factorable()->associate($prediction);

        $factor->save();

        return $prediction;
    }
}
