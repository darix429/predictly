<?php

declare(strict_types=1);

namespace Domain\Prediction\Provider;

use Domain\Prediction\Repository\PredictionRepository;
use Domain\Prediction\Repository\PredictionRepositoryInterface;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class PredictionRepositoryProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            PredictionRepositoryInterface::class,
        ];
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PredictionRepositoryInterface::class, PredictionRepository::class);
    }
}
