<?php

declare(strict_types=1);

namespace Domain\Prediction\Model;

use Domain\Factor\Model\StoreFactorModel;

interface StorePredictionModel extends StoreFactorModel
{
    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return string
     */
    public function getDescription(): string;

    /**
     * @return string
     */
    public function getCategory(): string;
}
