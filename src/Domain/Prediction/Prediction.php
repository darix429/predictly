<?php

declare(strict_types=1);

namespace Domain\Prediction;

use Domain\Factor\Factor;
use Domain\Factor\Model\FactorableInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphOne;

/**
 * @property-read int $id
 * @property string   $name
 * @property string   $description
 * @property string   $category
 */
class Prediction extends Model implements FactorableInterface
{
    public const PRIMARY_CATEGORY = 'primary';
    public const TESTING_CATEGORY = 'testing';
    public const DRAFTS_CATEGORY = 'drafts';

    public const CATEGORIES = [
        self::PRIMARY_CATEGORY,
        self::TESTING_CATEGORY,
        self::DRAFTS_CATEGORY,
    ];

    /**
     * @var string
     */
    protected $table = 'predictions';

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'category',
        'description',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function factor(): MorphOne
    {
        return $this->morphOne(Factor::class, 'factorable');
    }
}
