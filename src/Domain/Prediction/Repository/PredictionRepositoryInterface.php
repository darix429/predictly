<?php

declare(strict_types=1);

namespace Domain\Prediction\Repository;

use Domain\Core\Model\ListModelInterface;
use Domain\Prediction\Prediction;
use Illuminate\Database\Eloquent\Collection;

interface PredictionRepositoryInterface
{
    /**
     * @param \Domain\Core\Model\ListModelInterface $model
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findAll(ListModelInterface $model): Collection;

    /**
     * @param int $id
     *
     * @return \Domain\Prediction\Prediction
     */
    public function findOrFail(int $id): Prediction;

    /**
     * @param \Domain\Core\Model\ListModelInterface $model
     * @param string                                $category
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findByCategory(ListModelInterface $model, string $category): Collection;
}
