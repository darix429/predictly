<?php

declare(strict_types=1);

namespace Domain\Prediction\Repository;

use Domain\Core\Model\ListModelInterface;
use Domain\Prediction\Prediction;
use Illuminate\Database\Eloquent\Collection;

class PredictionRepository implements PredictionRepositoryInterface
{
    /**
     * @param \Domain\Core\Model\ListModelInterface $model
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findAll(ListModelInterface $model): Collection
    {
        return Prediction::query()->offset($model->getOffset())->limit($model->getLimit())->get();
    }

    /**
     * @param int $id
     *
     * @return \Domain\Prediction\Prediction
     */
    public function findOrFail(int $id): Prediction
    {
        /** @var Prediction $prediction */
        $prediction = Prediction::query()->findOrFail($id);

        return $prediction;
    }

    /**
     * @param \Domain\Core\Model\ListModelInterface $model
     * @param string                                $category
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findByCategory(ListModelInterface $model, string $category): Collection
    {
        return Prediction::query()
            ->where('category', $category)
            ->offset($model->getOffset())
            ->limit($model->getLimit())
            ->get();
    }
}
