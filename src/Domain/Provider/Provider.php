<?php

declare(strict_types=1);

namespace Domain\Provider;

use Domain\User\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @property-read int $id
 * @property string   $name
 */
class Provider extends Model
{
    public const GOOGLE = 'google';
    public const FACEBOOK = 'facebook';

    public const PROVIDERS = [
        self::GOOGLE,
        self::FACEBOOK,

    ];

    public const RELATION_USER = 'user';

    /** @var string */
    protected $table = 'providers';

    /** @var array */
    protected $fillable = [
        'name',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'provider_user', 'provider_id', 'user_id', 'id', 'id',
            self::RELATION_USER);
    }
}
