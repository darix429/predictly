<?php

declare(strict_types=1);

namespace Domain\Provider\Repository;

use Domain\Provider\Provider;

class ProviderRepository
{
    /**
     * @param string $name
     *
     * @return \Domain\Provider\Provider|null
     * @noinspection PhpIncompatibleReturnTypeInspection
     */
    public function findByName(string $name): ?Provider
    {
        return Provider::query()->where('name', $name)->first();
    }
}
