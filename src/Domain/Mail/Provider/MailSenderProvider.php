<?php

declare(strict_types=1);

namespace Domain\Mail\Provider;

use Domain\Mail\Service\MailSender;
use Domain\Mail\Service\MailSenderInterface;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class MailSenderProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides(): array
    {
        return [
            MailSenderInterface::class,
        ];
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(MailSenderInterface::class, MailSender::class);
    }
}
