<?php

declare(strict_types=1);

namespace Domain\Mail\Service;

use App\Api\Auth\Mail\UserRegistred;
use App\Api\Support\Mail\FeedbackSent;
use App\Api\Support\Mail\QuestionSent;
use Domain\Support\Model\SendFeedbackModel;
use Domain\Support\Model\SendQuestionModel;
use Domain\User\User;
use Illuminate\Support\Facades\Mail;

class MailSender implements MailSenderInterface
{
    /**
     * @param \Domain\User\User $user
     *
     * @return void
     */
    public function sendUserRegisteredSuccessfully(User $user): void
    {
        Mail::send(new UserRegistred($user));
    }

    /**
     * @param \Domain\Support\Model\SendFeedbackModel $model
     */
    public function sendSupportFeedbackMessage(SendFeedbackModel $model): void
    {
        Mail::send(new FeedbackSent($model));
    }

    /**
     * @param \Domain\Support\Model\SendQuestionModel $model
     */
    public function sendQuestionMessage(SendQuestionModel $model): void
    {
        Mail::send(new QuestionSent($model));
    }
}
