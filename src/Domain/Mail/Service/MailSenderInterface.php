<?php

declare(strict_types=1);

namespace Domain\Mail\Service;

use Domain\Support\Model\SendFeedbackModel;
use Domain\Support\Model\SendQuestionModel;
use Domain\User\User;

interface MailSenderInterface
{
    /**
     * @param \Domain\User\User $user
     *
     * @return void
     */
    public function sendUserRegisteredSuccessfully(User $user): void;

    /**
     * @param \Domain\Support\Model\SendFeedbackModel $model
     *
     * @return void
     */
    public function sendSupportFeedbackMessage(SendFeedbackModel $model): void;

    /**
     * @param \Domain\Support\Model\SendQuestionModel $model
     */
    public function sendQuestionMessage(SendQuestionModel $model): void;
}
