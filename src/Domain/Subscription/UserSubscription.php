<?php

declare(strict_types=1);

namespace Domain\Subscription;

use Domain\User\User;
use Domain\Subscription\Subscription;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class UserSubscription extends Model
{

    public const RELATION_USER    = 'user';
    public const RELATION_SUBSCRIPTION = 'subscription';

    /** @var string */
    protected $table = 'user_subscriptions';

    /** @var array */
    protected $fillable = [
        'status',
        'expiry_date',
        'created_at'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id', self::RELATION_USER);
    }

    public function subscription(): BelongsTo
    {
        return $this->belongsTo(Subscription::class, 'subscription_id', 'id', self::RELATION_SUBSCRIPTION);
    }
}
