<?php

declare(strict_types=1);

namespace Domain\Subscription;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Domain\Subscription\UserSubscription;

class Subscription extends Model
{


    /** @var string */
    protected $table = 'subscriptions';

    /** @var array */
    protected $fillable = [
        'name',
        'desc',
        'image',
        'price',
        'duration_days'
    ];

    public function userSubscriptions(): HasMany
    {
        return $this->hasMany(UserSubscription::class, 'subscription_id', 'id');
    }
}
