<?php

declare(strict_types=1);

namespace Domain\Marketplace\Domain;

use Domain\Marketplace\Marketplace;
use Domain\Marketplace\Model\UpdateMarketplaceModel;

class MarketplaceUpdater
{
    /**
     * @param \Domain\Marketplace\Marketplace                  $marketplace
     * @param \Domain\Marketplace\Model\UpdateMarketplaceModel $model
     *
     * @return bool
     */
    public function update(Marketplace $marketplace, UpdateMarketplaceModel $model): bool
    {
        $marketplace->name     = $model->getName();
        $marketplace->fee      = $model->getFee();
        $marketplace->category = $model->getCategory();

        return $marketplace->save();
    }
}
