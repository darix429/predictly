<?php

declare(strict_types=1);

namespace Domain\Marketplace\Domain;

use Domain\Marketplace\Marketplace;
use Domain\Marketplace\Model\StoreMarketplaceModel;
use Domain\Marketplace\Model\UpdateMarketplaceModel;

class MarketplaceDomain
{
    /**
     * @var \Domain\Marketplace\Domain\MarketplaceCreator
     */
    private MarketplaceCreator $creator;

    /**
     * @var \Domain\Marketplace\Domain\MarketplaceUpdater
     */
    private MarketplaceUpdater $updater;

    /**
     * MarketplaceDomain constructor.
     *
     * @param \Domain\Marketplace\Domain\MarketplaceCreator $creator
     * @param \Domain\Marketplace\Domain\MarketplaceUpdater $updater
     */
    public function __construct(MarketplaceCreator $creator, MarketplaceUpdater $updater)
    {
        $this->creator = $creator;
        $this->updater = $updater;
    }

    /**\
     * @param \Domain\Marketplace\Model\StoreMarketplaceModel $model
     *
     * @return \Domain\Marketplace\Marketplace
     */
    public function create(StoreMarketplaceModel $model): Marketplace
    {
        return $this->creator->create($model);
    }

    /**
     * @param \Domain\Marketplace\Marketplace                  $marketplace
     * @param \Domain\Marketplace\Model\UpdateMarketplaceModel $model
     *
     * @return bool
     */
    public function update(Marketplace $marketplace, UpdateMarketplaceModel $model): bool
    {
        return $this->updater->update($marketplace, $model);
    }
}
