<?php

declare(strict_types=1);

namespace Domain\Marketplace\Domain;

use Domain\Marketplace\Marketplace;
use Domain\Marketplace\Model\StoreMarketplaceModel;

class MarketplaceCreator
{
    /**
     * @param \Domain\Marketplace\Model\StoreMarketplaceModel $model
     *
     * @return \Domain\Marketplace\Marketplace
     */
    public function create(StoreMarketplaceModel $model): Marketplace
    {
        $marketplace = new Marketplace();

        $marketplace->name     = $model->getName();
        $marketplace->fee      = $model->getFee();
        $marketplace->category = $model->getCategory();

        $marketplace->save();

        return $marketplace;
    }
}
