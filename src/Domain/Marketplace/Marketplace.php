<?php

declare(strict_types=1);

namespace Domain\Marketplace;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property int    $id
 * @property string $name
 * @property int    $fee
 * @property string $category
 */
class Marketplace extends Model
{
    public const AMAZON_MARKETPLACE = 'Amazon';

    public const PRIMARY_CATEGORY = 'primary';
    public const TESTING_CATEGORY = 'testing';
    public const DRAFTS_CATEGORY  = 'drafts';

    public const CATEGORIES = [
        self::PRIMARY_CATEGORY,
        self::TESTING_CATEGORY,
        self::DRAFTS_CATEGORY,
    ];

    /** @var string */
    protected $table = 'marketplaces';

    /** @var array */
    protected $fillable = [
        'name',
        'fee',
        'category',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sessions(): HasMany
    {
        return $this->hasMany(Marketplace::class, 'marketplace_id', 'id');
    }
}
