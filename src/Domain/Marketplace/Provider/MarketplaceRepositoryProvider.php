<?php

declare(strict_types=1);

namespace Domain\Marketplace\Provider;

use Domain\Marketplace\Repository\MarketplaceRepository;
use Domain\Marketplace\Repository\MarketplaceRepositoryInterface;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class MarketplaceRepositoryProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            MarketplaceRepositoryInterface::class,
        ];
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(MarketplaceRepositoryInterface::class, MarketplaceRepository::class);
    }
}
