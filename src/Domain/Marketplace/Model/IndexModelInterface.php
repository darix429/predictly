<?php

declare(strict_types=1);

namespace Domain\Marketplace\Model;

use Domain\Core\Model\ListModelInterface;

interface IndexModelInterface extends ListModelInterface
{
    /**
     * @return bool
     */
    public function hasCategory(): bool;

    /**
     * @return string|null
     */
    public function getCategory(): ?string;
}
