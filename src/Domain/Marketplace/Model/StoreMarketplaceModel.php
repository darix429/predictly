<?php

declare(strict_types=1);

namespace Domain\Marketplace\Model;

interface StoreMarketplaceModel
{
    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return int
     */
    public function getFee(): int;

    /**
     * @return string
     */
    public function getCategory(): string;
}
