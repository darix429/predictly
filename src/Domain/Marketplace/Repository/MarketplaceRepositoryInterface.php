<?php

declare(strict_types=1);

namespace Domain\Marketplace\Repository;

use Domain\Core\Model\ListModelInterface;
use Domain\Marketplace\Marketplace;
use Illuminate\Database\Eloquent\Collection;

interface MarketplaceRepositoryInterface
{
    /**
     * @param \Domain\Core\Model\ListModelInterface $model
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findAll(ListModelInterface $model): Collection;

    /**
     * @param int $id
     *
     * @return \Domain\Marketplace\Marketplace
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findOrFail(int $id): Marketplace;

    /**
     * @param \Domain\Core\Model\ListModelInterface $model
     * @param string                                $category
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findByCategory(ListMOdelInterface $model, string $category): Collection;
}
