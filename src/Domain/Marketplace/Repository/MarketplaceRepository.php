<?php

declare(strict_types=1);

namespace Domain\Marketplace\Repository;

use Domain\Core\Model\ListModelInterface;
use Domain\Marketplace\Marketplace;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class MarketplaceRepository implements MarketplaceRepositoryInterface
{
    /**
     * @param \Domain\Core\Model\ListModelInterface $model
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findAll(ListModelInterface $model): Collection
    {
        return Marketplace::query()->offset($model->getOffset())->limit($model->getLimit())->get();
    }

    /**
     * @param int $id
     *
     * @return \Domain\Marketplace\Marketplace
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public function findOrFail(int $id): Marketplace
    {
        /** @var Marketplace $marketplace */
        $marketplace = Marketplace::query()->findOrFail($id);

        return $marketplace;
    }

    /**
     * @param \Domain\Core\Model\ListModelInterface $model
     * @param string                                $category
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findByCategory(ListModelInterface $model, string $category): Collection
    {
        /** @var \Domain\Marketplace\Marketplace $marketplace */
        return Marketplace::query()
            ->where('category', $category)
            ->offset($model->getOffset())
            ->limit($model->getLimit())
            ->get();
    }
}
