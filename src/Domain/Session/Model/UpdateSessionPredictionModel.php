<?php

declare(strict_types=1);

namespace Domain\Session\Model;

interface UpdateSessionPredictionModel
{
    /**
     * @return int
     */
    public function getPredictionId(): int;
}
