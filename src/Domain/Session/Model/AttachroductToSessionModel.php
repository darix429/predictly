<?php

declare(strict_types=1);

namespace Domain\Session\Model;

interface AttachroductToSessionModel
{
    /**
     * @return int
     */
    public function getProductId(): int;
}
