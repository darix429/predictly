<?php

declare(strict_types=1);

namespace Domain\Session\Model;

interface UpdateSessionMarketplaceModel
{
    /**
     * @return int
     */
    public function getMarketplaceId(): int;
}
