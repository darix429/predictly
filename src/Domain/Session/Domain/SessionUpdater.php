<?php

declare(strict_types=1);

namespace Domain\Session\Domain;

use Domain\Factor\Domain\FactorCreator;
use Domain\Factor\Factor;
use Domain\Factor\Model\FactorableInterface;
use Domain\Marketplace\Marketplace;
use Domain\Prediction\Prediction;
use Domain\Product\Product;
use Domain\Session\Domain\Validator\AttachProductValidator;
use Domain\Session\Session;

class SessionUpdater
{
    /**
     * @var \Domain\Session\Domain\Validator\AttachProductValidator
     */
    private AttachProductValidator $attachProductValidator;

    /**
     * SessionUpdater constructor.
     *
     * @param \Domain\Session\Domain\Validator\AttachProductValidator $attachProductValidator
     */
    public function __construct(AttachProductValidator $attachProductValidator)
    {
        $this->attachProductValidator = $attachProductValidator;
    }

    /**
     * @param \Domain\Session\Session         $session
     * @param \Domain\Marketplace\Marketplace $marketplace
     *
     * @return void
     */
    public function updateMarketplace(Session $session, Marketplace $marketplace): void
    {
        $session->marketplace()->associate($marketplace)->save();
    }

    /**
     * @param \Domain\Session\Session       $session
     * @param \Domain\Prediction\Prediction $prediction
     *
     * @return void
     */
    public function updatePrediction(Session $session, Prediction $prediction): void
    {
        $session->prediction()->associate($prediction)->save();
    }

    /**
     * @param \Domain\Session\Session $session
     * @param \Domain\Product\Product $product
     *
     * @return void
     * @throws \Illuminate\Validation\ValidationException
     */
    public function attachProduct(Session $session, Product $product): void
    {
        $this->attachProductValidator->validate($session, $product);

        $session->products()->syncWithoutDetaching($product);
    }

    /**
     * @param \Domain\Session\Session $session
     * @param \Domain\Product\Product $product
     *
     * @return void
     */
    public function detachProduct(Session $session, Product $product): void
    {
        $session->products()->detach($product);
    }
}
