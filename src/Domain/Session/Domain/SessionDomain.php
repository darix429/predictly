<?php

declare(strict_types=1);

namespace Domain\Session\Domain;

use Domain\Factor\Domain\FactorDomain;
use Domain\Marketplace\Marketplace;
use Domain\Prediction\Prediction;
use Domain\Product\Product;
use Domain\Product\Repository\ProductRepositoryInterface;
use Domain\Session\Repository\SessionRepositoryInterface;
use Domain\Session\Session;
use Domain\User\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SessionDomain
{
    /**
     * @var \Domain\Session\Domain\SessionCreator
     */
    private SessionCreator $sessionCreator;

    /**
     * @var \Domain\Session\Repository\SessionRepositoryInterface
     */
    private SessionRepositoryInterface $sessionRepository;

    /**
     * @var \Domain\Session\Domain\SessionUpdater
     */
    private SessionUpdater $sessionUpdater;
    /**
     * @var \Domain\Product\Repository\ProductRepositoryInterface
     */
    private ProductRepositoryInterface $productRepository;
    /**
     * @var \Domain\Factor\Domain\FactorDomain
     */
    private FactorDomain $factorDomain;

    /**
     * SessionDomain constructor.
     *
     * @param \Domain\Session\Domain\SessionCreator                 $sessionCreator
     * @param \Domain\Session\Domain\SessionUpdater                 $sessionUpdater
     * @param \Domain\Session\Repository\SessionRepositoryInterface $sessionRepository
     * @param \Domain\Product\Repository\ProductRepositoryInterface $productRepository
     * @param \Domain\Factor\Domain\FactorDomain                    $factorDomain
     */
    public function __construct(
        SessionCreator $sessionCreator,
        SessionUpdater $sessionUpdater,
        SessionRepositoryInterface $sessionRepository,
        ProductRepositoryInterface $productRepository,
        FactorDomain $factorDomain
    ) {
        $this->sessionCreator    = $sessionCreator;
        $this->sessionUpdater    = $sessionUpdater;
        $this->sessionRepository = $sessionRepository;
        $this->productRepository = $productRepository;
        $this->factorDomain      = $factorDomain;
    }

    /**
     * @param \Domain\User\User $user
     *
     * @return \Domain\Session\Session
     */
    public function getActiveForUser(User $user): Session
    {
        try {
            return $this->sessionRepository->findActiveForUser($user);
        } catch (ModelNotFoundException $ex) {
            return $this->sessionCreator->createEmptySession($user);
        }
    }

    /**
     * @param \Domain\Session\Session         $session
     * @param \Domain\Marketplace\Marketplace $marketplace
     *
     * @return void
     */
    public function updateMarketplace(Session $session, Marketplace $marketplace): void
    {
        $this->sessionUpdater->updateMarketplace($session, $marketplace);
    }

    /**
     * @param \Domain\Session\Session       $session
     * @param \Domain\Prediction\Prediction $prediction
     *
     * @return void
     */
    public function updatePrediction(Session $session, Prediction $prediction): void
    {
        $this->sessionUpdater->updatePrediction($session, $prediction);
    }

    /**
     * @param \Domain\Session\Session $session
     * @param \Domain\Product\Product $product
     *
     * @return void
     * @throws \Illuminate\Validation\ValidationException
     */
    public function attachProduct(Session $session, Product $product): void
    {
        $this->sessionUpdater->attachProduct($session, $product);
    }

    /**
     * @param \Domain\Session\Session $session
     * @param \Domain\Product\Product $product
     *
     * @return void
     */
    public function detachProduct(Session $session, Product $product): void
    {
        $this->sessionUpdater->detachProduct($session, $product);
    }

    /**
     * @param \Domain\User\User                           $user
     * @param \Domain\Session\Session                     $calculatedSession
     * @param \Domain\Factor\Model\FactorModelInterface[] $factors
     *
     * @return \Domain\Session\Session
     * @throws \Illuminate\Validation\ValidationException
     */
    public function tune(User $user, Session $calculatedSession, array $factors): Session
    {
        $session = $this->getActiveForUser($user);

        /** @var \Domain\Marketplace\Marketplace $calculatedMarketplace */
        $calculatedMarketplace = $calculatedSession->marketplace()->first();

        /** @var \Domain\Prediction\Prediction $calculatedPrediction */
        $calculatedPrediction = $calculatedSession->prediction()->first();

        $this->updateMarketplace($session, $calculatedMarketplace);
        $this->updatePrediction($session, $calculatedPrediction);

        /** @var \Domain\Factor\Model\FactorModelInterface $factor */
        foreach ($factors as $factor) {
            /** @var \Domain\Product\Product $product */
            $product = $this->productRepository->findOrFail($factor->getFactorableId());
            $this->factorDomain->create($factor, $product);

            $this->attachProduct($session, $product);
        }

        return $session;
    }
}
