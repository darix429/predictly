<?php

declare(strict_types=1);

namespace Domain\Session\Domain\Validator;

use Domain\Product\Product;
use Domain\Product\Repository\ProductRepositoryInterface;
use Domain\Session\Session;
use Illuminate\Validation\ValidationException;

class AttachProductValidator
{
    /**
     * @var array
     */
    private array $config;

    /**
     * @var \Domain\Product\Repository\ProductRepositoryInterface
     */
    private ProductRepositoryInterface $productRepository;

    /**
     * AttachProductValidator constructor.
     *
     * @param array                                                 $config
     * @param \Domain\Product\Repository\ProductRepositoryInterface $productRepository
     */
    public function __construct(array $config, ProductRepositoryInterface $productRepository)
    {
        $this->config = $config;
        $this->productRepository = $productRepository;
    }

    /**
     * @param \Domain\Session\Session $session
     * @param \Domain\Product\Product $product
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function validate(Session $session, Product $product)
    {
        if (!$session->active) {
            throw ValidationException::withMessages(['Session is not active']);
        }

        if ($this->productRepository->countForSessionByProductAndSession($product, $session) > 0) {
            throw ValidationException::withMessages(["You can't add the same product to session twice"]);
        }

        $freeProducts = $this->config['free']['sessions']['products']['count'];

        if ($this->productRepository->countForSession($session) >= $freeProducts) {//todo: For payment sessions move to config
            throw ValidationException::withMessages(["You can't add more than $freeProducts products to compare"]);
        }
    }
}
