<?php

declare(strict_types=1);

namespace Domain\Session\Domain;

use Domain\Marketplace\Marketplace;
use Domain\Marketplace\Repository\MarketplaceRepository;
use Domain\Prediction\Prediction;
use Domain\Session\Repository\SessionRepositoryInterface;
use Domain\Session\Session;
use Domain\User\User;

class SessionCreator
{
    /**
     * @var \Domain\Session\Repository\SessionRepositoryInterface
     */
    private SessionRepositoryInterface $sessionRepository;

    /**
     * @var \Domain\Marketplace\Repository\MarketplaceRepository
     */
    private MarketplaceRepository $marketplaceRepository;

    /**
     * SessionCreator constructor.
     *
     * @param \Domain\Session\Repository\SessionRepositoryInterface $sessionRepository
     * @param \Domain\Marketplace\Repository\MarketplaceRepository  $marketplaceRepository
     */
    public function __construct(
        SessionRepositoryInterface $sessionRepository,
        MarketplaceRepository $marketplaceRepository
    ) {
        $this->sessionRepository     = $sessionRepository;
        $this->marketplaceRepository = $marketplaceRepository;
    }

    /**
     * @param \Domain\User\User $user
     *
     * @return \Domain\Session\Session
     */
    public function createEmptySession(User $user): Session
    {
        $session         = new Session();

        $session->active = true;
        $session->user()->associate($user);
        $session->save();

        return $session;
    }
}
