<?php

declare(strict_types=1);

namespace Domain\Session\Provider;

use Domain\Product\Repository\ProductRepositoryInterface;
use Domain\Session\Domain\Validator\AttachProductValidator;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class AttachProductValidatorProvider extends ServiceProvider
{
    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            AttachProductValidator::class,
        ];
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(AttachProductValidator::class, function (Application $application) {
            /** @var \Illuminate\Contracts\Config\Repository $configRepository */
            $configRepository = $application->make(Repository::class);
            $config = $configRepository->get('payment');

            $productRepository = $application->make(ProductRepositoryInterface::class);

            return new AttachProductValidator($config, $productRepository);
        });
    }
}
