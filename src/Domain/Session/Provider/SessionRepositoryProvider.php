<?php

declare(strict_types=1);

namespace Domain\Session\Provider;

use Domain\Session\Repository\SessionRepository;
use Domain\Session\Repository\SessionRepositoryInterface;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class SessionRepositoryProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            SessionRepositoryInterface::class,
        ];
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(SessionRepositoryInterface::class, SessionRepository::class);
    }
}
