<?php

declare(strict_types=1);

namespace Domain\Session\Result\Repository;

use Domain\Session\Session;
use Illuminate\Database\Eloquent\Collection;

interface SessionResultRepositoryInterface
{
    /**
     * @param \Domain\Session\Session $session
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findBySession(Session $session): Collection;
}
