<?php

declare(strict_types=1);

namespace Domain\Session\Result\Repository;

use Domain\Session\Session;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class SessionResultRepository implements SessionResultRepositoryInterface
{
    /**
     * @var \Illuminate\Database\Eloquent\Builder
     */
    private Builder $builder;

    /**
     * SessionResultRepository constructor.
     *
     * @param \Illuminate\Database\Eloquent\Builder $builder
     */
    public function __construct(Builder $builder)
    {
        $this->builder = $builder;
    }

    /**
     * @param \Domain\Session\Session $session
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findBySession(Session $session): Collection
    {
        return $this->builder->where('session_id', $session->id)->get();
    }
}
