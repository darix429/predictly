<?php

declare(strict_types=1);

namespace Domain\Session\Result\Provider;

use Domain\Session\Result\Repository\SessionResultRepository;
use Domain\Session\Result\Repository\SessionResultRepositoryInterface;
use Domain\Session\Result\SessionResult;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

class SessionResultRepositoryProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            SessionResultRepositoryInterface::class,
        ];
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(SessionResultRepositoryInterface::class,
            fn(): SessionResultRepository => new SessionResultRepository(SessionResult::query()));
    }
}
