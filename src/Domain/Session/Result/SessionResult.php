<?php

declare(strict_types=1);

namespace Domain\Session\Result;

use Domain\Product\Product;
use Domain\Session\Session;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property-read int $id
 * @property int      $cost
 * @property int      $minimum_order_quantity
 * @property int      $break_even_sales_price
 * @property int      $break_even_sales_quantity
 * @property int      $initial_investment
 * @property int      $investment_return_in
 * @property int      $self_sustainability_in
 * @property int      $total_profit
 * @property array    $cash_conversion_cycle
 * @property array    $profit_timeline
 */
class SessionResult extends Model
{
    private const RELATION_SESSION = 'session';
    private const RELATION_PRODUCT = 'product';

    /**
     * @var string
     */
    protected $table = 'session_results';

    /**
     * @var array
     */
    protected $fillable = [
        'initial_investment',
        'cost',
        'minimum_order_quantity',
        'break_even_sales_price',
        'break_even_sales_quantity',
        'investment_return_in',
        'self_sustainability_in',
        'total_profit',
        'cash_conversion_cycle',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'cash_conversion_cycle' => 'array',
        'profit_timeline'       => 'array',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function session(): BelongsTo
    {
        return $this->belongsTo(Session::class, 'session_id', 'id', self::RELATION_SESSION);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class, 'product_id', 'id', self::RELATION_PRODUCT);
    }
}
