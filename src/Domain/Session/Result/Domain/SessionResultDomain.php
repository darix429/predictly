<?php

declare(strict_types=1);

namespace Domain\Session\Result\Domain;

use Domain\Calculation\Model\CalculationResultModelInterface;
use Domain\Session\Session;

class SessionResultDomain
{
    /**
     * @var \Domain\Session\Result\Domain\SessionResultCreator
     */
    private SessionResultCreator $creator;

    /**
     * SessionResultDomain constructor.
     *
     * @param \Domain\Session\Result\Domain\SessionResultCreator $creator
     */
    public function __construct(SessionResultCreator $creator)
    {
        $this->creator = $creator;
    }

    /**
     * @param \Domain\Session\Session $session
     * @param array                   $calculationResultModels
     *
     * @return \Domain\Session\Result\SessionResult[]
     * @throws \Exception
     */
    public function createFromCalculationResultModels(Session $session, array $calculationResultModels): array
    {
        return $this->creator->createFromCalculationResultModels($session, $calculationResultModels);
    }
}
