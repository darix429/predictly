<?php

declare(strict_types=1);

namespace Domain\Session\Result\Domain;

use Domain\Calculation\Model\CalculationResultModelInterface;
use Domain\Core\CurrencyTransformer;
use Domain\Session\Result\SessionResult;
use Domain\Session\Session;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class SessionResultCreator
{
    /**
     * @param \Domain\Session\Session                                     $session
     * @param \Domain\Calculation\Model\CalculationResultModelInterface[] $calculationResultModels
     *
     * @return \Domain\Session\Result\SessionResult[]
     * @throws \Exception
     */
    public function createFromCalculationResultModels(Session $session, array $calculationResultModels): array
    {
        $sessionResults = [];

        DB::beginTransaction();

        try {
            foreach ($calculationResultModels as $model) {
                $sessionResults[] = $this->createFromCalculationResultModel($session, $model);
            }
        } catch (\PDOException $exception) {
            DB::rollBack();

            throw new \Exception('Model didn\'t save');
        }

        DB::commit();

        return $sessionResults;
    }

    /**
     * @param \Domain\Session\Session                                   $session
     * @param \Domain\Calculation\Model\CalculationResultModelInterface $calculationResultModel
     *
     * @return \Domain\Session\Result\SessionResult
     */
    public function createFromCalculationResultModel(
        Session $session,
        CalculationResultModelInterface $calculationResultModel
    ): SessionResult {
        $sessionResult = new SessionResult();

        $sessionResult->cost                      = $calculationResultModel->getProductCost();
        $sessionResult->minimum_order_quantity    = $calculationResultModel->getInitialMinimumOrderQuantity();
        $sessionResult->break_even_sales_price    = $calculationResultModel->getBreakEvenSalesPrice();
        $sessionResult->break_even_sales_quantity = $calculationResultModel->getBreakEvenSalesQuantity();
        $sessionResult->initial_investment        = $calculationResultModel->getInitialInvestment();
        $sessionResult->investment_return_in      = $calculationResultModel->getInvestmentReturnIn();
        $sessionResult->self_sustainability_in    = $calculationResultModel->getSelfSustainabilityIn();
        $sessionResult->total_profit              = $calculationResultModel->getTotalProfit();
        $sessionResult->cash_conversion_cycle     = $calculationResultModel->getConversionCycle();
        $sessionResult->profit_timeline           = $calculationResultModel->getProfitTimeline();

        $sessionResult->session()->associate($session);
        $sessionResult->product()->associate($calculationResultModel->getProduct());

        $session->update([
            'active' => false,
        ]);

        if (!$sessionResult->save() || !$session->save()) {
            throw new \PDOException();
        }

        return $sessionResult;
    }
}
