<?php

declare(strict_types=1);

namespace Domain\Session\Repository;

use Domain\Session\Session;
use Domain\User\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SessionRepository implements SessionRepositoryInterface
{
    /**
     * @param \Domain\User\User $user
     *
     * @return \Domain\Session\Session
     */
    public function findActiveForUser(User $user): Session
    {
        /** @var \Domain\Session\Session $session */
        $session = Session::query()->where('user_id', $user->id)->where('active', true)->first();

        if (!$session) {
            throw new ModelNotFoundException();
        }

        return $session;
    }

    /**
     * @param \Domain\User\User $user
     *
     * @return \Domain\Session\Session|null
     */
    public function findLastCalculatedForUser(User $user): ?Session
    {
        /** @var \Domain\Session\Session $session */
        $session = Session::query()
            ->where('user_id', $user->id)
            ->where('active', false)
            ->orderBy('id', 'desc')
            ->first();

        return $session;
    }

    /**
     * @param \Domain\User\User $user
     * @param int               $sessionId
     *
     * @return \Domain\Session\Session
     */
    public function findOrFailCalculatedForUser(User $user, int $sessionId): Session
    {
        /** @var \Domain\Session\Session $session */
        $session =Session::query()
            ->where('user_id', $user->id)
            ->where('id', $sessionId)
            ->where('active', false)
            ->firstOrFail();

        return $session;
    }

    /**
     * @param \Domain\User\User $user
     * @param int               $id
     *
     * @return \Domain\Session\Session
     */
    public function findActiveByUserAndSessionId(User $user, int $id): Session
    {
        /** @var \Domain\Session\Session $session */
        $session = Session::query()
            ->where('user_id', $user->id)
            ->where('id', $id)
            ->where('active', true)
            ->first();

        if (!$session) {
            throw new ModelNotFoundException('Session not found');
        }

        return $session;
    }
}
