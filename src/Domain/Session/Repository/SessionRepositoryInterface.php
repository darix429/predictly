<?php

declare(strict_types=1);

namespace Domain\Session\Repository;

use Domain\Session\Session;
use Domain\User\User;

interface SessionRepositoryInterface
{
    /**
     * @param \Domain\User\User $user
     *
     * @return \Domain\Session\Session
     */
    public function findActiveForUser(User $user): Session;

    /**
     * @param \Domain\User\User $user
     *
     * @return \Domain\Session\Session|null
     */
    public function findLastCalculatedForUser(User $user): ?Session;

    /**
     * @param \Domain\User\User $user
     * @param int               $sessionId
     *
     * @return Session
     */
    public function findOrFailCalculatedForUser(User $user, int $sessionId): Session;

    /**
     * @param \Domain\User\User $user
     * @param int               $id
     *
     * @return \Domain\Session\Session
     */
    public function findActiveByUserAndSessionId(User $user, int $id): Session;
}
