<?php

declare(strict_types=1);

namespace Domain\Session;

use Domain\Factor\Factor;
use Domain\Factor\Model\FactorableInterface;
use Domain\Marketplace\Marketplace;
use Domain\Prediction\Prediction;
use Domain\Product\Product;
use Domain\Session\Result\SessionResult;
use Domain\User\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;

/**
 * @property-read int $id
 * @property int      user_id
 * @property int      marketplace_id
 * @property int      prediction_id
 * @property boolean  $active
 */
class Session extends Model implements FactorableInterface
{
    private const RELATION_PRODUCT = 'product';
    private const RELATION_USER = 'user';
    private const RELATION_MARKETPLACE = 'marketplace';
    private const RELATION_PREDICTION = 'prediction';

    /**
     * @var string
     */
    protected $table = 'sessions';

    /**
     * @var array
     */
    protected $fillable = [
        'active',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'active' => 'boolean',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id', self::RELATION_USER);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function marketplace(): BelongsTo
    {
        return $this->belongsTo(Marketplace::class, 'marketplace_id', 'id', self::RELATION_MARKETPLACE);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function prediction(): BelongsTo
    {
        return $this->belongsTo(Prediction::class, 'prediction_id', 'id', self::RELATION_PREDICTION);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sessionResults(): HasMany
    {
        return $this->hasMany(SessionResult::class, 'session_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function factor(): MorphOne
    {
        return $this->morphOne(Factor::class, 'factorable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products(): BelongsToMany
    {
        return $this->belongsToMany(
            Product::class,
            'product_session',
            'session_id',
            'product_id',
            'id',
            'id',
            self::RELATION_PRODUCT);
    }
}
