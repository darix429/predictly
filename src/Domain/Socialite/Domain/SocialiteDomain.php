<?php

declare(strict_types=1);

namespace Domain\Socialite\Domain;

use Domain\User\Domain\UserDomain;
use Domain\User\Repository\UserRepositoryInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Passport\Bridge\User as UserEntity;
use Laravel\Socialite\SocialiteManager;
use League\OAuth2\Server\Exception\OAuthServerException;

/**
 * Class SocialiteDomain
 *
 * @package Domain\Socialite\Domain
 */
class SocialiteDomain
{
    /**
     * @var \Domain\User\Domain\UserDomain
     */
    private UserDomain $userDomain;

    /**
     * The user repository instance.
     *
     * @var \Domain\User\Repository\UserRepositoryInterface
     */
    protected UserRepositoryInterface $userRepository;

    /**
     * @var \Laravel\Socialite\SocialiteManager
     */
    private SocialiteManager $socialiteManager;

    /**
     * Create a social user provider instance.
     *
     * @param \Domain\User\Domain\UserDomain                  $userDomain
     * @param \Domain\User\Repository\UserRepositoryInterface $userRepository
     * @param \Laravel\Socialite\SocialiteManager             $socialiteManager
     */
    public function __construct(
        UserDomain $userDomain,
        UserRepositoryInterface $userRepository,
        SocialiteManager $socialiteManager
    ) {
        $this->userDomain       = $userDomain;
        $this->userRepository   = $userRepository;
        $this->socialiteManager = $socialiteManager;
    }

    /**
     * @param $provider
     * @param $accessToken
     *
     * @return \Laravel\Passport\Bridge\User
     * @throws \League\OAuth2\Server\Exception\OAuthServerException
     */
    public function getUserEntityByAccessToken($provider, $accessToken)
    {
        $user = $this->getUserFromSocialProvider($provider, $accessToken);

        return new UserEntity($user->getAuthIdentifier());
    }

    /**
     * Get the user from the specified provider using the given access token.
     *
     * @param string $provider
     * @param string $accessToken
     *
     * @return \Domain\User\User
     * @throws \League\OAuth2\Server\Exception\OAuthServerException
     *
     */
    private function getUserFromSocialProvider($provider, $accessToken)
    {
        try {
            /** @var \Laravel\Socialite\Two\User $user */
            $user = $this->socialiteManager->driver($provider)->userFromToken($accessToken);

            return $this->userRepository->findByProviderAndId($provider, $user->getId());
        } catch (\Exception|ModelNotFoundException $ex) {
            throw new OAuthServerException(
                'Authentication error, invalid access token',
                $errorCode = 400,
                'invalid_request'
            );
        }
    }
}
