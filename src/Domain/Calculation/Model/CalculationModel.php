<?php

declare(strict_types=1);

namespace Domain\Calculation\Model;

use Domain\Factor\Factor;
use Domain\Marketplace\Marketplace;
use Domain\Product\Product;

class CalculationModel implements CalculationModelInterface
{
    /**
     * @var \Domain\Product\Product
     */
    protected Product $product;

    /**
     * @var int
     */
    protected int $averageCost;

    /**
     * @var int
     */
    protected int $averageMonthlySales;

    /**
     * @var int|null
     */
    protected ?int $averageSalesRank;

    /**
     * @var int
     */
    private int $salesPriceAssumption;

    /**
     * @var int
     */
    private int $salesPriceIncrease;

    /**
     * @var int
     */
    private int $monthlySales;

    /**
     * @var int
     */
    private int $salesGrowthRate;

    /**
     * @var int
     */
    private int $stableGrowthRate;

    /**
     * @var int|null
     */
    protected ?int $reviews;

    /**
     * @var int
     */
    protected int $marketplaceFee;

    /**
     * @var int
     */
    private int $revenuePeriodInMonths;

    /**
     * @var int|null
     */
    private ?int $productCost;

    /**
     * @var int|null
     */
    private ?int $minimumOrderQuantity;

    /**
     * CalculationModel constructor.
     *
     * @param \Domain\Marketplace\Marketplace $marketplace
     * @param \Domain\Factor\Factor           $factor
     * @param \Domain\Product\Product         $product
     * @param int                             $revenuePeriodInMonths
     */
    private function __construct(
        Marketplace $marketplace,
        Factor $factor,
        Product $product,
        int $revenuePeriodInMonths
    ) {
        $this->marketplaceFee        = $marketplace->fee;
        $this->product               = $product;
        $this->averageCost           = $product->cost;
        $this->averageMonthlySales   = $product->monthly_sales;
        $this->salesPriceAssumption  = $factor->sales_price_assumption;
        $this->salesPriceIncrease    = $factor->sales_price_increase;
        $this->monthlySales          = $factor->monthly_sales;
        $this->salesGrowthRate       = $factor->sales_growth_rate;
        $this->stableGrowthRate      = $factor->stable_growth_rate;
        $this->reviews               = $product->reviews;
        $this->averageSalesRank      = $product->sales_rank;
        $this->productCost           = $factor->product_cost;
        $this->minimumOrderQuantity  = $factor->minimum_order_quantity;
        $this->revenuePeriodInMonths = $revenuePeriodInMonths;
    }

    /**
     * @return \Domain\Product\Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @return int
     */
    public function getAverageCost(): int
    {
        return $this->averageCost;
    }

    /**
     * @return int
     */
    public function getAverageMonthlySales(): int
    {
        return $this->averageMonthlySales;
    }

    /**
     * @return float
     */
    public function getMarketplaceFee(): float
    {
        return round($this->marketplaceFee / 100, 2);
    }

    /**
     * @return float
     */
    public function getSalesPriceAssumption(): float
    {
        return round($this->salesPriceAssumption / 100, 2);
    }

    /**
     * @return float
     */
    public function getSalesPriceIncrease(): float
    {
        return round($this->salesPriceIncrease / 100, 2);
    }

    /**
     * @return float
     */
    public function getMonthlySalesAssumptions(): float
    {
        return round($this->monthlySales / 100, 2);
    }

    /**
     * @return float
     */
    public function getSalesGrowthRate(): float
    {
        return round($this->salesGrowthRate / 100, 2);
    }

    /**
     * @return float
     */
    public function getStableGrowthRate(): float
    {
        return round($this->stableGrowthRate / 100, 2);
    }

    /**
     * @return int|null
     */
    public function getAverageSalesRank(): ?int
    {
        return $this->averageSalesRank;
    }

    /**
     * @return int|null
     */
    public function getReviews(): ?int
    {
        return $this->reviews;
    }

    /**
     * @return int
     */
    public function getRevenuePeriodInMonths(): int
    {
        return $this->revenuePeriodInMonths;
    }

    /**
     * @return bool
     */
    public function hasProductCost(): bool
    {
        return (bool)$this->productCost;
    }

    /**
     * @return int
     */
    public function getProductCost(): int
    {
        return $this->productCost;
    }

    /**
     * @return bool
     */
    public function hasMinimumOrderQuantity(): bool
    {
        return (bool)$this->minimumOrderQuantity;
    }

    /**
     * @return int
     */
    public function getMinimumOrderQuantity(): int
    {
        return $this->minimumOrderQuantity;
    }

    /**
     * @param \Domain\Marketplace\Marketplace $marketplace
     * @param \Domain\Product\Product         $product
     *
     * @param int                             $revenuePeriodInMonths
     *
     * @return \Domain\Calculation\Model\CalculationModelInterface
     */
    public static function create(
        Marketplace $marketplace,
        Product $product,
        int $revenuePeriodInMonths = 6
    ): CalculationModelInterface {
        /** @var \Domain\Factor\Factor $factor */
        $factor = $product->factor()->orderBy('id', 'desc')->first();

        return new static($marketplace, $factor, $product, $revenuePeriodInMonths);
    }
}
