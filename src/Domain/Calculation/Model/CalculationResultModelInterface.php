<?php

declare(strict_types=1);

namespace Domain\Calculation\Model;

use Domain\Product\Product;

interface CalculationResultModelInterface
{
    /**
     * @return \Domain\Product\Product
     */
    public function getProduct(): Product;

    /**
     * @return int
     */
    public function getProductCost(): int;

    /**
     * @return float
     */
    public function getInitialMinimumOrderQuantity(): int;

    /**
     * @return int
     */
    public function getBreakEvenSalesPrice(): int;

    /**
     * @return int
     */
    public function getBreakEvenSalesQuantity(): int;

    /**
     * @return int
     */
    public function getInitialInvestment(): int;

    /**
     * @return int|null
     */
    public function getInvestmentReturnIn(): ?int;

    /**
     * @return int|null
     */
    public function getSelfSustainabilityIn(): ?int;

    /**
     * @return int
     */
    public function getTotalProfit(): int;

    /**
     * @return array
     */
    public function getConversionCycle(): array;

    /**
     * @return array
     */
    public function getProfitTimeline(): array;
}
