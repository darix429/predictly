<?php

declare(strict_types=1);

namespace Domain\Calculation\Model;

use Domain\Product\Product;

class CalculationResultModel implements CalculationResultModelInterface
{
    /**
     * @var \Domain\Product\Product
     */
    private Product $product;

    /**
     * @var int
     */
    private int $productCost;

    /**
     * @var int
     */
    private int $initialMinimumOrderQuantity;

    /**
     * @var int
     */
    private int $breakEvenSalesPrice;

    /**
     * @var int
     */
    private int $breakEvenSalesQuantity;

    /**
     * @var int
     */
    private int $initialInvestment;

    /**
     * @var int|null
     */
    private ?int $investmentReturnIn;

    /**
     * @var int|null
     */
    private ?int $selfSustainabilityIn;

    /**
     * @var int
     */
    private int $totalProfit;

    /**
     * @var array
     */
    private array $cashConversionCycle;

    /**
     * @var array
     */
    private array $profitTimeline;

    /**
     * CalculationResultModel constructor.
     *
     * @param \Domain\Product\Product $product
     * @param int                     $productCost
     * @param int                     $initialMinimumOrderQuantity
     * @param int                     $breakEvenSalesPrice
     * @param int                     $breakEvenSalesQuantity
     * @param int                     $initialInvestment
     * @param int|null                $investmentReturnIn
     * @param int|null                $selfSustainabilityIn
     * @param int                     $totalProfit
     * @param array                   $cashConversionCycle
     * @param array                   $profitTimeline
     */
    private function __construct(
        Product $product,
        int $productCost,
        int $initialMinimumOrderQuantity,
        int $breakEvenSalesPrice,
        int $breakEvenSalesQuantity,
        int $initialInvestment,
        ?int $investmentReturnIn,
        ?int $selfSustainabilityIn,
        int $totalProfit,
        array $cashConversionCycle,
        array $profitTimeline
    ) {
        $this->product                     = $product;
        $this->productCost                 = $productCost;
        $this->initialMinimumOrderQuantity = $initialMinimumOrderQuantity;
        $this->breakEvenSalesPrice         = $breakEvenSalesPrice;
        $this->breakEvenSalesQuantity      = $breakEvenSalesQuantity;
        $this->initialInvestment           = $initialInvestment;
        $this->investmentReturnIn          = $investmentReturnIn;
        $this->selfSustainabilityIn        = $selfSustainabilityIn;
        $this->totalProfit                 = $totalProfit;
        $this->cashConversionCycle         = $cashConversionCycle;
        $this->profitTimeline              = $profitTimeline;
    }

    /**
     * @return \Domain\Product\Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @return int
     */
    public function getProductCost(): int
    {
        return $this->productCost;
    }

    /**
     * @return int
     */
    public function getInitialMinimumOrderQuantity(): int
    {
        return $this->initialMinimumOrderQuantity;
    }

    /**
     * @return int
     */
    public function getBreakEvenSalesPrice(): int
    {
        return $this->breakEvenSalesPrice;
    }

    /**
     * @return int
     */
    public function getBreakEvenSalesQuantity(): int
    {
        return $this->breakEvenSalesQuantity;
    }

    /**
     * @return int
     */
    public function getInitialInvestment(): int
    {
        return $this->initialInvestment;
    }

    /**
     * @return int|null
     */
    public function getInvestmentReturnIn(): ?int
    {
        return $this->investmentReturnIn;
    }

    /**
     * @return int|null
     */
    public function getSelfSustainabilityIn(): ?int
    {
        return $this->selfSustainabilityIn;
    }

    /**
     * @return int
     */
    public function getTotalProfit(): int
    {
        return $this->totalProfit;
    }

    /**
     * @return array
     */
    public function getConversionCycle(): array
    {
        return $this->cashConversionCycle;
    }

    /**
     * @return array
     */
    public function getProfitTimeline(): array
    {
        return $this->profitTimeline;
    }

    /**
     * @param \Domain\Product\Product $product
     * @param int                     $productCost
     * @param int                     $initialMinimumOrderQuantity
     * @param int                     $breakEvenSalesPrice
     * @param int                     $breakEvenSalesQuantity
     * @param int                     $initialInvestment
     * @param int|null                $investmentReturnIn
     * @param int|null                $selfSustainabilityIn
     * @param int                     $totalProfit
     * @param array                   $cashConversionCycle
     * @param array                   $profitTimeline
     *
     * @return static
     */
    public static function make(
        Product $product,
        int $productCost,
        int $initialMinimumOrderQuantity,
        int $breakEvenSalesPrice,
        int $breakEvenSalesQuantity,
        int $initialInvestment,
        ?int $investmentReturnIn,
        ?int $selfSustainabilityIn,
        int $totalProfit,
        array $cashConversionCycle,
        array $profitTimeline
    ): self {
        return new static(
            $product,
            $productCost,
            $initialMinimumOrderQuantity,
            $breakEvenSalesPrice,
            $breakEvenSalesQuantity,
            $initialInvestment,
            $investmentReturnIn,
            $selfSustainabilityIn,
            $totalProfit,
            $cashConversionCycle,
            $profitTimeline
        );
    }
}
