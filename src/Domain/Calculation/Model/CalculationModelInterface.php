<?php

declare(strict_types=1);

namespace Domain\Calculation\Model;

use Domain\Product\Product;

interface CalculationModelInterface
{
    /**
     * @return \Domain\Product\Product
     */
    public function getProduct(): Product;

    /**
     * @return int
     */
    public function getAverageCost(): int;

    /**
     * @return int
     */
    public function getAverageMonthlySales(): int;

    /**
     * @return float
     */
    public function getMarketplaceFee(): float;

    /**
     * @return float
     */
    public function getSalesPriceAssumption(): float;

    /**
     * @return float
     */
    public function getSalesPriceIncrease(): float;

    /**
     * @return float
     */
    public function getMonthlySalesAssumptions(): float;

    /**
     * @return float
     */
    public function getSalesGrowthRate(): float;

    /**
     * @return float
     */
    public function getStableGrowthRate(): float;

    /**
     * @return int|null
     */
    public function getAverageSalesRank(): ?int;

    /**
     * @return int|null
     */
    public function getReviews(): ?int;

    /**
     * @return int
     */
    public function getRevenuePeriodInMonths(): int;

    /**
     * @return bool
     */
    public function hasProductCost(): bool;

    /**
     * @return int
     */
    public function getProductCost(): int;

    /**
     * @return bool
     */
    public function hasMinimumOrderQuantity(): bool;

    /**
     * @return int
     */
    public function getMinimumOrderQuantity(): int;
}
