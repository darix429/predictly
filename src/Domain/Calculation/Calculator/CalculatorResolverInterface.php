<?php

declare(strict_types=1);

namespace Domain\Calculation\Calculator;

use Domain\Marketplace\Marketplace;

interface CalculatorResolverInterface
{
    /**
     * @param \Domain\Marketplace\Marketplace $marketplace
     *
     * @return \Domain\Calculation\Calculator\CalculatorInterface
     */
    public function getCalculatorForMarketplace(Marketplace $marketplace): CalculatorInterface;
}
