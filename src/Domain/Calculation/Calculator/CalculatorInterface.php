<?php

declare(strict_types=1);

namespace Domain\Calculation\Calculator;

use Domain\Calculation\Model\CalculationModelInterface;
use Domain\Calculation\Model\CalculationResultModelInterface;

interface CalculatorInterface
{
    /**
     * @param \Domain\Calculation\Model\CalculationModelInterface[] $calculationModels
     *
     * @return array
     */
    public function calculate(array $calculationModels): array;
}
