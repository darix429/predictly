<?php

declare(strict_types=1);

namespace Domain\Calculation\Calculator;

use Domain\Calculation\Model\CalculationModelInterface;
use Domain\Calculation\Model\CalculationResultModel;
use Domain\Calculation\Model\CalculationResultModelInterface;
use Domain\Core\CurrencyTransformer;

abstract class AbstractCalculator implements CalculatorInterface
{
    /**
     * @var \Domain\Calculation\Model\CalculationModelInterface
     */
    protected CalculationModelInterface $calculationModel;

    /**
     * @var array
     */
    private array $priceFactors;

    /**
     * @var array
     */
    private array $cccConfig;

    /**
     * @var int
     */
    private int $monthsPeriod;

    /**
     * AbstractCalculator constructor.
     *
     * @param array $priceFactors
     * @param array $cccConfig
     * @param int   $monthsPeriod
     */
    public function __construct(array $priceFactors, array $cccConfig, int $monthsPeriod)
    {
        $this->priceFactors = $priceFactors;
        $this->cccConfig    = $cccConfig;
        $this->monthsPeriod = $monthsPeriod;
    }

    /**
     * @param \Domain\Calculation\Model\CalculationResultModelInterface[] $calculationModels
     *
     * @return array
     */
    public function calculate(array $calculationModels): array
    {
        $result = [];

        foreach ($calculationModels as $calculationModel) {
            $result[] = $this->calculateSingle($calculationModel);
        }

        return $result;
    }

    /**
     * @param \Domain\Calculation\Model\CalculationModelInterface $calculationModel
     *
     * @return \Domain\Calculation\Model\CalculationResultModelInterface
     */
    protected function calculateSingle(CalculationModelInterface $calculationModel): CalculationResultModelInterface
    {
        $this->setCalculationModel($calculationModel);

        $productCost = $this->getAverageProductCost();

        $breakEvenSalesPrice = $this->getBreakEvenSalesPrice($productCost);

        $initialMinimumOrderQuantity = $this->getInitialMinimumOrderQuantity();

        $initialOutlayForProduct = $this->getOutlay($productCost, $initialMinimumOrderQuantity);

        $breakEvenSalesQuantity = $this->getBreakEvenSalesQuantity($initialOutlayForProduct);

        $cashConversionCycleSensitivity = $this->getCashConversionCycleSensitivity();

        [
            $investmentReturnIn,
            $selfSustainabilityIn,
            $periodMonthlyRevenueSum,
            $profitTimeline,
        ] = $this->getCalculationResult($initialMinimumOrderQuantity, $initialOutlayForProduct, $productCost);

        return CalculationResultModel::make(
            $this->calculationModel->getProduct(),
            $productCost,
            $initialMinimumOrderQuantity,
            $breakEvenSalesPrice,
            $breakEvenSalesQuantity,
            $initialOutlayForProduct,
            $investmentReturnIn,
            $selfSustainabilityIn,
            $periodMonthlyRevenueSum,
            $cashConversionCycleSensitivity,
            $profitTimeline
        );
    }

    /**
     * @param int $minimumOrderQuantity
     * @param int $initialOutlayForProduct
     * @param int $averageProductCost
     *
     * @return array
     */
    protected function getCalculationResult(
        int $minimumOrderQuantity,
        int $initialOutlayForProduct,
        int $averageProductCost
    ): array {
        $salesPriceAssumption = $this->calculationModel->getSalesPriceAssumption();
        $monthlySalesPrice    = $this->getMonthlySalesPrice($salesPriceAssumption);

        $monthlySalesAssumptions = $this->calculationModel->getMonthlySalesAssumptions();
        $monthlySalesQuantity    = $this->getMonthlySalesQuantity($monthlySalesAssumptions);

        $investmentReturnIn            = null;
        $selfSustainabilityIn          = null;
        $monthlyOutlaySum              = 0;
        $totalMonthlyRevenueSum        = 0;
        $periodMonthlyRevenueSum       = 0;
        $profitTimeline                = [];
        $salesPriceIncrease            = $this->calculationModel->getSalesPriceIncrease();
        $salesGrowthRateIncrease       = $this->calculationModel->getSalesGrowthRate();
        $stableSalesGrowthRateIncrease = $this->calculationModel->getStableGrowthRate();

        for ($monthNumber = 1; $monthNumber <= $this->monthsPeriod; $monthNumber++) {

            $monthlyRevenue = $this->getMonthlyRevenue($monthlySalesPrice, $monthlySalesQuantity);

            $monthlyOutlay          = $this->getOutlay($averageProductCost, $minimumOrderQuantity);
            $monthlyOutlaySum       += $monthlyOutlay;
            $totalMonthlyRevenueSum += $monthlyRevenue;

            if ($monthNumber <= $this->calculationModel->getRevenuePeriodInMonths()) {
                $periodMonthlyRevenueSum = $totalMonthlyRevenueSum;
            }

            if (!$investmentReturnIn && $monthlyRevenue > $initialOutlayForProduct) {
                $investmentReturnIn = $monthNumber;
            }

            if (!$selfSustainabilityIn && $totalMonthlyRevenueSum > $monthlyOutlaySum) {
                $selfSustainabilityIn = $monthNumber;
            }

            if (($salesPriceAssumption + $salesPriceIncrease) < 1) {
                $salesPriceAssumption += $salesPriceIncrease;
            } else {
                $salesPriceAssumption = 1;
            }

            $monthlySalesPrice = $this->getMonthlySalesPrice($salesPriceAssumption);

            if (($monthlySalesAssumptions + $salesGrowthRateIncrease) < 1) {
                $monthlySalesAssumptions += $salesGrowthRateIncrease;
            } else {
                $monthlySalesAssumptions += $stableSalesGrowthRateIncrease;
            }

            $monthlySalesQuantity = $this->getMonthlySalesQuantity($monthlySalesAssumptions);
            $minimumOrderQuantity = $this->getMinimumOrderQuantity($monthlySalesQuantity);

            $profitTimeline[$monthNumber] =
                CurrencyTransformer::centsToDollars($monthlyRevenue - $monthlyOutlay); //todo: Need check formula
        }

        return [
            $investmentReturnIn,
            $selfSustainabilityIn,
            $periodMonthlyRevenueSum,
            $profitTimeline,
        ];
    }

    /**
     * @return int
     */
    protected function getAverageProductCost(): int
    {
        if ($this->calculationModel->hasProductCost()) {
            return $this->calculationModel->getProductCost();
        }

        $factor = $this->getPriceFactor();

        return $averageProductCost = (int)($this->calculationModel->getAverageCost() / 100 * $factor);
    }

    /**
     * @param int $averageProductCost
     *
     * @return int
     */
    protected function getBreakEvenSalesPrice(int $averageProductCost): int
    {
        $fee = $averageProductCost * $this->calculationModel->getMarketplaceFee();

        return (int)round(($averageProductCost + $fee), 2);
    }

    /**
     * @param \Domain\Calculation\Model\CalculationModelInterface $calculationModel
     */
    protected function setCalculationModel(CalculationModelInterface $calculationModel): void
    {
        $this->calculationModel = $calculationModel;
    }

    /**
     * @return int
     */
    protected function getInitialMinimumOrderQuantity(): int
    {
        if ($this->calculationModel->hasMinimumOrderQuantity()) {
            return $this->calculationModel->getMinimumOrderQuantity();
        }

        $initialMinimumOrderQuantity = ceil(
            $this->calculationModel->getAverageMonthlySales() *
            (
                $this->calculationModel->getMonthlySalesAssumptions() +
                $this->calculationModel->getMonthlySalesAssumptions() +
                $this->calculationModel->getSalesGrowthRate()
            ));

        return (int)$initialMinimumOrderQuantity;
    }

    /**
     * @param int   $averageProductCost
     * @param float $initialMinimumOrderQuantity
     *
     * @return int
     */
    protected function getOutlay(int $averageProductCost, float $initialMinimumOrderQuantity): int
    {
        return (int)($averageProductCost * $initialMinimumOrderQuantity);
    }

    /**
     * @param int $initialOutlay
     *
     * @return int
     */
    protected function getBreakEvenSalesQuantity(int $initialOutlay): int
    {
        $cost = $this->calculationModel->getAverageCost();
        $fee  = $cost * $this->calculationModel->getMarketplaceFee();

        return (int)round(
            $initialOutlay / ($cost + $fee)
        );
    }

    /**
     * @param int $breakEvenSalesQuantity
     *
     * @param int $monthlySales
     *
     * @return float
     */
    protected function getCashConversionCycle(int $breakEvenSalesQuantity, int $monthlySales): float
    {
        return round($breakEvenSalesQuantity / $monthlySales, 2);
    }

    /**
     * @return array
     */
    protected function getCashConversionCycleSensitivity(): array
    {
        // TODO: refactor this block after we check logic

        $cccConfig      = $this->cccConfig;
        $productCost    = $this->getAverageProductCost();
        $monthlySales   = $this->calculationModel->getProduct()->monthly_sales;
        $marketplaceFee = $this->calculationModel->getMarketplaceFee();
        $salesPrice     = $this->calculationModel->getProduct()->cost;
        $breakEvenSp    = $this->getBreakEvenSalesPrice($productCost);
        $moq            = $this->getMinimumOrderQuantity($monthlySales);

        $static = [
            'monthly-sales'           => $monthlySales,
            'sales-price'             => $salesPrice,
            'break-event-sales-price' => $breakEvenSp,
        ];

        foreach ($cccConfig['ms'] as $msKey => $msConfig) {
            $static[$msKey] = (int)round($static[$msConfig['value']] * $msConfig['factor'], 0);
        }

        foreach ($cccConfig['sp'] as $spKey => $spConfig) {
            $static[$spKey] = (int)round($static[$spConfig['value']] * $spConfig['factor'], 0);
        }

        uasort($cccConfig['ms'], static function ($v1, $v2) {
            return $v1['order'] <=> $v2['order'];
        });

        uasort($cccConfig['sp'], static function ($v1, $v2) {
            return $v1['order'] <=> $v2['order'];
        });

        $result = [];

        foreach ($cccConfig['sp'] as $spKey => $spConfig) {

            $sp        = $static[$spKey];
            $spDollars = CurrencyTransformer::centsToDollars($sp);
            $percents  = $this->diffPercents($static['sp-in'], $sp);

            $row = [$percents, $spDollars, []];

            foreach ($cccConfig['ms'] as $msKey => $msConfig) {

                $ms       = $static[$msKey];
                $value    = round($productCost * $moq / ($ms * $sp * $marketplaceFee), 2);
                $percents = $this->diffPercents($static['ms-in'], $ms);

                $row[2][] = [$percents, $ms, $value];
            }

            $result[] = $row;
        }

        return $result;
    }

    /**
     * @param int $initial
     * @param int $current
     *
     * @return int
     */
    protected function diffPercents(int $initial, int $current): int
    {
        if ($initial - $current !== 0) {
            $diff = $initial - $current;

            return (int)ceil(-$diff / $initial * 100);
        }

        return 0;
    }

    /**
     * @param float $salesPriceAssumption
     *
     * @return int
     */
    protected function getMonthlySalesPrice(float $salesPriceAssumption): int
    {
        return (int)round(($this->calculationModel->getAverageCost() * $salesPriceAssumption));
    }

    /**
     * @param float $monthlySalesAssumptions
     *
     * @return int
     */
    protected function getMonthlySalesQuantity(float $monthlySalesAssumptions): int
    {
        return (int)round($this->calculationModel->getAverageMonthlySales() * $monthlySalesAssumptions);
    }

    /**
     * @param int $monthlySalesPrice
     * @param int $monthlySalesQuantity
     *
     * @return int
     */
    protected function getMonthlyRevenue(int $monthlySalesPrice, int $monthlySalesQuantity): int
    {
        $total = $monthlySalesPrice * $monthlySalesQuantity;
        $fee   = $total * $this->calculationModel->getMarketplaceFee();

        return (int)round($total - $fee);
    }

    /**
     * @param $monthlySalesQuantity
     *
     * @return int
     */
    protected function getMinimumOrderQuantity($monthlySalesQuantity): int
    {
        $minimumOrderQuantity = \ceil(
            (
                $monthlySalesQuantity +
                $this->calculationModel->getSalesGrowthRate() * $this->calculationModel->getAverageMonthlySales()
            )
        );

        return (int)$minimumOrderQuantity;
    }

    /**
     * @return int
     */
    protected function getPriceFactor(): int
    {
        $averageCost = $this->calculationModel->getAverageCost();

        foreach ($this->priceFactors as $factor => $priceFactor) {
            if ($averageCost > $priceFactor[0]) {
                if (!isset($priceFactor[1])) {
                    return $factor;
                } elseif ($averageCost <= $priceFactor[1]) {
                    return $factor;
                }
            }
        }
    }
}
