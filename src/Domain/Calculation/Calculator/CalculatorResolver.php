<?php

declare(strict_types=1);

namespace Domain\Calculation\Calculator;

use Domain\Marketplace\Marketplace;
use DomainException;
use Illuminate\Contracts\Foundation\Application;

class CalculatorResolver implements CalculatorResolverInterface
{
    /**
     * @var array
     */
    private array $config;

    /**
     * @var \Illuminate\Contracts\Foundation\Application
     */
    private Application $application;

    /**
     * CalculatorResolver constructor.
     *
     * @param array                                        $config
     * @param \Illuminate\Contracts\Foundation\Application $application
     */
    public function __construct(array $config, Application $application)
    {
        $this->config      = $config;
        $this->application = $application;
    }

    /**
     * @param \Domain\Marketplace\Marketplace $marketplace
     *
     * @return \Domain\Calculation\Calculator\CalculatorInterface
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function getCalculatorForMarketplace(Marketplace $marketplace): CalculatorInterface
    {
        $name = $marketplace->name;

        if (!isset($this->config[$name])) {
            throw new DomainException("Parser for marketplace [{$name}] not found!");
        }

        return $this->application->make($this->config[$name]['calculator']['class']);
    }
}
