<?php

declare(strict_types=1);

namespace Domain\Calculation\Domain;

use Domain\Calculation\Model\CalculationModel;
use Domain\Calculation\Model\CalculationModelInterface;
use Domain\Factor\Factor;
use Domain\Marketplace\Marketplace;
use Domain\Prediction\Prediction;
use Domain\Product\Product;
use Domain\Session\Session;

class CalculationModelCreator
{
    /**
     * @param \Domain\Session\Session $session
     *
     * @return \Domain\Calculation\Model\CalculationModelInterface[]
     */
    public function createModels(Session $session): array
    {
        /** @var \Domain\Marketplace\Marketplace $marketplace */
        $marketplace = $session->marketplace()->first();

//        /** @var \Domain\Factor\Factor $factor */
//        if (!$session->factor()->first()) {
//            /** @var \Domain\Prediction\Prediction $prediction */
//            $prediction = $session->prediction()->first();
//            $factor     = $prediction->factor()->first();
//        } else {
//            $factor = $session->factor()->first();
//        }

        /** @var \Domain\Product\Product[] $products */
        $products = $session->products()->get();

        $result = [];

        /** @var \Domain\Product\Product $product */
        foreach ($products as $product) {
            $result[] = $this->create($marketplace, $product);
        }

        return $result;
    }

    /**
     * @param \Domain\Marketplace\Marketplace $marketplace
     * @param \Domain\Product\Product         $product
     *
     * @return \Domain\Calculation\Model\CalculationModelInterface
     */
    private function create(
        Marketplace $marketplace,
        Product $product
    ): CalculationModelInterface {
        return CalculationModel::create($marketplace, $product);
    }
}
