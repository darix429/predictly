<?php

declare(strict_types=1);

namespace Domain\Calculation\Domain;

use Domain\Calculation\Calculator\CalculatorResolverInterface;
use Domain\Session\Result\Domain\SessionResultDomain;
use Domain\Session\Session;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CalculationDomain
{
    /**
     * @var \Domain\Calculation\Calculator\CalculatorResolverInterface
     */
    private CalculatorResolverInterface $calculatorResolver;

    /**
     * @var \Domain\Calculation\Domain\CalculationModelCreator
     */
    private CalculationModelCreator $modelCreator;

    /**
     * @var \Domain\Session\Result\Domain\SessionResultDomain
     */
    private SessionResultDomain $sessionResultDomain;

    /**
     * CalculationDomain constructor.
     *
     * @param \Domain\Calculation\Calculator\CalculatorResolverInterface $calculatorResolver
     * @param \Domain\Calculation\Domain\CalculationModelCreator         $modelCreator
     * @param \Domain\Session\Result\Domain\SessionResultDomain          $sessionResultDomain
     */
    public function __construct(
        CalculatorResolverInterface $calculatorResolver,
        CalculationModelCreator $modelCreator,
        SessionResultDomain $sessionResultDomain
    ) {
        $this->calculatorResolver = $calculatorResolver;
        $this->modelCreator = $modelCreator;
        $this->sessionResultDomain = $sessionResultDomain;
    }

    /**
     * @param \Domain\Session\Session $session
     *
     * @return \Domain\Session\Result\SessionResult[]
     * @throws \Exception
     */
    public function calculate(Session $session): array
    {
        // 1. Get marketplace
        /** @var \Domain\Marketplace\Marketplace $marketplace */
        if (!($marketplace = $session->marketplace()->first())) {
            throw new ModelNotFoundException('There is no marketplace for calculation');
        }

        // 2. Get Marketplace calculator
        $calculator = $this->calculatorResolver->getCalculatorForMarketplace($marketplace);

        // 3. Create calculation models from session
        $calculationModels = $this->modelCreator->createModels($session);

        // 4. Calculate and create calculation result models from calculation models
        $calculationResultModels = $calculator->calculate($calculationModels);

        // 5. Create session results from calculation result models
        return $this->sessionResultDomain->createFromCalculationResultModels($session, $calculationResultModels);
    }
}
