<?php

declare(strict_types=1);

namespace Domain\Calculation\Provider;

use Carbon\Laravel\ServiceProvider;
use Domain\Calculation\Calculator\CalculatorResolver;
use Domain\Calculation\Calculator\CalculatorResolverInterface;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Support\DeferrableProvider;

class CalculatorResolverProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * @return array
     */
    public function provides()
    {
        return [
            CalculatorResolverInterface::class,
        ];
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->singleton(CalculatorResolverInterface::class, function (Application $application) {
            $configRepository = $application->make(Repository::class);

            $config = $configRepository->get('marketplace.marketplaces');

            return new CalculatorResolver($config, $application);
        });
    }
}
