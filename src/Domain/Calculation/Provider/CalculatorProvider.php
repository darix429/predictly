<?php

declare(strict_types=1);

namespace Domain\Calculation\Provider;

use Domain\Calculation\Calculator\Marketplace\AmazonCalculator;
use Domain\Marketplace\Marketplace;
use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class CalculatorProvider extends ServiceProvider implements DeferrableProvider
{
    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            AmazonCalculator::class,
        ];
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerAmazonCalculator();
    }

    public function registerAmazonCalculator(): void
    {
        $this->app->bind(AmazonCalculator::class, function (Application $application) {
            $configRepository = $application->make(Repository::class);

            $config    = $configRepository->get('marketplace.marketplaces.' . Marketplace::AMAZON_MARKETPLACE);
            $cccConfig = $configRepository->get('calculation.ccc');

            return new AmazonCalculator($config['price']['factors'], $cccConfig, $config['monthsPeriod']);
        });
    }
}
