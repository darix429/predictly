<?php

declare(strict_types=1);

namespace Domain\Factor\Model;

use Illuminate\Database\Eloquent\Relations\MorphOne;

interface FactorableInterface
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function factor(): MorphOne;
}
