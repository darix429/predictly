<?php

declare(strict_types=1);

namespace Domain\Factor\Model;

interface TuneModel
{
    /**
     * @return \Domain\Factor\Model\FactorModelInterface[]
     */
    public function getFactors(): array ;
}
