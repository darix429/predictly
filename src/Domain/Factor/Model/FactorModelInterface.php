<?php

declare(strict_types=1);

namespace Domain\Factor\Model;

interface FactorModelInterface
{
    /**
     * @return int|null
     */
    public function getFactorableId(): ?int;

    /**
     * @return int
     */
    public function getSalesPriceAssumption(): int;

    /**
     * @return int
     */
    public function getSalesPriceIncrease(): int;

    /**
     * @return int
     */
    public function getMonthlySales(): int;

    /**
     * @return int
     */
    public function getSalesGrowsRate(): int;

    /**
     * @return int
     */
    public function getStableGrowthRate(): int;

    /**
     * @return bool
     */
    public function hasProductCost(): bool;

    /**
     * @return int|null
     */
    public function getProductCost(): ?int;

    /**
     * @return bool
     */
    public function hasMinimumOrderQuantity(): bool;

    /**
     * @return int
     */
    public function getMinimumOrderQuantity(): ?int;
}
