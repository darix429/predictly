<?php

declare(strict_types=1);

namespace Domain\Factor\Model;

use Domain\Core\CurrencyTransformer;
use Domain\Core\RoundTransformer;

class FactorModel implements FactorModelInterface
{
    /**
     * @var int
     */
    private int $factorableId;

    /**
     * @var int
     */
    private int $salesPriceAssumption;

    /**
     * @var int
     */
    private int $salesPriceIncrease;

    /**
     * @var int
     */
    private int $monthlySales;

    /**
     * @var int
     */
    private int $salesGrowsRate;

    /**
     * @var int|null
     */
    private ?int $productCost;

    /**
     * @var int|null
     */
    private ?int $minimumOrderQuantity;

    /**
     * @var int
     */
    private int $stableGrowthRate;

    /**
     * FactorModel constructor.
     *
     * @param int        $factorableId
     * @param int        $salesPriceAssumption
     * @param int        $salesPriceIncrease
     * @param int        $monthlySales
     * @param int        $salesGrowsRate
     * @param int|null $productCost
     * @param int|null   $minimumOrderQuantity
     * @param int        $stableGrowthRate
     */
    public function __construct(
        int $factorableId,
        int $salesPriceAssumption,
        int $salesPriceIncrease,
        int $monthlySales,
        int $salesGrowsRate,
        ?int $productCost,
        ?int $minimumOrderQuantity,
        int $stableGrowthRate = 5
    ) {
        $this->factorableId         = $factorableId;
        $this->salesPriceAssumption = $salesPriceAssumption;
        $this->salesPriceIncrease   = $salesPriceIncrease;
        $this->monthlySales         = $monthlySales;
        $this->salesGrowsRate       = $salesGrowsRate;
        $this->productCost          = $productCost;
        $this->minimumOrderQuantity = $minimumOrderQuantity;
        $this->stableGrowthRate     = $stableGrowthRate;
    }

    /**
     * @inheritDoc
     */
    public function getFactorableId(): int
    {
        return $this->factorableId;
    }

    /**
     * @inheritDoc
     */
    public function getSalesPriceAssumption(): int
    {
        return $this->salesPriceAssumption;
    }

    /**
     * @inheritDoc
     */
    public function getSalesPriceIncrease(): int
    {
        return $this->salesPriceIncrease;
    }

    /**
     * @inheritDoc
     */
    public function getMonthlySales(): int
    {
        return $this->monthlySales;
    }

    /**
     * @inheritDoc
     */
    public function getSalesGrowsRate(): int
    {
        return $this->salesGrowsRate;
    }

    /**
     * @return bool
     */
    public function hasProductCost(): bool
    {
        return (bool)$this->productCost;
    }

    /**
     * @return int|null
     */
    public function getProductCost(): ?int
    {
        if (!$this->hasProductCost()) {
            return null;
        }

        return $this->productCost;
    }

    /**
     * @return bool
     */
    public function hasMinimumOrderQuantity(): bool
    {
        return (bool)$this->minimumOrderQuantity;
    }

    /**
     * @return int|null
     */
    public function getMinimumOrderQuantity(): ?int
    {
        if (!$this->hasMinimumOrderQuantity()) {
            return null;
        }

        return $this->minimumOrderQuantity;
    }

    /**
     * @return int
     */
    public function getStableGrowthRate(): int
    {
        return $this->stableGrowthRate;
    }

    /**
     * @param array $attributes
     *
     * @return static
     */
    public static function makeForProductFromArray(array $attributes): FactorModelInterface
    {
        $productCost          = isset($attributes['productCost'])
            ? CurrencyTransformer::dollarsToCents($attributes['productCost'])
            : null;
        $minimumOrderQuantity = $attributes['minimumOrderQuantity'] ?? null;

        return new static(
            $attributes['productId'],
            $attributes['salesPriceAssumption'],
            $attributes['salesPriceIncrease'],
            $attributes['monthlySales'],
            $attributes['salesGrowsRate'],
            $productCost,
            $minimumOrderQuantity,
        );
    }
}
