<?php

declare(strict_types=1);

namespace Domain\Factor\Domain;

use Domain\Factor\Factor;
use Domain\Factor\Model\FactorableInterface;
use Domain\Factor\Model\FactorModelInterface;
use Domain\Factor\Model\StoreFactorModel;
use Domain\Factor\Model\TuneModel;
use Domain\Prediction\Model\UpdatePredictionModel;
use Domain\Prediction\Prediction;
use Domain\Session\Session;

class FactorDomain
{
    /**
     * @var \Domain\Factor\Domain\FactorCreator
     */
    private FactorCreator $creator;

    /**
     * @var \Domain\Factor\Domain\FactorUpdater
     */
    private FactorUpdater $updater;

    /**
     * FactorDomain constructor.
     *
     * @param \Domain\Factor\Domain\FactorCreator $creator
     * @param \Domain\Factor\Domain\FactorUpdater $updater
     */
    public function __construct(FactorCreator $creator, FactorUpdater $updater)
    {
        $this->creator = $creator;
        $this->updater = $updater;
    }

    /**
     * @param \Domain\Factor\Model\TuneModel $tuneModel
     *
     * @param \Domain\Session\Session        $session
     *
     * @return \Domain\Factor\Factor
     */
    public function tune(TuneModel $tuneModel, Session $session): Factor
    {
        return $this->creator->tune($tuneModel, $session);
    }

    /**
     * @param \Domain\Factor\Model\FactorModelInterface $model
     *
     * @param \Domain\Factor\Model\FactorableInterface  $factorable
     *
     * @return \Domain\Factor\Factor
     */
    public function create(FactorModelInterface $model, FactorableInterface $factorable): Factor
    {
        return $this->creator->create($model, $factorable);
    }

    /**
     * @param \Domain\Factor\Factor                          $factor
     * @param \Domain\Prediction\Model\UpdatePredictionModel $model
     *
     * @return bool
     */
    public function update(Factor $factor, UpdatePredictionModel $model): bool
    {
        return $this->updater->update($factor, $model);
    }

    public function replicate(Session $session): Session
    {
        $newSession = $session->replicate();
        $newSession->push();
    }
}
