<?php

declare(strict_types=1);

namespace Domain\Factor\Domain;

use Domain\Factor\Factor;
use Domain\Factor\Model\FactorableInterface;
use Domain\Factor\Model\FactorModelInterface;
use Domain\Factor\Model\StoreFactorModel;
use Domain\Factor\Model\TuneModel;
use Domain\Session\Session;

class FactorCreator
{
    /**
     * @param \Domain\Factor\Model\TuneModel $tuneModel
     * @param \Domain\Session\Session        $session
     *
     * @return \Domain\Factor\Factor
     */
    public function tune(TuneModel $tuneModel, Session $session): Factor
    {
        $factor = new Factor();

        $factor->sales_price_assumption = $tuneModel->getSalesPriceAssumption();
        $factor->sales_price_increase   = $tuneModel->getSalesPriceIncrease();
        $factor->monthly_sales          = $tuneModel->getMonthlySales();
        $factor->sales_growth_rate      = $tuneModel->getSalesGrowsRate();
        $factor->stable_growth_rate     = 5; //todo: Fixed when pm ask client about default value

        $factor->factorable()->associate($session);
        $factor->save();

        return $factor;
    }

    /**
     * @param \Domain\Factor\Model\FactorModelInterface $model
     *
     * @param \Domain\Factor\Model\FactorableInterface  $factorable
     *
     * @return \Domain\Factor\Factor
     */
    public function create(FactorModelInterface $model, FactorableInterface $factorable): Factor
    {
        $factor = new Factor();

        $factor->sales_price_assumption = $model->getSalesPriceAssumption();
        $factor->sales_price_increase   = $model->getSalesPriceIncrease();
        $factor->monthly_sales          = $model->getMonthlySales();
        $factor->sales_growth_rate      = $model->getSalesGrowsRate();
        $factor->stable_growth_rate     = 5; //todo: Fixed when pm ask client about default value
        $factor->product_cost           = $model->getProductCost();
        $factor->minimum_order_quantity = $model->getMinimumOrderQuantity();

        $factor->factorable()->associate($factorable);
        $factor->save();

        return $factor;
    }

    /**
     * @param \Domain\Factor\Factor                    $factor
     * @param \Domain\Factor\Model\FactorableInterface $factorable
     *
     * @return \Domain\Factor\Factor
     */
    public function replicate(Factor $factor, FactorableInterface $factorable): Factor
    {
        $replica = new Factor();

        $replica->sales_price_assumption = $factor->sales_price_assumption;
        $replica->sales_price_increase   = $factor->sales_price_increase;
        $replica->monthly_sales          = $factor->monthly_sales;
        $replica->sales_growth_rate      = $factor->sales_growth_rate;
        $replica->stable_growth_rate     = $factor->stable_growth_rate;

        $replica->factorable()->associate($factorable);
        $replica->save();

        return $replica;
    }
}
