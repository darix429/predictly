<?php

declare(strict_types=1);

namespace Domain\Factor\Domain;

use Domain\Factor\Factor;
use Domain\Factor\Model\StoreFactorModel;

class FactorUpdater
{
    /**
     * @param \Domain\Factor\Factor                 $factor
     * @param \Domain\Factor\Model\StoreFactorModel $model
     *
     * @return bool
     */
    public function update(Factor $factor, StoreFactorModel $model): bool
    {
        $factor->sales_price_assumption = $model->getSalesPriceAssumption();
        $factor->sales_price_increase   = $model->getSalesPriceIncrease();
        $factor->monthly_sales          = $model->getMonthlySales();
        $factor->sales_growth_rate      = $model->getSalesGrowsRate();
        $factor->stable_growth_rate     = $model->getStableGrowthRate();

        return $factor->save();
    }
}
