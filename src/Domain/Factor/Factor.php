<?php

declare(strict_types=1);

namespace Domain\Factor;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * @property-read in $id
 * @property int     $sales_price_assumption
 * @property int     $sales_price_increase
 * @property int     $monthly_sales
 * @property int     $sales_growth_rate
 * @property int     $stable_growth_rate
 * @property int     $product_cost
 * @property int     $minimum_order_quantity
 */
class Factor extends Model
{
    private const RELATION_SESSION = 'session';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'factors';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sales_price_assumption',
        'sales_price_increase',
        'monthly_sales',
        'sales_growth_rate',
        'stable_growth_rate',
        'product_cost',
        'minimum_order_quantity',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function factorable(): MorphTo
    {
        return $this->morphTo();
    }
}
