<?php

declare(strict_types=1);

namespace Domain\Support\Enum;

use Domain\Core\Enum\Enum;
use Illuminate\Support\Collection;

class Topic extends Enum
{
    private const SUGGESTION = 'Suggestion';
    private const BUG_REPORT = 'Bug report';
    private const NEED_GUIDANCE = 'Need Guidance';
    private const OTHER = 'Other';

    /**
     * @return array
     */
    public static function getAvailableValues(): array
    {
        return [
            self::SUGGESTION,
            self::BUG_REPORT,
            self::NEED_GUIDANCE,
            self::OTHER,
        ];
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public static function getCollection(): Collection
    {
        return Collection::make([
            self::SUGGESTION(),
            self::BUG_REPORT(),
            self::NEED_GUIDANCE(),
            self::OTHER(),
        ]);
    }

    /**
     * @return static
     */
    public static function SUGGESTION(): self
    {
        return new static(self::SUGGESTION);
    }

    /**
     * @return static
     */
    public static function BUG_REPORT(): self
    {
        return new static(self::BUG_REPORT);
    }

    /**
     * @return static
     */
    public static function NEED_GUIDANCE(): self
    {
        return new static(self::NEED_GUIDANCE);
    }

    /**
     * @return static
     */
    public static function OTHER(): self
    {
        return new static(self::OTHER);
    }
}
