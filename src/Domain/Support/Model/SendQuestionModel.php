<?php

declare(strict_types=1);

namespace Domain\Support\Model;

interface SendQuestionModel
{
    /**
     * @return string
     */
    public function getEmail(): string;

    /**
     * @return string
     */
    public function getQuestion(): string;
}
