<?php

declare(strict_types=1);

namespace Domain\Support\Model;

interface SendFeedbackModel
{
    /**
     * @return string
     */
    public function getTopic(): string;

    /**
     * @return string
     */
    public function getMessage(): string;
}
