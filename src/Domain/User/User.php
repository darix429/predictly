<?php

namespace Domain\User;

use Domain\Product\Product;
use Domain\Provider\Provider;
use Domain\Session\Session;
use Domain\Subscription\UserSubscription;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use League\OAuth2\Server\Entities\UserEntityInterface;
use Spatie\Permission\Traits\HasRoles;

/**
 * @property-read  int      $id
 * @property string         $name
 * @property string         $avatar
 * @property string         $nickname
 * @property string         $email
 * @property string         $password
 * @property boolean        $verified
 * @property \Carbon\Carbon $created_at
 */
class User extends Authenticatable implements UserEntityInterface
{
    use Notifiable, HasApiTokens, HasRoles;

    public const RELATION_PROVIDER = 'provider';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'nickname',
        'avatar',
        'verified',
        'free'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'verified'          => 'boolean',
        'created_at'        => 'datetime',
    ];

    /**
     * @inheritDoc
     */
    public function getIdentifier()
    {
        return $this->id;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function providers(): BelongsToMany
    {
        return $this->belongsToMany(Provider::class, 'provider_user', 'user_id', 'provider_id', 'id', 'id',
            self::RELATION_PROVIDER);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products(): HasMany
    {
        return $this->hasMany(Product::class, 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sessions(): HasMany
    {
        return $this->hasMany(Session::class, 'user_id', 'id');
    }

    public function subscriptions(): HasMany
    {
        return $this->hasMany(UserSubscription::class, 'user_id', 'id');
    }
}
