<?php

declare(strict_types=1);

namespace Domain\User\Repository;

use Domain\Core\Model\ListModelInterface;
use Domain\User\User;
use Illuminate\Database\Eloquent\Collection;

interface UserRepositoryInterface
{
    /**
     * @param \Domain\Core\Model\ListModelInterface $model
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findAll(ListModelInterface $model): Collection;

    /**
     * @param int $userId
     *
     * @return \Domain\User\User
     */
    public function findOrFail(int $userId): User;

    /**
     * @param string $provider
     * @param string $providerId
     *
     * @return \Domain\User\User
     */
    public function findByProviderAndId(string $provider, string $providerId): User;

    /**
     * @param string $email
     *
     * @return int
     */
    public function countByEmail(string $email): int;

    /**
     *
     * @return int
     */
    public function count(): int;
}
