<?php

declare(strict_types=1);

namespace Domain\User\Repository;

use Domain\Core\Model\ListModelInterface;
use Domain\User\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Query\JoinClause;

/**
 * Class UserRepository
 *
 * @package Domain\User\Repository
 */
class UserRepository implements UserRepositoryInterface
{
    /**
     * @param \Domain\Core\Model\ListModelInterface $model
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findAll(ListModelInterface $model): Collection
    {
        return User::query()
            ->offset($model->getOffset())
            ->limit($model->getLimit())
            ->get();
    }

    /**
     * @param int $userId
     *
     * @return \Domain\User\User
     */
    public function findOrFail(int $userId): User
    {
        /** @var \Domain\User\User $user */
        $user = User::query()->findOrFail($userId);

        return $user;
    }

    /**
     * @param string $provider
     * @param string $providerId
     *
     * @return \Domain\User\User
     */
    public function findByProviderAndId(string $provider, string $providerId): User
    {
        /** @var \Domain\User\User $user */
        $user = User::query()
            ->selectRaw('users.*')
            ->join('provider_user', function (JoinClause $join) use ($providerId) {
                $join->on('users.id', '=', 'provider_user.user_id')
                    ->where('provider_user.key', '=', $providerId);
            })
            ->join('providers', function (JoinClause $join) use ($provider) {
                $join->on('provider_user.provider_id', '=', 'providers.id')
                    ->where('providers.name', '=', $provider);
            })->firstOrFail();

        return $user;
    }

    /**
     * @param string $email
     *
     * @return int
     */
    public function countByEmail(string $email): int
    {
        return User::query()->where('email', '=', $email)->count();
    }

    /**
     *
     * @return int
     */
    public function count(): int
    {
        return User::query()->count();
    }
}
