<?php

declare(strict_types=1);

namespace Domain\User\Domain;

use Domain\Provider\Repository\ProviderRepository;
use Domain\User\Repository\UserRepositoryInterface;
use Domain\User\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Laravel\Socialite\Two\User as SocialiteUser;

class UserLinker
{
    /**
     * @var \Domain\Provider\Repository\ProviderRepository
     */
    private ProviderRepository $providerRepository;

    /**
     * @var \Domain\User\Repository\UserRepositoryInterface
     */
    private UserRepositoryInterface $userRepository;

    /**
     * @param \Domain\Provider\Repository\ProviderRepository  $providerRepository
     * @param \Domain\User\Repository\UserRepositoryInterface $userRepository
     */
    public function __construct(ProviderRepository $providerRepository, UserRepositoryInterface $userRepository)
    {
        $this->providerRepository = $providerRepository;
        $this->userRepository     = $userRepository;
    }

    /**
     * @param \Domain\User\User           $user
     * @param \Laravel\Socialite\Two\User $socialiteUser
     * @param string                      $providerName
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function link(User $user, SocialiteUser $socialiteUser, string $providerName): void
    {
        try {
            $linkedUser = $this->userRepository->findByProviderAndId($providerName, $socialiteUser->getId());

            if ($linkedUser) {

                if ($linkedUser->id === $user->id) {
                    throw ValidationException::withMessages([
                        'provider' => 'Social Account Already Linked To This User!',
                    ]);

                }

                throw ValidationException::withMessages([
                    'provider' => 'Social Account Already Linked To Another User!',
                ]);
            }
        } catch (ModelNotFoundException $exception) {
            // do nothing
            // user is not linked
        }

        $provider = $this->providerRepository->findByName($providerName);

        $user->providers()->attach($provider, [
            'key' => $socialiteUser->getId(),
        ]);
    }
}
