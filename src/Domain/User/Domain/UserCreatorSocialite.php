<?php

declare(strict_types=1);

namespace Domain\User\Domain;

use Domain\Mail\Service\MailSenderInterface;
use Domain\User\Repository\UserRepositoryInterface;
use Domain\User\User;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Database\DatabaseManager;
use Illuminate\Validation\ValidationException;
use Laravel\Socialite\Two\User as SocialiteUser;

class UserCreatorSocialite
{
    /**
     * @var \Domain\Mail\Service\MailSenderInterface
     */
    private MailSenderInterface $mailSender;

    /**
     * @var \Illuminate\Contracts\Hashing\Hasher
     */
    private Hasher $hasher;
    /**
     * @var \Illuminate\Database\DatabaseManager
     */
    private DatabaseManager $databaseManager;

    /**
     * @var \Domain\User\Domain\UserLinker
     */
    private UserLinker $userLinker;
    /**
     * @var \Domain\User\Repository\UserRepositoryInterface
     */
    private UserRepositoryInterface $userRepository;

    /**
     * @param \Domain\Mail\Service\MailSenderInterface        $mailSender
     * @param \Illuminate\Contracts\Hashing\Hasher            $hasher
     * @param \Illuminate\Database\DatabaseManager            $databaseManager
     * @param \Domain\User\Domain\UserLinker                  $userLinker
     * @param \Domain\User\Repository\UserRepositoryInterface $userRepository
     */
    public function __construct(
        MailSenderInterface $mailSender,
        Hasher $hasher,
        DatabaseManager $databaseManager,
        UserLinker $userLinker,
        UserRepositoryInterface $userRepository
    ) {
        $this->mailSender      = $mailSender;
        $this->hasher          = $hasher;
        $this->databaseManager = $databaseManager;
        $this->userLinker      = $userLinker;
        $this->userRepository  = $userRepository;
    }

    /**
     * @param \Laravel\Socialite\Two\User $socialiteUser
     * @param string                      $providerName
     *
     * @return \Domain\User\User
     * @throws \Exception
     * @noinspection CryptographicallySecureRandomnessInspection
     */
    public function create(SocialiteUser $socialiteUser, string $providerName): User
    {
        $email = $socialiteUser->getEmail();

        if ($this->userRepository->countByEmail($email)) {
            throw ValidationException::withMessages([
                'provider' => 'Email From Your Account Already Used. Please Login And Link Your Account.',
            ]);
        }

        $this->databaseManager->beginTransaction();

        $entity = new User();

        $entity->name     = $socialiteUser->getName();
        $entity->avatar   = $socialiteUser->getAvatar();
        $entity->nickname = $socialiteUser->getNickname();
        $entity->email    = $email;
        $entity->password = $this->hasher->make(bin2hex(openssl_random_pseudo_bytes(16)));

        $entity->save();

        $this->userLinker->link($entity, $socialiteUser, $providerName);
        // $this->mailSender->sendUserRegisteredSuccessfully($entity);

        $this->databaseManager->commit();

        return $entity;
    }
}
