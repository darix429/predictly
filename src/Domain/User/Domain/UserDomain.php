<?php

declare(strict_types=1);

namespace Domain\User\Domain;

use Domain\User\User;
use Laravel\Socialite\Two\User as SocialiteUser;

/**
 * Class UserDomain
 *
 * @package Domain\User\Domain
 */
class UserDomain
{
    /**
     * @var \Domain\User\Domain\UserCreatorSocialite
     */
    private UserCreatorSocialite $socialiteCreator;

    /**
     * @var \Domain\User\Domain\UserCreatorArray
     */
    private UserCreatorArray $arrayCreator;

    /**
     * @var \Domain\User\Domain\UserUpdater
     */
    private UserUpdater $updater;

    /**
     * @var \Domain\User\Domain\UserDeleter
     */
    private UserDeleter $deleter;

    /**
     * @var \Domain\User\Domain\UserLinker
     */
    private UserLinker $linker;

    /**
     * UserDomain constructor.
     *
     * @param \Domain\User\Domain\UserCreatorSocialite $socialiteCreator
     * @param \Domain\User\Domain\UserCreatorArray     $arrayCreator
     * @param \Domain\User\Domain\UserUpdater          $updater
     * @param \Domain\User\Domain\UserDeleter          $deleter
     * @param \Domain\User\Domain\UserLinker           $linker
     */
    public function __construct(
        UserCreatorSocialite $socialiteCreator,
        UserCreatorArray $arrayCreator,
        UserUpdater $updater,
        UserDeleter $deleter,
        UserLinker $linker
    ) {
        $this->socialiteCreator = $socialiteCreator;
        $this->arrayCreator     = $arrayCreator;

        $this->updater = $updater;
        $this->deleter = $deleter;
        $this->linker  = $linker;
    }

    /**
     * @param \Laravel\Socialite\Two\User $socialiteUser
     * @param string                      $providerName
     *
     * @return \Domain\User\User
     * @throws \Exception
     */
    public function createFromSocialite(SocialiteUser $socialiteUser, string $providerName): User
    {
        return $this->socialiteCreator->create($socialiteUser, $providerName);
    }

    /**
     * @param array $parameters
     *
     * @return \Domain\User\User
     * @throws \Exception
     */
    public function createFromArray(array $parameters): User
    {
        return $this->arrayCreator->create($parameters);
    }

    /**
     * @param \Domain\User\User           $user
     * @param \Laravel\Socialite\Two\User $socialiteUser
     * @param string                      $providerName
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function link(User $user, SocialiteUser $socialiteUser, string $providerName): void
    {
        $this->linker->link($user, $socialiteUser, $providerName);
    }

    /**
     * @param \Domain\User\User $user
     */
    public function verify(User $user): void
    {
        $this->updater->verify($user);
    }

    /**
     * @param \Domain\User\User $user
     */
    public function unverify(User $user): void
    {
        $this->updater->unverify($user);
    }

    /**
     * @param \Domain\User\User $user
     *
     * @throws \Exception
     */
    public function delete(User $user): void
    {
        $this->deleter->delete($user);
    }

    public function switchFree(User $user): void
    {
        $this->updater->switchFree($user);
    }
}
