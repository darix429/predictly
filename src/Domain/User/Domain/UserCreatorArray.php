<?php

declare(strict_types=1);

namespace Domain\User\Domain;

use Domain\Mail\Service\MailSenderInterface;
use Domain\User\User;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Database\DatabaseManager;

class UserCreatorArray
{
    /**
     * @var \Domain\Mail\Service\MailSenderInterface
     */
    private MailSenderInterface $mailSender;

    /**
     * @var \Illuminate\Contracts\Hashing\Hasher
     */
    private Hasher $hasher;

    /**
     * @var \Illuminate\Database\DatabaseManager
     */
    private DatabaseManager $databaseManager;

    /**
     * UserCreator constructor.
     *
     * @param \Domain\Mail\Service\MailSenderInterface $mailSender
     * @param \Illuminate\Contracts\Hashing\Hasher     $hasher
     * @param \Illuminate\Database\DatabaseManager     $databaseManager
     */
    public function __construct(MailSenderInterface $mailSender, Hasher $hasher, DatabaseManager $databaseManager)
    {
        $this->mailSender      = $mailSender;
        $this->hasher          = $hasher;
        $this->databaseManager = $databaseManager;
    }

    /**
     * @param array $parameters
     *
     * @return \Domain\User\User
     * @throws \Exception
     */
    public function create(array $parameters): User
    {
        $this->databaseManager->beginTransaction();

        $entity = new User();

        $entity->name     = $parameters['name'];
        $entity->avatar   = $parameters['avatar'];
        $entity->email    = $parameters['email'];

        $entity->password = $this->hasher->make($parameters['password']);

        $entity->save();

        // $this->mailSender->sendUserRegisteredSuccessfully($entity);

        $this->databaseManager->commit();

        return $entity;
    }
}
