<?php

declare(strict_types=1);

namespace Domain\User\Domain;

use Domain\User\User;

class UserUpdater
{
    /**
     * @param \Domain\User\User $user
     */
    public function verify(User $user): void
    {
        $user->update([
            'verified' => true,
        ]);
    }

    /**
     * @param \Domain\User\User $user
     */
    public function unverify(User $user): void
    {
        $user->update([
            'verified' => false,
        ]);
    }

    public function switchFree(User $user): void
    {
        $val = !$user["free"];

        $user->update([
            'free' => $val
        ]);
    }
}
