<?php

declare(strict_types=1);

namespace Domain\User\Domain;

use Domain\Product\Product;
use Domain\Provider\Provider;
use Domain\Session\Result\SessionResult;
use Domain\Session\Session;
use Domain\User\User;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\Token;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UserDeleter
{
    /**
     * @param \Domain\User\User $user
     *
     * @throws \Exception
     */
    public function delete(User $user): void
    {
        DB::transaction(function () use ($user) {
            /** Delete all users providers */
            $user->providers()->sync([]);

            /** Delete all user sessions */
            $user->sessions()->each(function (Session $session) {
                $session->sessionResults()->each(function (SessionResult $sessionResult) {
                    $sessionResult->delete();
                });

                $session->products()->sync([]);

                $session->delete();
            });

            $user->products()->each(function (Product $product) use ($user) {
                $product->user()->dissociate();
                $product->delete();
                $product->save();
            });

            $user->tokens()->each(function (Token $token) {
                $token->forceDelete();
            });

            $user->roles()->each(function (Role $role) {
                $role->forceDelete();
            });

            $user->permissions()->each(function (Permission $permission) {
                $permission->delete();
            });

            /** Delete user */
            $user->delete();
        });

    }
}
