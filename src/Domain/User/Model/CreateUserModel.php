<?php

declare(strict_types=1);

namespace Domain\User\Model;

use Laravel\Socialite\Two\User;

class CreateUserModel
{
    /**
     * @var string
     */
    private string $email;

    /**
     * @var string
     */
    private string $password;

    /**
     * @var string
     */
    private string $name;

    /**
     * @var string
     */
    private string $nickname;

    /**
     * @var string|null
     */
    private ?string $avatar;

    /**
     * @param string      $email
     * @param string      $password
     * @param string      $name
     * @param string      $nickname
     * @param string|null $avatar
     */
    public function __construct(string $email, string $password, string $name, string $nickname, ?string $avatar = null)
    {
        $this->email    = $email;
        $this->password = $password;
        $this->name     = $name;
        $this->nickname = $nickname;
        $this->avatar   = $avatar;
    }

    /**
     * @param \Laravel\Socialite\Two\User $user
     *
     * @return \Domain\User\Model\CreateUserModel
     * @noinspection CryptographicallySecureRandomnessInspection
     */
    public static function createFromSocialite(User $user): CreateUserModel
    {
        return new self(
            $user->getEmail(),
            bin2hex(openssl_random_pseudo_bytes(16)),
            $user->getName(),
            $user->getNickname(),
            $user->getAvatar()
        );
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getNickname(): string
    {
        return $this->nickname;
    }

    /**
     * @return string|null
     */
    public function getAvatar(): ?string
    {
        return $this->avatar;
    }
}
