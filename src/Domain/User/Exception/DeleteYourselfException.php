<?php

declare(strict_types=1);

namespace Domain\User\Exception;

class DeleteYourselfException extends \LogicException
{

}
