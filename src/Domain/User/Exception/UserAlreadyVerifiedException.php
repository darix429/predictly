<?php

declare(strict_types=1);

namespace Domain\User\Exception;

use Domain\User\User;

class UserAlreadyVerifiedException extends \LogicException
{
    /**
     * @param \Domain\User\User $user
     *
     * @return \Domain\User\Exception\UserAlreadyVerifiedException
     */
    public function setUser(User $user): UserAlreadyVerifiedException
    {
        $name = $user->name;

        $this->message = "User {$name} has already verified";

        return $this;
    }
}
