<?php

declare(strict_types=1);

namespace Domain\Core\Enum;

use Illuminate\Support\Collection;
use Illuminate\Validation\Rules\In;
use MyCLabs\Enum\Enum as EnumMyCLabsEnym;

abstract class Enum extends EnumMyCLabsEnym
{
    /**
     * @return array
     */
    abstract public static function getAvailableValues(): array;

    /**
     * @return \Illuminate\Support\Collection
     */
    abstract public static function getCollection(): Collection;

    /**
     * @return \Illuminate\Validation\Rules\In
     */
    public static function getValidationRule(): In
    {
        return new In(static::getAvailableValues());
    }
}
