<?php

declare(strict_types=1);

namespace Domain\Core;

class RoundTransformer
{
    /**
     * @param     $number
     * @param int $parts
     *
     * @return float
     */
    public static function round($number, $parts = 4): float
    {
        $result = $number * $parts;
        $result = round($result);

        return $result / $parts;
    }
}
