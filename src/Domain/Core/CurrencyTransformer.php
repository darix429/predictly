<?php

declare(strict_types=1);

namespace Domain\Core;

class CurrencyTransformer
{
    /**
     * @param float $dollars
     *
     * @return int
     */
    public static function dollarsToCents(float $dollars): int
    {
        return (int)($dollars * 100);
    }

    /**
     * @param int $cents
     *
     * @return float
     */
    public static function centsToDollars(int $cents): float
    {
        return round($cents / 100, 2);
    }
}
