<?php

declare(strict_types=1);

namespace Domain\Core\Model;

interface OffsetModelInterface
{
    /**
     * @return int
     */
    public function getOffset(): int;
}
