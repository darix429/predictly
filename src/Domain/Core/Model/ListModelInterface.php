<?php

declare(strict_types=1);

namespace Domain\Core\Model;

interface ListModelInterface extends LimitedModelInterface, OffsetModelInterface
{

}
