<?php

declare(strict_types=1);

namespace Domain\Core\Model;

interface LimitedModelInterface
{
    /**
     * @return int
     */
    public function getLimit(): int;
}
