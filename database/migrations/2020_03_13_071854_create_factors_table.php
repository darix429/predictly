<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFactorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('factorable_id');
            $table->string('factorable_type');
            $table->unsignedInteger('sales_price_assumption')->comment('In percents');
            $table->unsignedInteger('sales_price_increase')->comment('In percents');
            $table->unsignedInteger('monthly_sales')->comment('In percents');
            $table->unsignedInteger('sales_growth_rate')->comment('In percents');
            $table->unsignedInteger('stable_growth_rate')->comment('In percents');
            $table->unsignedInteger('product_cost')->nullable()->comment('In cents');
            $table->unsignedInteger('minimum_order_quantity')->nullable()->comment('In items');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factors');
    }
}
