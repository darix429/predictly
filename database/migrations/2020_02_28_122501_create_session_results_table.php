<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSessionResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('session_results', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('session_id');
            $table->unsignedBigInteger('product_id');
            $table->unsignedInteger('cost');
            $table->unsignedBigInteger('minimum_order_quantity');
            $table->unsignedBigInteger('break_even_sales_price');
            $table->unsignedBigInteger('break_even_sales_quantity');
            $table->unsignedInteger('initial_investment');
            $table->unsignedInteger('investment_return_in')->nullable();
            $table->unsignedInteger('self_sustainability_in')->nullable();
            $table->unsignedInteger('total_profit');
            $table->json('cash_conversion_cycle');
            $table->json('profit_timeline');
            $table->timestamps();

            $table->foreign('session_id')->references('id')->on('sessions');
            $table->foreign('product_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('session_results');
    }
}
