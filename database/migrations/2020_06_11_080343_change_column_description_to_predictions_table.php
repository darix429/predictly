<?php

use Domain\Prediction\Prediction;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnDescriptionToPredictionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('predictions', 'category')) {
            Schema::table('predictions', function (Blueprint $table) {
                $table->dropColumn('category');
            });
        }

        Schema::table('predictions', function (Blueprint $table) {
            $table->text('description')->change();
            $table->enum('category', Prediction::CATEGORIES);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('predictions', 'category')) {
            Schema::table('predictions', function (Blueprint $table) {
                $table->dropColumn('category');
            });
        }

        Schema::table('predictions', function (Blueprint $table) {
            $table->string('description')->change();
            $table->enum('category', Prediction::CATEGORIES);
        });
    }
}
