<?php

declare(strict_types=1);

use Domain\User\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Seeder;
use Spatie\Permission\Exceptions\PermissionAlreadyExists;
use Spatie\Permission\Exceptions\RoleAlreadyExists;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class PermissionSeeder extends Seeder
{
    private const ADMINISTRATOR_ROLE = 'administrator';

    /**
     * Run the database seeds.
     *
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function run()
    {
        $this->createPermissionsAndRoles();
    }

    /**
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     *
     */
    private function createPermissionsAndRoles(): void
    {
        $this->container->make(PermissionRegistrar::class)->forgetCachedPermissions();

        /** Permissions for manage users */
        $viewUsers     = $this->createPermission('view users');
        $viewUser      = $this->createPermission('show user');
        $verifyUser    = $this->createPermission('verify user');
        $unverifyUsers = $this->createPermission('unverify user');
        $deleteUsers   = $this->createPermission('delete user');

        /** Permissions for manage marketplaces */
        $viewMarketplaces  = $this->createPermission('view marketplaces');
        $viewMarketplace   = $this->createPermission('show marketplace');
        $createMarketplace = $this->createPermission('create marketplace');
        $updateMarketplace = $this->createPermission('update marketplace');

        /** Permissions for manage predictions */
        $viewPredictions  = $this->createPermission('view predictions');
        $viewPrediction   = $this->createPermission('show prediction');
        $createPrediction = $this->createPermission('create prediction');
        $updatePrediction = $this->createPermission('update prediction');
        $deletePrediction = $this->createPermission('delete prediction');

        $administrator = $this->createRole(self::ADMINISTRATOR_ROLE);
        $administrator
            ->syncPermissions([])
            /** Manage Users */
            ->givePermissionTo($viewUsers)
            ->givePermissionTo($viewUser)
            ->givePermissionTo($verifyUser)
            ->givePermissionTo($unverifyUsers)
            ->givePermissionTo($deleteUsers)
            /** Manage Marketplaces */
            ->givePermissionTo($viewMarketplaces)
            ->givePermissionTo($viewMarketplace)
            ->givePermissionTo($createMarketplace)
            ->givePermissionTo($updateMarketplace)
            /** Manage Predictions */
            ->givePermissionTo($viewPredictions)
            ->givePermissionTo($viewPrediction)
            ->givePermissionTo($createPrediction)
            ->givePermissionTo($updatePrediction)
            ->givePermissionTo($deletePrediction);
    }

    /**
     * @param string $name
     *
     * @return \Spatie\Permission\Models\Permission
     */
    private function createPermission(string $name): Permission
    {
        /** @var \Spatie\Permission\Models\Permission $permission */
        try {
            $permission = Permission::create(['name' => $name]);
        } catch (PermissionAlreadyExists $exception) {
            $permission = Permission::query()->where('name', $name)->firstOrFail();
        }

        return $permission;
    }

    /**
     * @param string $name
     *
     * @return \Spatie\Permission\Models\Role
     */
    private function createRole(string $name): Role
    {
        /** @var \Spatie\Permission\Models\Role $role */
        try {
            $role = Role::create(['name' => $name]);
        } catch (RoleAlreadyExists $exception) {
            $role = Role::query()->where('name', $name)->firstOrFail();
        }

        return $role;
    }
}
