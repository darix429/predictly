<?php

use Illuminate\Database\Seeder;

class SubscriptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subscriptions')->insert([
            'name'=>'14-Day Trial',
            'desc'=>'Money back guarantee',
            'price'=>5,
            'duration_days'=>14
        ]);
        //
    }
}
