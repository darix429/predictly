<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(OAuthSeeder::class);
        $this->call(ProviderSeeder::class);
        $this->call(MarketplaceSeeder::class);
        $this->call(PredictionSeeder::class);
        $this->call(PermissionSeeder::class);
    }
}
