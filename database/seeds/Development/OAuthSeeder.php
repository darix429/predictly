<?php

use Illuminate\Contracts\Config\Repository;
use Illuminate\Database\Seeder;
use Laravel\Passport\Client;
use Laravel\Passport\Passport;

class OAuthSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param \Illuminate\Contracts\Config\Repository $configRepository
     *
     * @return void
     */
    public function run(Repository $configRepository)
    {
        $config = $configRepository->get('auth.oauth.clients');

        foreach ($config as $client) {
            $this->create(
                $client['id'],
                $client['secret'],
                $client['name'],
                $client['route'],
                $client['password'],
            );
        }
    }

    /**
     * @param int $id
     * @param string $secret
     * @param string $name
     * @param string $redirectRoute
     * @param bool $password
     *
     * @return \Laravel\Passport\Client
     */
    protected function create(
        int $id,
        string $secret,
        string $name,
        string $redirectRoute,
        bool $password = false
    ): Client {
        $client = Passport::client()->forceFill([
            'id'                     => $id,
            'user_id'                => null,
            'name'                   => $name,
            'secret'                 => $secret,
            'redirect'               => route($redirectRoute),
            'personal_access_client' => false,
            'password_client'        => $password,
            'revoked'                => false,
        ]);

        $client->save();

        return $client;
    }
}
