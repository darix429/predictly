<?php

declare(strict_types=1);

use Domain\Provider\Provider;
use Illuminate\Database\Seeder;


/**
 * Class ProviderSeeder
 *
 * @package ${NAMESPACE}
 */
class ProviderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createProvider(Provider::GOOGLE);
        $this->createProvider(Provider::FACEBOOK);
    }

    /**
     * @param string $name
     *
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|mixed
     */
    private function createProvider(string $name)
    {
        return factory(Provider::class)->create([
            'name' => $name,
        ]);
    }
}
