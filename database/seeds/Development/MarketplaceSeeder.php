<?php

use Domain\Marketplace\Marketplace;
use Illuminate\Database\Seeder;

class MarketplaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createMarketplace('Amazon', 15, Marketplace::PRIMARY_CATEGORY);
    }

    /**
     * @param string $name
     * @param int    $fee
     * @param string $category
     *
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|mixed
     */
    private function createMarketplace(string $name, int $fee, string $category)
    {
        return factory(Marketplace::class)->create([
            'name'     => $name,
            'fee'      => $fee,
            'category' => $category,
        ]);
    }
}
