<?php

use Domain\Factor\Factor;
use Domain\Prediction\Prediction;
use Illuminate\Database\Seeder;

class
PredictionSeeder extends Seeder
{
    private const NAMES = [
        'Worst Case'   => [
            'description'          => "Sales per Month:
                                       <b><span class='factor-span'>- 40%</span> below Average</b>
                                      Product Sales Price:
                                       <b><span class='factor-span'>- 40%</span> below Average</b>

                                      <b>This prediction is great to look at your downside and risk, and a worst-case
                                      scenario.</b>

                                      Use it to look at the downside risks of selling your product, for ultra-competitive markets, and for slow growth niches. If you’re product can make profit in this prediction model, you’re in a really good spot.",
            'salesPriceAssumption' => 40,
        ],
        'Below Average'         => [
            'description'          => "Sales per Month:
                                       <b><span class='factor-span'>- 20%</span> below Average</b>
                                       Product Sales Price: 
                                       <b><span class='factor-span'>- 20%</span> below Average</b>

                                       <b>This is a conservative or safe view on how your products will perform. It gives you a bit of buffer for mistakes, errors, marketing and more.</b>

                                       Use it to understand what your product performance will be like if you offer discounts, giveaways and have a lot of promotions when you start, or for competitive niches where you will have to work your way up to the average sales.",
            'salesPriceAssumption' => 50,
        ],
        'Average'           => [
            'description'          => "Sales per Month: 
                                       <b><span class='factor-span'>0%</span> change</b>
                                       Product Sales Price: 
                                       <b><span class='factor-span'>0%</span> change</b>

                                       <b>This is a normal view on how your products will perform.</b>

                                       This prediction model uses the data you’ve manually entered, or the average numbers from the .csv file you’ve uploaded. Use it to see how your product will perform if you sell the average number of units at the average sales price, or if you're in an average competition niche.",
            'salesPriceAssumption' => 60,
        ],
        'Above Average'       => [
            'description'          => "Sales per Month: 
                                       <b><span class='factor-span'>+20%</span> above Average</b>
                                       Product Sales Price: 
                                       <b><span class='factor-span'>+20%</span> above Average</b>

                                       <b>This is an above average view on how your product will perform.</b>

                                       This prediction model assumes you will sell more products than average, at
                                       a higher sales price than average. It’s a great choice if you want to see how
                                       your product will perform in the future. Use it for low competition niches that have really high potential and for sellers who are confident they can market a product well.",
            'salesPriceAssumption' => 70,
        ],
        'Best Case' => [
            'description'          => "Sales per Month: 
                                       <b><span class='factor-span'>+40%</span> above Average</b>
                                       Product Sales Price: 
                                       <b><span class='factor-span'>+40%</span> above Average</b>

                                       <b>This is a best case scenario or optimistic view on how your product will perform.</b>

                                       It’s a great prediction model to see how much profit and sales you’ll make in your product does really well in the market. Use it to see the upper end of your product sales and profit potential, or for sellers who are highly-confident in their ability to market a product well and sell far above the industry standard.",
            'salesPriceAssumption' => 80,
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (self::NAMES as $name => $parameters) {
            $this->createFactor(
                $this->createPrediction($name, $parameters['description']),
                $parameters['salesPriceAssumption']
            );
        }
    }

    /**
     * @param string $name
     *
     * @param string $description
     *
     * @return \Domain\Prediction\Prediction
     */
    private function createPrediction(string $name, string $description): Prediction
    {
        return factory(Prediction::class)->create([
            'name'        => $name,
            'description' => $description,
            'category'    => Prediction::PRIMARY_CATEGORY,
        ]);
    }

    /**
     * @param \Domain\Prediction\Prediction $prediction
     *
     * @param int                           $salesPriceAssumption
     *
     * @return void
     */
    private function createFactor(Prediction $prediction, int $salesPriceAssumption): void
    {
        /** @var \Domain\Factor\Factor $factor */
        $factor = factory(Factor::class)->make([
            'sales_price_assumption' => $salesPriceAssumption,
            'sales_price_increase'   => 5,
            'monthly_sales'          => 20,
            'sales_growth_rate'      => 15,
            'stable_growth_rate'     => 5,
        ]);

        $factor->factorable()->associate($prediction);
        $factor->save();
    }
}
